package xxxxxxxxxxxxxxxxxxxxx;
import android.widget.Toast;
import android.app.Activity;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.AppodealCallbacks;
/**
 * Created by benosa on 23.03.2015.
 */
public class InitAppodial {
    private static boolean shown = false;
    public static void createAds(final Activity activity,String appKey){
        //String appKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        Appodeal.initialize(activity, appKey, new AppodealCallbacks() {
            @Override
            public void onAdLoaded(boolean b) {
                //showToast("onAdLoaded");
                if(!shown && b) {
                    Appodeal.showBanner(activity);
                    shown = true;
                }
            }

            @Override
            public void onAdFailedToLoad() {

            }

            @Override
            public void onAdShown() {

            }

            @Override
            public void onAdClicked() {

            }

            @Override
            public void onAdClosed() {

            }
        });
    }
}
