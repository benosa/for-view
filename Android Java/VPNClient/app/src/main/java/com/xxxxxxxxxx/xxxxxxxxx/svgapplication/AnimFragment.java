package xxxxxxxxxxxxxxxx.svgapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import xxxxxxxxxxxxxxxxxxxxxx.MainActivity;
import xxxxxxxxxxxxxxxxxxxx.R;
import xxxxxxxxxxxxxxxxxxxx.api.IOpenVPNStatusCallback;
import com.trevorpage.tpsvg.SVGDrawable;
import com.trevorpage.tpsvg.SVGParserRenderer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AnimFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AnimFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
//http://www.pvsm.ru/android-development/8114
public class AnimFragment extends Fragment implements Handler.Callback {

    //AnimationCallback AnimationEvents;
    private ImageView imageAnim;

    private OnFragmentInteractionListener mListener;
    private Handler mHandler;
    private TextView mStatus;
    private static final int MSG_UPDATE_STATE = 0;
    private static boolean mConnected = false;
    private boolean clicked = false;
    private  CandyAnimation animation;
    public MainActivity ma;
    @Override
    public void onStart() {
        super.onStart();

    }
    public static AnimFragment newInstance(String param1, String param2) {
        AnimFragment fragment = new AnimFragment();
        return fragment;
    }
    public AnimFragment() {}
    private IOpenVPNStatusCallback mCallback = new IOpenVPNStatusCallback.Stub() {
        @Override
        public void newStatus(String uuid, String state, String message, String level)
                throws RemoteException {
            Message msg = Message.obtain(mHandler, MSG_UPDATE_STATE, state);
            msg.sendToTarget();

        }

    };
    private boolean NoProcessFlag= false;
    @Override
    public boolean handleMessage(Message msg) {
        if(msg.what == MSG_UPDATE_STATE) {
            String str = msg.obj.toString();
            if("CONNECTED".equalsIgnoreCase(str)){
                if(mConnected)return true;
                mConnected = true;
                new Thread(new Runnable() {
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                animation.StopRoundAnimation();
                                imageAnim.setImageDrawable(animation.ConnectedAnimation);
                                animation.StartConnectedAnimation();
                                mStatus.setText(R.string.service_active);
                                new Thread(new Runnable() {
                                    public void run() {
                                        try {
                                            Thread.sleep(2000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("finish", true);
                                        getActivity().startActivity(intent);
                                        mConnected = false;
                                    }
                                }).start();
                            }
                        });
                    }
                }).start();
            }else if("NOPROCESS".equalsIgnoreCase(str)){
                if(NoProcessFlag){
                    clicked = false;
                    /*Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("finish", true);
                    getActivity().startActivity(intent);*/
                    /*Intent localIntent = new Intent(getActivity(), MainActivity.class);
                    localIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    startActivity(localIntent);*/
                    /*Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("stopanimation", true);
                    getActivity().startActivity(intent);*/
                    imageAnim.setImageDrawable(new SVGDrawable(new SVGParserRenderer(getActivity(), R.raw.color_full_no_active)));
                    imageAnim.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (clicked) return;
                            if (v.equals(imageAnim)) {
                                try {
                                    ma.mService.registerStatusCallback(mCallback);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    ma.prepareStartProfile(ma.START_PROFILE_EMBEDDED);
                                } catch (RemoteException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
                NoProcessFlag = true;
            }
        }
        return true;
    }

    public void RoundAnimationStart(){
        new Thread(new Runnable() {
            int index = 0;
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        clicked = true;
                        imageAnim.setImageDrawable(animation.RoundAnimation);
                        animation.StartRoundAnimation();
                    }
                });
            }
        }).start();
    }

    private  void CreateImageViewAnimationContent(ImageView iview){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        animation = new CandyAnimation(getActivity());
        //MainActivity ma = (MainActivity)inflater.inflate(R.layout.activity_main, container, false);
        //final MainActivity ma = MainActivity.getInstance();
       // MainActivity ma = (MainActivity)getActivity();
       // ma.af = this;
        mHandler = new Handler(this);
        mStatus = (TextView) rootView.findViewById(R.id.textView);
        imageAnim = (ImageView) rootView.findViewById(R.id.imageView);
        imageAnim.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (clicked) return;
                if (v.equals(imageAnim)) {
                    try {
                        ma.mService.registerStatusCallback(mCallback);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    try {
                        ma.prepareStartProfile(ma.START_PROFILE_EMBEDDED);
                    } catch (RemoteException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });
        imageAnim.setImageDrawable(new SVGDrawable(new SVGParserRenderer(getActivity(), R.raw.color_full_no_active)));
        /*rootView.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            @Override
            public void onSwipeLeft() {
                // Whatever
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("finish", true);
                startActivity(intent);
                mConnected = false;
            }
        });*/
        /*imageAnim.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            @Override
            public void onSwipeLeft() {
                // Whatever
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("finish", true);
                startActivity(intent);
                mConnected = false;
            }
        });*/
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
