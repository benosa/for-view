﻿package ssssssssssssssss.svgapplication;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import sssssssssssssssssss.R;
import com.trevorpage.tpsvg.SVGDrawable;
import com.trevorpage.tpsvg.SVGParserRenderer;

/**
 * Created by Nastenka on 24.03.2015.
 */
public class CandyAnimation{
    public AnimationDrawable RoundAnimation;
    public AnimationDrawable ConnectedAnimation;
    public AnimationDrawable StopAnimation;
    private Activity myActivity;




    private void InitAnimation(){
        //animation3 -анимация когда произойдет подключение

        ConnectedAnimation.addFrame((new SVGDrawable(new SVGParserRenderer(myActivity, R.raw.color_full_active17))), 100);
        ConnectedAnimation.addFrame((new SVGDrawable(new SVGParserRenderer(myActivity, R.raw.color_full_active18))), 100);
        ConnectedAnimation.addFrame((new SVGDrawable(new SVGParserRenderer(myActivity, R.raw.color_full_active19))), 100);
        ConnectedAnimation.addFrame((new SVGDrawable(new SVGParserRenderer(myActivity, R.raw.color_full_active20))), 100);
        ConnectedAnimation.setOneShot(true);
        ConnectedAnimation.setVisible(true,false);
        //d1.setScaleType(ImageView.ScaleType.CENTER_CROP);
        SVGParserRenderer image1 = new SVGParserRenderer(myActivity, R.raw.color_full_active01);
        SVGParserRenderer image2 = new SVGParserRenderer(myActivity, R.raw.color_full_active02);
        SVGParserRenderer image3 = new SVGParserRenderer(myActivity, R.raw.color_full_active03);
        SVGParserRenderer image4 = new SVGParserRenderer(myActivity, R.raw.color_full_active04);
        SVGParserRenderer image5 = new SVGParserRenderer(myActivity, R.raw.color_full_active05);
        SVGParserRenderer image6 = new SVGParserRenderer(myActivity, R.raw.color_full_active06);
        SVGParserRenderer image7 = new SVGParserRenderer(myActivity, R.raw.color_full_active07);
        SVGParserRenderer image8 = new SVGParserRenderer(myActivity, R.raw.color_full_active08);
        SVGParserRenderer image9 = new SVGParserRenderer(myActivity, R.raw.color_full_active09);
        SVGParserRenderer image10 = new SVGParserRenderer(myActivity, R.raw.color_full_active10);
        SVGParserRenderer image11 = new SVGParserRenderer(myActivity, R.raw.color_full_active11);
        SVGParserRenderer image12 = new SVGParserRenderer(myActivity, R.raw.color_full_active12);
        SVGParserRenderer image13 = new SVGParserRenderer(myActivity, R.raw.color_full_active13);
        SVGParserRenderer image14 = new SVGParserRenderer(myActivity, R.raw.color_full_active14);
        SVGParserRenderer image15 = new SVGParserRenderer(myActivity, R.raw.color_full_active15);
        SVGParserRenderer image16 = new SVGParserRenderer(myActivity, R.raw.color_full_active16);
        //SVGDrawable image1a = (new SVGDrawable(image1));
        //image1a.setAntiAlias(true);
        RoundAnimation.addFrame(new SVGDrawable(image1), 900);
        RoundAnimation.addFrame(new SVGDrawable(image2), 900);
        RoundAnimation.addFrame(new SVGDrawable(image3), 900);
        RoundAnimation.addFrame(new SVGDrawable(image4), 900);
        RoundAnimation.addFrame(new SVGDrawable(image5), 900);
        RoundAnimation.addFrame(new SVGDrawable(image6), 900);
        RoundAnimation.addFrame(new SVGDrawable(image7), 900);
        RoundAnimation.addFrame(new SVGDrawable(image8), 900);
        RoundAnimation.addFrame(new SVGDrawable(image9), 900);
        RoundAnimation.addFrame(new SVGDrawable(image10), 900);
        RoundAnimation.addFrame(new SVGDrawable(image11), 900);
        RoundAnimation.addFrame(new SVGDrawable(image12), 900);
        RoundAnimation.addFrame(new SVGDrawable(image13), 900);
        RoundAnimation.addFrame(new SVGDrawable(image14), 900);
        RoundAnimation.addFrame(new SVGDrawable(image15), 900);
        RoundAnimation.addFrame(new SVGDrawable(image16), 900);
        RoundAnimation.setOneShot(false);
        RoundAnimation.setVisible(true,false);


    }
    CandyAnimation(Activity actv){

        myActivity = actv;
        RoundAnimation = new AnimationDrawable();
        ConnectedAnimation = new AnimationDrawable();
        InitAnimation();
    }


    public void StartRoundAnimation(){
        //RoundAnimation.setVisible(true,true);
        RoundAnimation.start();
    }
    public void StopRoundAnimation(){
        RoundAnimation.stop();
        RoundAnimation.setVisible(false, false);
    }
    public void StartConnectedAnimation(){
        ConnectedAnimation.start();
    }
    public void StopConnectedAnimation(){
        ConnectedAnimation.stop();
    }
}
