package xxxxxxxxxxxxxxxxx.svgapplication;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by Igor Dronov id@bevolved.net on 11.02.2015.
 */
public abstract class SvgAnimationDrawable extends AnimationDrawable {

    /**
     * Handles the animation callback.
     */
    Handler mAnimationHandler;

    public SvgAnimationDrawable(AnimationDrawable aniDrawable) {
    /* Add each frame to our animation drawable */
        for (int i = 0; i < aniDrawable.getNumberOfFrames(); i++) {
            this.addFrame(aniDrawable.getFrame(i), aniDrawable.getDuration(i));
        }
    }

    @Override
    public void start() {
        super.start();
    /*
     * Call super.start() to call the base class start animation method.
     * Then add a handler to call onAnimationFinish() when the total
     * duration for the animation has passed
     */
        //while(true) {
                /*mAnimationHandler = new Handler();
                mAnimationHandler.postDelayed(new Runnable() {
                    public void run() {
                        onAnimationFinish();
                    }
                }, getTotalDuration());*/
            //}
        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do Work
                        onAnimationFinish();
                        handler.removeCallbacks(this);
                        Looper.myLooper().quit();
                    }
                }, getTotalDuration());

                Looper.loop();
            }
        };
        thread.start();
    }


    /**
     * Gets the total duration of all frames.
     *
     * @return The total duration.
     */
    public int getTotalDuration() {

        int iDuration = 0;

        for (int i = 0; i < this.getNumberOfFrames(); i++) {
            iDuration += this.getDuration(i);
        }

        return iDuration;
    }

    /**
     * Called when the animation finishes.
     */
    abstract void onAnimationFinish();
}