package xxxxxxxxxxxxxxxxxx;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.flurry.android.FlurryAgent;
import xxxxxxxxxxxxxxx.api.IOpenVPNAPIService;
import xxxxxxxxxxxxxxx.core.OpenVPNService;
import xxxxxxxxxxxxxxx.core.VpnStatus;
import xxxxxxxxxxxxxxx.svgapplication.AnimFragment;

public class MainActivity extends ActionBarActivity implements AnimFragment.OnFragmentInteractionListener {
    public static final int START_PROFILE_EMBEDDED = 2;
    private static final int ICS_OPENVPN_PERMISSION = 7;
    public IOpenVPNAPIService mService=null;
    public static MainActivity instance;
    private boolean mCmfixed=false;
    //public AnimFragment af = null;
    private AnimFragment AnimationFragment;
    static boolean active = false;
    public static MainActivity getInstance()
    {
        return instance;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public boolean isRunningActivity(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String appKey = "xxxxxxxxxxxxxxxxxxxxxxxxx";
        InitAppodial.createAds(this, appKey);
        /*ActivityManager activityManager = (ActivityManager) this.getSystemService( ACTIVITY_SERVICE );
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for(int i = 0; i < procInfos.size(); i++)
        {
            if(procInfos.get(i).processName.equals("OpenVPNService"))
            {
                finish();
                //Toast.makeText(getApplicationContext(), "Browser is running", Toast.LENGTH_LONG).show();
            }
        }*/
       if (getIntent().getBooleanExtra("finish", false)) finish();
       if( isMyServiceRunning(OpenVPNService.class) || active){
           finish();
       }
                  //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        instance = this;

           if (savedInstanceState == null) {
               AnimationFragment = new AnimFragment();
               getSupportFragmentManager().beginTransaction().add(R.id.container, AnimationFragment).commit();
               AnimationFragment.ma = this;
           }

           //String appKey = "xxxxxxxxxxxxxxxxxxxxxxxxx";
           //Appodeal.initialize(this, appKey);




           FlurryAgent.init(this, "xxxxxxxxxxxxxxxxxxxxxx");
           FlurryAgent.setLogEnabled(false);
    }

    @Override
    public void onStart() {
        if( isMyServiceRunning(OpenVPNService.class) || active){
            finish();
        }
            super.onStart();
            bindService();
            active = true;
            FlurryAgent.onStartSession(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        unbindService();
        active = false;
        FlurryAgent.onEndSession(this);
    }

    private void bindService() {

        Intent icsopenvpnService = new Intent(IOpenVPNAPIService.class.getName());
        icsopenvpnService.setPackage("com.magicbunny.candylink");

        bindService(icsopenvpnService, mConnection, Context.BIND_AUTO_CREATE);
    }
    private void unbindService() {
        unbindService(mConnection);
    }
    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.

            mService = IOpenVPNAPIService.Stub.asInterface(service);

            try {
                // Request permission to use the API
                Intent i = mService.prepare(getPackageName());
                if (i!=null) {
                    startActivityForResult(i, ICS_OPENVPN_PERMISSION);
                } else {
                    onActivityResult(ICS_OPENVPN_PERMISSION, Activity.RESULT_OK,null);
                }

            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        try {
            prepareStartProfile(START_PROFILE_EMBEDDED);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void prepareStartProfile(int requestCode) throws RemoteException {
        Intent requestpermission = mService.prepareVPNService();
        if(requestpermission == null) {
            onActivityResult(requestCode, Activity.RESULT_OK, null);
        } else {
            // Have to call an external Activity since services cannot used onActivityResult
            startActivityForResult(requestpermission, requestCode);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            //AnimationFragment.RoundAnimationStart();
            if(requestCode==START_PROFILE_EMBEDDED)
                startEmbeddedProfile();

                //RoundAnimationStart();
        }
    };
    private boolean isCyanogenMod(PackageManager pm) {
        boolean isCyanogenMod = false;
        String version = System.getProperty("os.version");
        BufferedReader reader = null;

        try {
            if (version.contains("cyanogenmod") || pm.hasSystemFeature("com.cyanogenmod.android")) {
                isCyanogenMod = true;
            }
            else {
                // This does not require root
                reader = new BufferedReader(new FileReader("/proc/version"), 256);
                version = reader.readLine();

                if (version.contains("cyanogenmod")) {
                    isCyanogenMod = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(reader != null) {
                try { reader.close(); } catch (IOException e) { }
            }
        }

        return isCyanogenMod;
    }
    private void startEmbeddedProfile()
    {
        AnimationFragment.RoundAnimationStart();
        try {

            InputStream conf = getAssets().open("hd.conf");
            InputStreamReader isr = new InputStreamReader(conf);
            BufferedReader br = new BufferedReader(isr);
            String config="";
            String line;
            while(true) {
                line = br.readLine();
                if(line == null)
                    break;
                config += line + "\n";
            }
            br.readLine();

            //			mService.addVPNProfile("test", config);
            String version = System.getProperty("os.version");
            String androidOS = Build.HOST;
            PackageManager pm = getPackageManager();
            //boolean v = isCyanogenMod(pm);
            if(isCyanogenMod(pm)){
                //checkTUNowner();
                execeuteSUcmd("chown system /dev/tun");
            }
            mService.startVPN(config);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
   /* private String checkTUNowner(){
        String owner = "";
       // String command = "stat -c %U file";
        List<String> command = new ArrayList<String>();
        command.add("stat");
        command.add("-c");
        command.add("%U");
        command.add("/dev/tun");
        ProcessBuilder pb = new ProcessBuilder(command);
        //String stdout;// = commandExecutor.getStandardOutputFromCommand();
        //SystemCommandExecutor commandExecutor = new SystemCommandExecutor(command);
        try {
            Process p = pb.start();
            //int ret = p.waitFor();
            InputStream stdin = p.getInputStream();
            InputStreamReader isr = new InputStreamReader(stdin);
            BufferedReader br = new BufferedReader(isr);
            String line;
            String cb="";
            while ((line = br.readLine()) != null) {

                cb += line;
            }
            InputStreamReader esr = new InputStreamReader(p.getErrorStream());
            BufferedReader errorReader = new BufferedReader(esr);
            p.waitFor();
        } catch (InterruptedException e) {
            VpnStatus.logException("SU command", e);

        } catch (IOException e) {
            VpnStatus.logException("SU command", e);
        }

        return owner;
    }*/
    private void execeuteSUcmd(String command) {
        ProcessBuilder pb = new ProcessBuilder("su","-c",command);
        try {
            Process p = pb.start();
            int ret = p.waitFor();
            if(ret ==0)
                mCmfixed=true;
        } catch (InterruptedException e) {
            VpnStatus.logException("SU command", e);

        } catch (IOException e) {
            VpnStatus.logException("SU command", e);
        }
    }

}
