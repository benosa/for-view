/*
 * Copyright (c) 2012-2014 Arne Schwabe
 * Distributed under the GNU GPL v2 with additional terms. For full terms see the file doc/LICENSE.txt
 */

package com.magicbunny.candylink.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import com.magicbunny.candylink.R;
//import me.candylink.ovpn.fragments.LogFragment;

/**
 * Created by arne on 13.10.13.
 */
public class LogWindow extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_window);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            /*getFragmentManager().beginTransaction()
                    .add(R.id.container, new LogFragment())
                    .commit();*/
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
