/*
	Инициализация календаря
*/
$(function() {
    		$.ajaxSetup({
        		headers: {
            		'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        		}
    		});
    		
	});
$(document).ready(function() {
	

	var $datepicker = $("#datepicker");
	var dateToday = new Date();
	$datepicker.datepicker({
		firstDay : "1",
		minDate: dateToday,
		dateFormat: 'yy-mm-dd',
		"onSelect": function (dateText, inst) {
			$("#dateFrom").val(dateText);
		}
	});
	
	Date.prototype.format = function (mask, utc) {
	    return dateFormat(this, mask, utc);
	};
	
    $( "#resizable" ).resizable({
      handles: "se"
    });
    
    var DateNow = new Date();
    var Hours = DateNow.getHours();
    var Minutes = Hours*60+DateNow.getMinutes();

    $( "#slider" ).slider({
        range: "min",
        min: 0,
        max: 1435,
        step: 5,
        value: Minutes,
        /*values: [ 600, 1200 ],*/
        slide: function( event, ui ) {
            var hours1 = Math.floor(ui.value / 60);
            var minutes1 = ui.value - (hours1 * 60);
            if(hours1.length < 10) hours1= '0' + hours;
            if(minutes1.length < 10) minutes1 = '0' + minutes;
            if(minutes1 == 0) minutes1 = '00';
            //jQuery('#amount').val(hours1+':'+minutes1/*+' - '+hours2+':'+minutes2*/ );
            $("#amount").text(getTimeFromValue($( "#slider" ).slider( "option", "value" )));
            $("#time").val($( "#slider" ).slider( "option", "value" )); 
        }
    });
    //$("#amount").val(getTimeFromValue($( "#slider" ).slider( "option", "value" )));
    $("#amount").text(getTimeFromValue($( "#slider" ).slider( "option", "value" )));
    $("#time").val($( "#slider" ).slider( "option", "value" ));    
});

function getTimeFromValue(value){
            var hours1 = Math.floor(value / 60);
            var minutes1 = value - (hours1 * 60);
            if(hours1.length < 10) hours1= '0' + hours;
            if(minutes1.length < 10) minutes1 = '0' + minutes;
            if(minutes1 == 0) minutes1 = '00';
    return hours1+':'+minutes1;
}

function parseTime(value) {
	// parse time HH:MM into second
	var time = value.split(":");
	//return { hh: time[0], mm: time[1] };
	return parseInt(time[0])*60 + parseInt(time[1]);
}

/*
*	Delete Post
*/

 function deletePost(val){
        $.ajax({  
            type: 'POST',  
            url: 'http://social.bevolved.net/deletePost', 
            data: { postid: $(val).data('id'), type:$(val).data('type')},
            success: function(response) {
                if(response == 'ok'){
                	$("ul").remove('[data-id='+ $(val).data('id') +']');
                }
            }
        });
    }

/*
*AJAX Export CSV from php
*/
function exportCSV(type){
        $.ajax({  
            //type: 'POST',  
            url: 'http://social.bevolved.net/exportCSV/'+type,
            dataType: 'html', 
            //data: { postid: $(val).data('id')},
            success: function(response) {
                if(response == false)return;
                window.open('exportCSV','Download File', 'width=300, height=250');
            },
            error:function(response) {
                return;
            }
        });
    }
