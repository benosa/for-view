@extends('layouts.base')

@section('content')
	{{ HTML::style('css/register.css'); }}
	{{ Form::open(array('url' => 'reset-password', 'class' => 'form-register', 'role' => 'form')) }}
		<h2 class="form-register-heading">Enter new password</h2>
		{{ Form::hidden('token', $token) }}
		{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email address', 'class' => 'form-control', 'required', 'autofocus')) }}
		{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control password', 'required')) }}
		{{ Form::password('password_confirmation', array('placeholder' => 'Password confirmation', 'class' => 'form-control password-confirmation', 'required')) }}

		{{ Form::submit('Reset Password', array('class' => 'btn btn-lg btn-primary btn-block')) }}
	{{ Form::close() }}
@stop
