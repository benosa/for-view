@extends('layouts.base')

@section('content')
	{{ HTML::style('css/remind.css'); }}
	{{ Form::open(array('url' => 'remind-password', 'class' => 'form-remind', 'role' => 'form')) }}
		<h2 class="form-remind-heading">Enter email address</h2>
		{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email address', 'class' => 'form-control', 'required', 'autofocus')) }}
		{{ Form::submit('Remind Password', array('class' => 'btn btn-lg btn-primary btn-block')) }}
	{{ Form::close() }}
@stop
