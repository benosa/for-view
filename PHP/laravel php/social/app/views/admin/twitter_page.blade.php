@extends('layouts.base')

@section('content')
	<!--div id="content"-->
	    <div class="layout">
		    <div class="wrap">
		      <div id="content">
    			<div style="padding-top:30px; width:100%;">
        		    <div class="table vmiddle" style="width:100%;height:100px;">
	        		<div class="tr">
		        	    <div class="td" style="height:133px;width: 180px;float:left;border-right:1px solid grey;margin-right:80px;">
		        		<img src="img/twee-bird-rock128.png" />
		        	    </div>
                		    <div class="td" style="height:133px;width: 500px;float:left;margin-top:30px;font-size:4em;">Scheduler</div>
                		    <div style="margin-bottom:40px;margin-left:280px;float:left;"><img src="img/question.png" width="100px" /></div>
	        		</div>
	        		<div class="tr" style="width:100%;overflow:hidden;">
                    		    <div style="float:right;"><a href="{{URL::to('exportCSV/twitter')}}" class="btn btn-warning btn-lg" role="button">EXPORT TWEETS</a></div>
                    		</div>
        		    </div>
        		    <div style="margin-top:20px;">
        		    	<!--ul class="list-group">
  				     <li class="list-group-item list-group-item-warning">TODAY</li>
  				</ul-->
				<table class="table table-hover" width="100%">  
    				    <thead>  
        				<tr>  
        				    <th>Student-ID</th>  
        				    <th>First Name</th>  
        					<th>Last Name</th>  
        				    <th>Grade</th>  
        				</tr>  
    				    </thead>  
    				    <tbody>  
        				<tr>  
        				    <td>001</td>  
        				    <td>Rammohan </td>  
        				    <td>Reddy</td>  
        				    <td>A+</td>  
        				</tr>  
				    </tbody>
				</table>
        		    </div>
    			</div>
		    </div>
		</div>
	    </div>
	<!--/div-->
@stop
