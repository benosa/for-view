@extends('layouts.base')

@section('content')
	{{ HTML::style('css/register.css'); }}
	{{ Form::open(array('url' => 'register/success', 'class' => 'form-register', 'role' => 'form')) }}
		<div class="progress progress-striped active">
			<div class="progress-bar"  role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }}%">
				<span class="sr-only">{{ $progress }}% Complete</span>
			</div>
		</div>
		<h2 class="form-register-heading">Registration completed</h2>
		{{ Form::submit('Continue', array('class' => 'btn btn-lg btn-primary btn-block')) }}
	{{ Form::close() }}
@stop
