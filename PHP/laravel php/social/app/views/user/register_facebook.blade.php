@extends('layouts.base')

@section('content')
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1429997790576690',
				status     : true,
				cookie     : true,
				xfbml      : true
			});

			FB.Event.subscribe('auth.authResponseChange', function(response) {
				if (response.status === 'connected') {
					var userID = response.authResponse.userID;
					var accessToken = response.authResponse.accessToken;
					FB.api('/me/permissions', function(response) {
						if (!response.data[0].publish_actions || response.data[0].publish_actions != 1) {
							$('#facebook_text')[0].innerHTML = "You didn't grant required permissions";
						} else {
							$('#facebook_id')[0].value = userID;
							$('#facebook_token')[0].value = accessToken;
							$('#submit').prop("disabled", false);
							$('#facebook_text')[0].innerHTML = "You are successfully loggged in";
							$('.fb-login-button').css('display', 'none');
							$('#progress1').css('display', 'none');
							$('#progress2').css('display', 'block');
						}
				    });
					console.log(response);
				} else if (response.status === 'not_authorized') {
					FB.login();
				} else {
					FB.login();
				}
			});
		};

		(function(d){
			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			ref.parentNode.insertBefore(js, ref);
		}(document));

	</script>

	{{ HTML::style('css/register.css'); }}
	{{ Form::open(array('url' => 'register/facebook', 'class' => 'form-register', 'role' => 'form')) }}
		<div class="progress progress-striped active" id="progress1">
			<div class="progress-bar"  role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }}%">
				<span class="sr-only">{{ $progress }}% Complete</span>
			</div>
		</div>
		<div class="progress progress-striped active" id="progress2" style="display:none">
			<div class="progress-bar"  role="progressbar" aria-valuenow="{{ $progress2 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress2 }}%">
				<span class="sr-only">{{ $progress2 }}% Complete</span>
			</div>
		</div>
		<h2 id="facebook_text" class="form-register-heading">Please login to your Facebook account</h2>
		<div class="fb-login-button" data-size="xlarge" data-width="123px" data-height="39px" data-scope="publish_actions"></div>
		{{ Form::hidden('id', '', array('id' => 'facebook_id')) }}
		{{ Form::hidden('token', '', array('id' => 'facebook_token')) }}
		{{ Form::submit('Continue', array('class' => 'btn btn-lg btn-primary btn-block', 'disabled', 'id' => 'submit', 'autocomplete' => 'off')) }}
	{{ Form::close() }}
@stop
