@extends('layouts.base')

@section('content')
	{{ HTML::style('css/register.css'); }}
	{{ Form::open(array('url' => 'register/social', 'class' => 'form-register', 'role' => 'form')) }}
		<div class="progress progress-striped active">
			<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
				<span class="sr-only">0% Complete</span>
			</div>
		</div>
		<h2 class="form-register-heading">Please select social networks</h2>
		<div class="checkbox">
			<label>{{ Form::checkbox('facebook', '1') }} Facebook</label>
		</div>
		<div class="checkbox">
			<label>{{ Form::checkbox('twitter', '1') }} Twitter</label>
		</div>
		<div class="checkbox">
			<label>{{ Form::checkbox('gplus', '1') }} Google+</label>
		</div>
		<div class="checkbox">
			<label>{{ Form::checkbox('linkedin', '1') }} LinkedIn</label>
		</div>
		{{ Form::submit('Continue', array('class' => 'btn btn-lg btn-primary btn-block')) }}
	{{ Form::close() }}
@stop
