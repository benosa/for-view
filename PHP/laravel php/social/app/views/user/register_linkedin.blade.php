@extends('layouts.base')

@section('content')
	{{ HTML::style('css/register.css'); }}
	<div class="form-register">
		<div class="progress progress-striped active" id="progress1">
			<div class="progress-bar"  role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }}%">
				<span class="sr-only">{{ $progress }}% Complete</span>
			</div>
		</div>
		<h2 id="linkedein_text" class="form-register-heading">{{ $text }}</h2>
		@if ($button == 'linkedin')
			<a href="{{ $url }}" class="btn-block btn-linkedin"></a>
		@else
			<a href="{{ $url }}" class="btn btn-lg btn-primary btn-block">Continue</a>
		@endif
	</div>
@stop
