@extends('layouts.base')

@section('content')
	{{ HTML::style('css/register.css'); }}
	{{ Form::open(array('url' => 'register', 'class' => 'form-register', 'role' => 'form')) }}
		<h2 class="form-register-heading">Please register</h2>
		{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email address', 'class' => 'form-control', 'required', 'autofocus')) }}
		{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control password', 'required')) }}
		{{ Form::password('password_confirmation', array('placeholder' => 'Password confirmation', 'class' => 'form-control password-confirmation', 'required')) }}

		{{ Form::submit('Register', array('class' => 'btn btn-lg btn-primary btn-block')) }}
	{{ Form::close() }}
@stop
