@extends('layouts.base')

@section('content')
	{{ HTML::style('css/signin.css'); }}
	{{ Form::open(array('url' => 'login', 'class' => 'form-signin', 'role' => 'form')) }}
		<h2 class="form-signin-heading">Please sign in</h2>
		{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email address', 'class' => 'form-control', 'required', 'autofocus')) }}
		{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'required')) }}

		{{ Form::submit('Sign in', array('class' => 'btn btn-lg btn-primary btn-block')) }}
		<a class="btn btn-link" role="button" href="{{ URL::to('register') }}">Register</a>
		<a class="btn btn-link pull-right" role="button" href="{{ URL::to('remind-password') }}">Forgot Password</a>
	{{ Form::close() }}
@stop

