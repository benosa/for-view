<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Social</title>
        {{ HTML::style('css/bootstrap.css'); }}
        {{ HTML::style('css/style.css'); }}
	@yield('styles')
        {{ HTML::style('css/twitterpage.css'); }}
        <script src="js/jquery-ui-upload/js/vendor/jquery.ui.widget.js"></script>
        <script src="js/jquery-ui-upload/js/jquery.iframe-transport.js"></script>
        <script src="js/jquery-ui-upload/js/jquery.fileupload.js"></script>
        <link href="js/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" rel="stylesheet">
        <!--[if lt IE 9]>
            {{ HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'); }}
            {{ HTML::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'); }}
        <![endif]-->
        {{ HTML::script('js/jquery-1.11.0.js'); }}
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript" src="js/data/jquery.timers.js"></script>
        {{ HTML::script('js/bootstrap.js'); }}
        {{ HTML::script('js/script.js'); }}
    </head>
    <body>
        @yield('header')
        <div class="container">
            @yield('messages')
            @yield('content')
        </div>
        @yield('footer')
    </body>
</html>