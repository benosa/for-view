@extends('layouts.base')

@section('header')
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">Social</a>
			</div>
			@if (isset($user))
				@include('header.loggedin')
			@else 
				@include('header.loginform')
			@endif
		</div>
		<?php /*var_dump(get_class_methods(get_class($user)));*/ ?>
	</div>
@stop
