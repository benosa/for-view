<div class="navbar-collapse collapse">
	{{ Form::open(array('url' => 'login', 'class' => 'navbar-form navbar-right', 'role' => 'form')) }}
		<div class="form-group">
			{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email Address', 'class' => 'form-control')) }}
		</div>
		<div class="form-group">
			{{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control')) }}
		</div>
		{{ Form::submit('Sign in', array('class' => 'btn btn-success')) }}
		<a class="btn btn-link" role="button" href="{{ URL::to('register') }}">Register</a>
		<a class="btn btn-link pull-right" role="button" href="{{ URL::to('remind-password') }}">Forgot Password</a>
	{{ Form::close() }}
</div><!--/.navbar-collapse -->
