<ul class="nav navbar-nav navbar-collapse collapse">
<?php
    $twitter = $user->twitterAccount();
    $facebook = $user->facebookAccount();
    $linkedin = $user->linkedinAccount();
    //var_dump(get_class_methods(get_class($user)));
    $admin = $user->isAdmin();
     if(isset($linkedin)){?>        
	    <li><a class="btn btn-link" role="button" href="{{ URL::to('linkedin') }}">Linkedin</a></li>
     <?php
     }
     if(isset($facebook)){?>
	    <li><a class="btn btn-link" role="button" href="{{ URL::to('facebook') }}">Facebook</a></li>
     <?php
     }
     if(isset($twitter)){?>
	    <li><a class="btn btn-link" role="button" href="{{ URL::to('twitter') }}">Twitter</a></li>

     <?php
     }
     ?>
     <li><a class="btn btn-link" role="button" href="{{ URL::to('stats') }}">Stats</a></li>
     <?php
     if(isset($admin) && $admin == true){?>
	<li class="dropdown">
	    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	      Admin <span class="caret"></span>
	    </a>
	    <ul class="dropdown-menu" role="menu">
	        <li><a href="{{ URL::to('search_users') }}">Search users</a></li>
	        <li class="divider"></li>
	        <li><a href="#">Manage users</a></li>
	    </ul>
	</li>
     <?php
     }
?>
    
</ul>
<ul class="nav navbar-nav navbar-right navbar-collapse collapse">
	<li><a class="btn btn-link" role="button" href="{{ URL::to('profile') }}">{{ $user->email }}</a></li>
	<li><a class="btn btn-link" role="button" href="{{ URL::to('logout') }}">Logout</a></li>
</ul>
