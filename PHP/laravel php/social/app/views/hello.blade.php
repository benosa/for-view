@extends('layouts.base')

@section('content')
	<div id="content">
	    <?php /*$user = Auth::user();*/ ?>
	    @if(isset($user))
		@include('content.content') 
	    @else
		<h1>You have arrived.</h1>
	    @endif
	</div>
@stop
