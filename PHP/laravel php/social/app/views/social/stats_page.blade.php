@extends('layouts.base')

@section('content')
	<!--div id="content"-->
	    <div class="layout">
		<div class="wrap">
		    <div id="content">
    			<div style="padding-top:30px; width:100%;">
        		    <div class="table vmiddle" style="width:100%;height:100px;">
	        		<div class="tr">
		        	    <div class="td" style="height:133px;width: 180px;float:left;border-right:1px solid grey;margin-right:80px;">
		        		<img src="{{URL::to('/')}}/img/stats.png" width="160px;" />
		        	    </div>
                		    <div class="td" style="height:133px;width: 500px;float:left;margin-top:30px;font-size:4em;">Statistics</div>
                		    <div style="margin-bottom:40px;margin-left:280px;float:left;"><img src="/img/question.png" width="100px" /></div>
	        		</div>
					</div>
    			</div>

    				<div class="pull-left"><a href="{{URL::to('stats/30days')}}" class="btn btn-primary" role="button">LAST 30 DAYS</a></div>
					<div class="pull-left"><a href="{{URL::to('stats/3month')}}" class="btn btn-primary" role="button">LAST 3 MONTH</a></div>
					<div class="pull-left"><a href="{{URL::to('stats/year')}}" class="btn btn-primary" role="button">LAST YEAR</a></div>

					<div class="pull-left" id="visualization" style="width: 1200px; height: 600px;"></div>

					<script type="text/javascript" src="http://www.google.com/jsapi"></script>
				    <script type="text/javascript">
				      google.load('visualization', '1.1', {packages: ['corechart', 'imagelinechart']});
				    </script>
				    <script type="text/javascript">
				    function drawVisualization() {
				        var data = new google.visualization.DataTable();
				        data.addColumn('string', 'Date');
				        data.addColumn('number', 'LinkedIn');
				        data.addColumn('number', 'Facebook');
						data.addColumn('number', 'Twitter');
				        data.addRows([
				        			<?php 
										foreach ($statictscs as $stat) {
									//echo "[new Date('{$stat['date']}'), {$stat['linkedin']}, {$stat['facebook']}, {$stat['twitter']}],";
											echo "[('{$stat['date']}'), {$stat['linkedin']}, {$stat['facebook']}, {$stat['twitter']}],";
										}	
									?>
						]);

				        

						var options = {
						   title: 'Posts Statistics',
						   curveType: 'none',
						   lineWidth: '2',
						   fontName: 'Courier New',
						   hAxis: {showTextEvery: 3},
						   vAxis: {format: '0'},
						   	legend: 'none',
						    focusTarget: "category",
						    selectionMode: 'multiple',
						    aggregationTarget: 'category',
						    isStacked: true,
						   };
						
				        var chart = new google.visualization.LineChart(document.getElementById('visualization'));
				        chart.draw(data, options);
				      }
				      google.setOnLoadCallback(drawVisualization);
				    </script>
					 

				
				</div>
		</div>
	    </div>
	<!--/div-->
@stop
