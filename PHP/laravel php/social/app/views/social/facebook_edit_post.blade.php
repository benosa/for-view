@extends('layouts.base')

@section('content')
<script type="text/javascript">
    function save(){
	   var message = $(".message").val();
	   alert(message);
	//var 
    }
    function changeColorTA(val1,val2){
	       if($(val1).text() == '') {
                    $(val2).css('border' , '1px solid red');
	       }
    }

    function sendData(){
        $.ajax({  
            type: 'POST',  
            url: 'http://social.bevolved.net/postEditFPost', 
            data: { post: $(".message").val(), dateFrom: $("#dateFrom").val(), id: $("#id_post").val(), filename: $("#filename").val(), time: $("#amount").text() },
            success: function(response) {
                if(response == 'ok')window.location.href = "{{URL::to('/')}}/facebook"
            }
        });
    }

    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }
    $(function() {
        $(':button').click(function(){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: 'http://social.bevolved.net/postImage',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                success: function(response){
                    $('#filename').val(response);
                },
                //Ajax events
                /*beforeSend: beforeSendHandler,
                success: completeHandler,
                error: errorHandler,*/
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
        });
        var vvv = parseTime("{{$user->FacebookPost->find($id)->time}}");
        var s = $('#slider').slider();
        s.slider('value',vvv);
        $("#amount").text(getTimeFromValue($( "#slider" ).slider( "option", "value" )));
        $("#time").val($( "#slider" ).slider( "option", "value" ));
        var datatemp = ("{{$user->FacebookPost->find($id)->date_from}}").split('-');
        $("#datepicker").datepicker('setDate', "{{$user->FacebookPost->find($id)->date_from}}");
    });
</script>
        <input type="hidden" id="id_post" value="{{$id}}">
	    <div class="layout">
		<div class="wrap">
		    <div id="content">
    			<div style="padding-top:30px; width:100%;">
        		    <div class="table vmiddle" style="width:100%;height:100px;">
	        		<div class="tr">
		        	    <div class="td" style="height:133px;width: 180px;float:left;border-right:1px solid grey;margin-right:80px;">
		        		<img src="{{URL::to('/')}}/img/facebook_256.png" width="100px;" />
		        	    </div>
                		    <div class="td" style="height:133px;width: 500px;float:left;margin-top:30px;font-size:4em;">Create a post</div>
	        		</div>
	        		<div class="tr">
	        		    COMPOSE
	        		</div>
            			<div class="tr" id="message">
                		    <textarea class="message" id="resizable" rows="15" cols="130">{{$user->FacebookPost->find($id)->post}}</textarea>
            			</div>
            			<div class="tr">
	        		    UPLOAD AN IMAGE (OPTIONAL)
	        		</div>
	        		
            			<div class="tr" style="width:795px; border: 1px solid grey; padding:10px;">
                                <form enctype="multipart/form-data">
                                    {{ Form::token() }}
                                    <!--input name="file" type="file" /-->
                                    <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                            Choose File...
                                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                        </a>
                                        &nbsp;
                                        <span class='label label-info' id="upload-file-info"></span>
                                    </div>
                                    <input style="margin-top:10px;" type="button" class="btn btn-warning" value="Upload"/>
                                </form>
                                    <progress  style="margin-top:10px;"></progress>
            			</div>
            			<input type="hidden" id="filename" value="{{$user->FacebookPost->find($id)->image}}" />
            			<div class="tr">
            			    <div style="margin-top:30px;">
	        			<div style="float:left;"><a href="#" onclick="changeColorTA($('.message'),$('#message'));sendData();return false;" class="btn btn-primary btn-lg" role="button">SAVE A POST</a></div>
	        			<div style="float:left;margin-left:30px;padding-top:3px;"><a href="{{ URL::previous() }}">BACK</a></div>
	        		    </div>
	        		</div>
        		    </div>
    			</div>
		    </div>
		    <div id="menu" style="margin-top:50px;">
			<div style="margin-bottom:40px;margin-left:180px;"><img src="{{URL::to('/')}}/img/question.png" width="100px" /></div>
			<div>SCHEDULE DATE</div>
			<div class="well" >
    			    <div id="datepicker"></div>
    			    <!--div id="dateText"></div-->
    			    <input type="hidden" id="dateFrom" value="{{$user->FacebookPost->find($id)->date_from}}" />
    			</div>
    			<div>SCHEDULE TIME</div>
    			<div class="well" >
    			    <div id="slider" style="width: 250px;"></div>
    			    <p>
        			<div class="center" style="width:250px;">
            			    <div id="amount" class="center" style="border:0; color:#f6931f; font-weight:bold;width:30px;"></div>
                            <input type="hidden" id="time" value="{{$user->FacebookPost->find($id)->time}}" />
            			    <div class="clear"></div>
        			</div>
    			    </p>
    			</div>
		    </div>
		</div>
	    </div>
	<!--/div-->
@stop
