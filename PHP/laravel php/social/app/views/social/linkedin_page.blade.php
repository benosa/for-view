@extends('layouts.base')

@section('content')
  <!--div id="content"-->
      <div class="layout">
        <div class="wrap">
          <div id="content">
             <div style="padding-top:30px; width:100%;">
                <div class="table vmiddle" style="width:100%;height:100px;">
                   <div class="tr">
                       <div class="td" style="height:133px;width: 180px;float:left;border-right:1px solid grey;margin-right:80px;">
                          <img src="{{URL::to('/')}}/img/LinkedIn_logo.png" width="100px;" />
                       </div>
                       <div class="td" style="height:133px;width: 500px;float:left;margin-top:30px;font-size:4em;">Scheduler</div>
                       <div style="margin-bottom:40px;margin-left:280px;float:left;"><img src="img/question.png" width="100px" /></div>
                   </div>
                   <div class="tr" style="width:100%;overflow:hidden;">
                       <div style="float:left;"><a href="{{URL::to('create_lpost')}}" class="btn btn-primary btn-lg" role="button">CREATE A POST</a></div>
                      <!-- <div style="float:right;"><a href="{{URL::to('create_lpost')}}" onclick="exportCSV('linkedin');return false;" class="btn btn-warning btn-lg" role="button">EXPORT POSTS</a></div>
                   </div> -->
                       <div style="float:right;"><a href="{{URL::to('exportCSV/linkedin')}}" class="btn btn-warning btn-lg" role="button">EXPORT POSTS</a></div>
                       </div>
                </div>
                <div style="margin-top:20px;">
                  <ul class="list-group">
                     <li class="list-group-item list-group-item-warning">TODAY</li>
                  </ul>
                  @foreach($linkedinpost as $post)
                   <ul class="list-group" data-id="{{$post->id}}">
                      <li class="list-group-item list-group-item-info">
                         <div style="width:100%;overflow:hidden;">
                            <div style="width:91%;float:left;">
                               <div style="font-size:12pt;color:black;font-weight: bold;">{{$post->time}}/{{$post->date_from}}/{{$post->date_to}}</div>
                               <div>{{$post->post}}</div>
                            </div>
                            <div>
                               <div style="float:left;"><a href="{{URL::to('edit_lpost/'.$post->id)}}"><img src="img/edit.png" width="35px" /></a></div>
                               <div style="float:left;margin-left:20px;"><a href="#" onclick="deletePost(this); return false;" data-id="{{$post->id}}" data-type="linkedin"><img src="img/close.png" width="35px" /></a></div>
                            </div>
                           </div>
                      </li>
                     </ul>
                  @endforeach
                  <ul class="list-group">
                     <li class="list-group-item list-group-item-warning">{{date('j F Y', strtotime(date('Y-m-d', strtotime(' +1 day'))))}}</li>
                  </ul>
                  @foreach($linkedinpost_1day as $post)
                   <ul class="list-group" data-id="{{$post->id}}">
                      <li class="list-group-item list-group-item-info">
                          <div style="width:100%;overflow:hidden;">
                            <div style="width:91%;float:left;">
                               <div style="font-size:12pt;color:black;font-weight: bold;">{{$post->time}}/{{$post->date_from}}/{{$post->date_to}}</div>
                               <div>{{$post->post}}</div>
                            </div>
                            <div>
                                 <div style="float:left;"><a href="{{URL::to('edit_lpost/'.$post->id)}}"><img src="img/edit.png" width="35px" /></a></div>
                                 <div style="float:left;margin-left:20px;"><a href="#" onclick="deletePost(this); return false;" data-id="{{$post->id}}"  data-type="linkedin"><img src="img/close.png" width="35px" /></a></div>
                            </div>
                         </div>
                      </li>
                   </ul>
                  @endforeach
              <ul class="list-group">
                <li class="list-group-item list-group-item-warning">{{date('j F Y', strtotime(date('Y-m-d', strtotime(' +2 day'))))}}</li>
              </ul>
              @foreach($linkedinpost_2day as $post)
                <ul class="list-group" data-id="{{$post->id}}">
                  <li class="list-group-item list-group-item-info">
                    <div style="width:100%;overflow:hidden;">
                      <div style="width:91%;float:left;">
                        <div style="font-size:12pt;color:black;font-weight: bold;">{{$post->time}}/{{$post->date_from}}/{{$post->date_to}}</div>
                        <div>{{$post->post}}</div>
                      </div>
                      <div>
                        <div style="float:left;"><a href="{{URL::to('edit_lpost/'.$post->id)}}"><img src="img/edit.png" width="35px" /></a></div>
                        <div style="float:left;margin-left:20px;"><a href="#" onclick="deletePost(this); return false;" data-id="{{$post->id}}"  data-type="linkedin"><img src="img/close.png" width="35px" /></a></div>
                      </div>
                    </div>
                  </li>
                </ul>
              @endforeach
              <ul class="list-group">
                <li class="list-group-item list-group-item-warning">{{date('j F Y', strtotime(date('Y-m-d', strtotime(' +3 day'))))}}</li>
              </ul>
              @foreach($linkedinpost_3day as $post)
                <ul class="list-group" data-id="{{$post->id}}">
                  <li class="list-group-item list-group-item-info">
                    <div style="width:100%;overflow:hidden;">
                      <div style="width:91%;float:left;">
                        <div style="font-size:12pt;color:black;font-weight: bold;">{{$post->time}}/{{$post->date_from}}/{{$post->date_to}}</div>
                        <div>{{$post->post}}</div>
                      </div>
                      <div>
                        <div style="float:left;"><a href="{{URL::to('edit_lpost/'.$post->id)}}"><img src="img/edit.png" width="35px" /></a></div>
                        <div style="float:left;margin-left:20px;"><a href="#" onclick="deletePost(this); return false;" data-id="{{$post->id}}"  data-type="linkedin"><img src="img/close.png" width="35px" /></a></div>
                      </div>
                    </div>
                  </li>
                </ul>
              @endforeach
                </div>
          </div>
        </div>
    </div>
      </div>
  <!--/div-->
@stop
