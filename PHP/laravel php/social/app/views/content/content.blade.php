@if(isset($social) && $social == 'linkedin')
    @include('content.linkedin')
@endif
@if(isset($social) && $social == 'facebook')
    @include('content.facebook')
@endif
@if(isset($social) && $social == 'twitter')
    @include('content.twitter')
@endif