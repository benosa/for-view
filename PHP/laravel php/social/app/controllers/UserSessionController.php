<?php

class UserSessionController extends BaseController {

	protected $layout = 'layouts.base';

	public function createPage()
	{
		$this->layout->content = View::make('user.login');
	}

	public function create()
	{
		$rules = array(
			'email'    => 'required',
			'password' => 'required|between:6,32'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('/')
				->withErrors($validator)
				->withInput(Input::only('email'));
		} else {
			$userdata1 = array(
				'email'    => strtolower(Input::get('email')),
				'password' => Input::get('password')
			);
			if (Auth::attempt($userdata1)) {
				//return $view->with('user',$user);
				return Redirect::to('/');
			} else {	 	
				$loginErrors = array('login' => "Email or password incorrect");
				return Redirect::to('/')->withInput(Input::only('email'))->withErrors($loginErrors);
			}
		}
	}

	public function destroy()
	{
		Auth::logout();
		return Redirect::to('/');
	}
}