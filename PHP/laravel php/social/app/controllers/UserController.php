<?php

class UserController extends BaseController {

	protected $layout = 'layouts.base';

	public function createPage()
	{
		$this->layout->content = View::make('user.register');
	}

	public function create()
	{
		$rules = array(
			'email'       => 'required|email|unique:users|between:2,64',
			'password'    => 'required|confirmed|between:6,32',
			'password_confirmation' => 'required|between:6,32',
		);

		$input = Input::all();
		$input['email'] = strtolower($input['email']);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			return Redirect::action('UserController@createPage')
				->withErrors($validator)
				->withInput(Input::only('email'));
		} else {
			$user = new User(array(
				'email' => $input['email'], 'password' => Hash::make($input['password'])
			));
			$user->save();

			$userdata = array(
				'email'    => $input['email'],
				'password' => $input['password']
			);

			if (Auth::attempt($userdata)) {
				return Redirect::to('register/social');
			}
		}
	}

	public function createSocialPage()
	{
		$this->layout->content = View::make('user.register_social');
	}

	public function createSocial()
	{
		$user = Auth::user();
		if (!Input::has('facebook') && !Input::has('twitter') && !Input::has('gplus') && !Input::has('linkedin')) {
			return Redirect::action('UserController@createSocialPage')
				->withErrors(array('error' => "No social networks selected"));
		} else {
			$count = 0;
			if (Input::has('facebook')) {
				if (!$user->facebookAccount) {
					$facebookAccount = new FacebookAccount;
					$facebookAccount->user()->associate($user);
					$facebookAccount->save();
				}
				Session::put('facebook', 1);
				$count++;
			}
			if (Input::has('twitter')) {
				if (!$user->twitterAccount) {
					$twitterAccount = new TwitterAccount;
					$twitterAccount->user()->associate($user);
					$twitterAccount->save();
				}
				Session::put('twitter', 1);
				$count++;
			}
			if (Input::has('gplus')) {
				if (!$user->gPlusAccount) {
					$gPlusAccount = new GPlusAccount;
					$gPlusAccount->user()->associate($user);
					$gPlusAccount->save();
				}
				Session::put('gplus', 1);
				$count++;
			}
			if (Input::has('linkedin')) {
				if (!$user->linkedInAccount) {
					$linkedInAccount = new LinkedInAccount;
					$linkedInAccount->user()->associate($user);
					$linkedInAccount->save();
				}
				Session::put('linkedin', 1);
				$count++;
			}
			Session::put('progress_step', 100/(2*$count+1));
			Session::put('progress', 100/(2*$count+1));
			return $this->createNext();
		}
	}

	public function createNext()
	{
		
		if (Session::has('facebook')) {
			Session::forget('facebook');
			return Redirect::to('register/facebook');
		}
		if (Session::has('twitter')) {
			Session::forget('twitter');
			return Redirect::to('register/twitter');
		}
		
		if (Session::has('gplus')) {
			Session::forget('gplus');
			return Redirect::to('register/gplus');
		}
		
		if (Session::has('linkedin')) {
			Session::forget('linkedin');
			return Redirect::to('register/linkedin');
		}
		/**/
		return Redirect::to('register/success');
	}

	public function createFacebookPage()
	{
		$this->layout->content = View::make('user.register_facebook')->withProgress(Session::get('progress'))
			->withProgress2(Session::get('progress')+Session::get('progress_step'));
	}

	public function createFacebook()
	{
		$rules = array(
			'id'    => 'required',
			'token' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);
		$id = Input::get('id');
		$token = Input::get('token');
		if ($validator->fails() || !ctype_alnum($token)) {
			return Redirect::action('UserController@createFacebookPage')
				->withErrors(array('error' => "Please Login to your Facebook account"));
		} else {
			$user = Auth::user();
			//var_dump($user->id);die();
			$connection = new FacebookAPI(Config::get('api.facebook_app_id'), Config::get('api.facebook_app_secret'));

			try {
			    $exchangeToken = $connection->exchangeShortToken($token);
			} catch (OAuthException $e) {
				//return Redirect::action('UserController@createFacebookPage')
				//	->withErrors(array('error' => "Please Login to your Facebook account"));
			}

			$date = new DateTime();
			$date->add(new DateInterval('PT'.$exchangeToken['expires'].'S'));
			$exires = $date->format('Y-m-d H:i:s');

			$longToken = $exchangeToken['access_token'];
			$permissions = $connection->getPermissions($id, $token);

			if (!isset($permissions['publish_actions']) || $permissions['publish_actions'] !== 1) {
				return Redirect::action('UserController@createFacebookPage')
					->withErrors(array('error' => "You didn't grant required permissions"));
			}

			if (!$user->facebookAccount) {
				$facebookAccount = new FacebookAccount;
				$facebookAccount->user()->associate($user);
			} else {
				$facebookAccount = $user->facebookAccount;
			}

			$facebookAccount->facebook_id  = $id;
			$facebookAccount->access_token = $longToken;
			$facebookAccount->exires = $exires;
			$facebookAccount->save();

			Session::put('progress', Session::get('progress')+2*Session::get('progress_step'));
			return $this->createNext();
		}
	}

	public function createTwitterPage()
	{
		if (!Input::has('oauth_token') || !Input::has('oauth_verifier') || !Session::has('oauth_token') || 
				Input::get('oauth_token') != Session::get('oauth_token')) {
			$connection = new TwitterOAuth(Config::get('api.twitter_app_id'), Config::get('api.twitter_app_secret'));
			$token = $connection->getRequestToken(URL::to('register/twitter'));
			Session::put('oauth_token', $token['oauth_token']);
			Session::put('oauth_token_secret', $token['oauth_token_secret']);
			$url = $connection->getAuthorizeURL($token, false);
			if (Session::has('twitter_success')) {
				Session::put('progress', Session::get('progress')-2*Session::get('progress_step'));
				Session::forget('twitter_success');
			}
			$this->layout->content = View::make('user.register_twitter')->withUrl($url)->withText('Please login to your Twitter account')
				->withButton('twitter')->withProgress(Session::get('progress'));
		} else {
			$user = Auth::user();
			$connection = new TwitterOAuth(Config::get('api.twitter_app_id'), Config::get('api.twitter_app_secret'), 
				Session::get('oauth_token'), Session::get('oauth_token_secret'));
			$verifier = Input::get('oauth_verifier');

			try {
			    $token = $connection->getAccessToken($verifier);
			} catch (OAuthException $e) {
				$connection = new TwitterOAuth(Config::get('api.twitter_app_id'), Config::get('api.twitter_app_secret'));
				$token = $connection->getRequestToken(URL::to('register/twitter'));
				Session::put('oauth_token', $token['oauth_token']);
				Session::put('oauth_token_secret', $token['oauth_token_secret']);
				$url = $connection->getAuthorizeURL($token, false);
				if (Session::has('twitter_success')) {
					Session::put('progress', Session::get('progress')-2*Session::get('progress_step'));
					Session::forget('twitter_success');
				}
				$this->layout->content = View::make('user.register_twitter')->withUrl($url)->withText('Please login to your Twitter account')
					->withButton('twitter')->withProgress(Session::get('progress'));
			    return;
			}

			$this->layout->content = View::make('user.register_twitter')->withUrl(URL::to('register/next'))
				->withText('You are successfully loggged in')->withButton('continue')
				->withProgress(Session::get('progress')+Session::get('progress_step'));

			if (!$user->twitterAccount) {
				$twitterAccount = new TwitterAccount;
				$twitterAccount->user()->associate($user);
			} else {
				$twitterAccount = $user->twitterAccount;
			}

			$twitterAccount->twitter_id  = $token['user_id'];
			$twitterAccount->oauth_token = $token['oauth_token'];
			$twitterAccount->oauth_token_secret = $token['oauth_token_secret'];
			$twitterAccount->save();

			Session::put('progress', Session::get('progress')+2*Session::get('progress_step'));
			Session::put('twitter_success', 1);
		}
	}
    

	public function createLinkedInPage()
	{
		 $code = Input::get( 'code' );
		 $_SESSION['state'] = 'DCEEFWF45453sdffef424';
	         $linkedinService = OAuth::consumer( 'Linkedin' );
	         if ( !empty( $code ) ) {
	            if ($_SESSION['state'] == $_GET['state']) {
                        $token = $linkedinService->requestAccessToken( $code );
                    }else{
                	return;
                    }
                 }
                 else {
                     $url = $linkedinService->getAuthorizationUri(array('state'=>$_SESSION['state']));
	             $this->layout->content = View::make('user.register_linkedin')->withUrl($url)->withText('Please login to your LinkedIn account')
				->withButton('linkedin')->withProgress(Session::get('progress'));
	             return;
	         }
	         $this->layout->content = View::make('user.register_linkedin')->withUrl(URL::to('register/next'))->withText('You are successfully loggged in')->withButton('continue')
				->withProgress(Session::get('progress')+Session::get('progress_step'));
		$user = Auth::user();
		if (!$user->linkedinAccount) {
			$linkedinAccount = new LinkedInAccount;
			$linkedinAccount->user()->associate($user);
		} else {
			$linkedinAccount = $user->linkedinAccount;
		}
		$linkedinAccount->accessToken = $token->getAccessToken();
		$linkedinAccount->endOfLife = date('Y-m-d H:i:s', $token->getEndOfLife());
		$linkedinAccount->save();

		Session::put('progress', Session::get('progress')+2*Session::get('progress_step'));
		Session::put('linkedin_success', 1);
	}

	public function createSuccessPage()
	{
		$this->layout->content = View::make('user.register_success')->withProgress(Session::get('progress'));
	}

	public function createSuccess()
	{
		return Redirect::to('/');
	}

	public function show()
	{
		$user = Auth::user();
		$this->layout->content = View::make('user.profile')->with('user',$user);
	}

}