<?php

class StatsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'layouts.base';

	public function page($period = null) {
		$user = Auth::user();
		$user_id = Auth::user()->getUserId();
		$time = new DateTime();
		$time = $time->setTime(0, 0, 0);
		$stats = array();
		$current_time = new DateTime();
		$current_time_0 = $current_time->setTime(0, 0, 0);
		$current_time = $current_time_0->format('Y-m-d');
		$current_year = $current_time_0->format('Y');

		/*$results = DB::select(DB::raw("SELECT count(*) AS cnt, CAST(date_from AS DATE) AS dat FROM linkedin_post WHERE user_id = ? AND date_from > '2014-01-01' GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id));
		$temp['count'] = $results[0]->cnt;
		$temp['date'] = $results[0]->dat;
		$rows = count($results);*/
			

		switch ($period) {
			case null:
				$time = $time->sub(new DateInterval('P31D'));

				$linkedin_data = DB::select(DB::raw("SELECT count(*) AS lcnt, CAST(date_from AS DATE) AS ldat FROM linkedin_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id, $time, $current_time));
				$facebook_data = DB::select(DB::raw("SELECT count(*) AS fcnt, CAST(date_from AS DATE) AS fdat FROM facebook_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id, $time, $current_time));
				$twitter_data = DB::select(DB::raw("SELECT count(*) AS tcnt, CAST(date_from AS DATE) AS tdat FROM twitter_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id, $time, $current_time));

				    for ($i = 0; $i <= 31; $i++) {    
				    	$stats[$time->format('Y-m-d')] = array('linkedin' => 0, 'facebook' => 0, 'twitter' => 0);
     					$time->add(new DateInterval('P1D'));
				    }
				    $lrows = count($linkedin_data);
				    for ($i=0; $i < $lrows; $i++) {
				    	$stats[$linkedin_data[$i]->ldat]['linkedin'] = $linkedin_data[$i]->lcnt;
				    }
				    $frows = count($facebook_data);
				    for ($i=0; $i < $frows; $i++) {
				    	$stats[$facebook_data[$i]->fdat]['facebook'] = $facebook_data[$i]->fcnt;
				    }
				    $trows = count($twitter_data);
				    for ($i=0; $i < $trows; $i++) {
				    	$stats[$twitter_data[$i]->tdat]['twitter'] = $twitter_data[$i]->tcnt;
				    }
				break;

			case '30days':
			default:				
				$time = $time->sub(new DateInterval('P31D'));

				$linkedin_data = DB::select(DB::raw("SELECT count(*) AS lcnt, CAST(date_from AS DATE) AS ldat FROM linkedin_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id, $time, $current_time));
				$facebook_data = DB::select(DB::raw("SELECT count(*) AS fcnt, CAST(date_from AS DATE) AS fdat FROM facebook_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id, $time, $current_time));
				$twitter_data = DB::select(DB::raw("SELECT count(*) AS tcnt, CAST(date_from AS DATE) AS tdat FROM twitter_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CAST(date_from AS DATE) ORDER BY date_from"), array($user_id, $time, $current_time));

				    for ($i = 0; $i <= 31; $i++) {    
				    	$stats[$time->format('Y-m-d')] = array('linkedin' => 0, 'facebook' => 0, 'twitter' => 0);
     					$time->add(new DateInterval('P1D'));
				    }
				    $lrows = count($linkedin_data);
				    for ($i=0; $i < $lrows; $i++) {
				    	$stats[$linkedin_data[$i]->ldat]['linkedin'] = $linkedin_data[$i]->lcnt;
				    }
				    $frows = count($facebook_data);
				    for ($i=0; $i < $frows; $i++) {
				    	$stats[$facebook_data[$i]->fdat]['facebook'] = $facebook_data[$i]->fcnt;
				    }
				    $trows = count($twitter_data);
				    for ($i=0; $i < $trows; $i++) {
				    	$stats[$twitter_data[$i]->tdat]['twitter'] = $twitter_data[$i]->tcnt;
				    }
				break;

			case '3month':
				$interval = new DateInterval('P3M');
				$time = $time->sub($interval);

				$linkedin_data = DB::select(DB::raw("SELECT count(*) AS lcnt, CONCAT(YEAR(date_from), '/', WEEK(date_from)) AS ldat FROM linkedin_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CONCAT(WEEK(date_from))  ORDER BY date_from"), array($user_id, $time, $current_time));
				$facebook_data = DB::select(DB::raw("SELECT count(*) AS fcnt, CONCAT(YEAR(date_from), '/', WEEK(date_from)) AS fdat FROM facebook_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CONCAT(WEEK(date_from)) ORDER BY date_from"), array($user_id, $time, $current_time));
				$twitter_data = DB::select(DB::raw("SELECT count(*) AS tcnt, CONCAT(YEAR(date_from), '/', WEEK(date_from)) AS tdat FROM twitter_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CONCAT(WEEK(date_from)) ORDER BY date_from"), array($user_id, $time, $current_time));

				    for ($i = 0; $i <= 13; $i++) {    
				    	$stats[$time->format('Y/W')] = array('linkedin' => 0, 'facebook' => 0, 'twitter' => 0);
     					$time->add(new DateInterval('P1W'));
				    }
				    $lrows = count($linkedin_data);
				    for ($i=0; $i < $lrows; $i++) {
				    	$stats[$linkedin_data[$i]->ldat]['linkedin'] = $linkedin_data[$i]->lcnt;
				    }
				    $frows = count($facebook_data);
				    for ($i=0; $i < $frows; $i++) {
				    	$stats[$facebook_data[$i]->fdat]['facebook'] = $facebook_data[$i]->fcnt;
				    }
				    $trows = count($twitter_data);
				    for ($i=0; $i < $trows; $i++) {
				    	$stats[$twitter_data[$i]->tdat]['twitter'] = $twitter_data[$i]->tcnt;
				    }
				break;

			case 'year':
				$interval = new DateInterval('P1Y');
				$time = $time->sub($interval);

				$linkedin_data = DB::select(DB::raw("SELECT count(*) AS lcnt, CONCAT(YEAR(date_from), '/', MONTH(date_from)) AS ldat FROM linkedin_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CONCAT(MONTH(date_from))  ORDER BY date_from"), array($user_id, $time, $current_time));
				$facebook_data = DB::select(DB::raw("SELECT count(*) AS fcnt, CONCAT(YEAR(date_from), '/', MONTH(date_from)) AS fdat FROM facebook_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CONCAT(MONTH(date_from)) ORDER BY date_from"), array($user_id, $time, $current_time));
				$twitter_data = DB::select(DB::raw("SELECT count(*) AS tcnt, CONCAT(YEAR(date_from), '/', MONTH(date_from)) AS tdat FROM twitter_post WHERE user_id = ? AND date_from > ? AND date_from <= ? GROUP BY CONCAT(MONTH(date_from)) ORDER BY date_from"), array($user_id, $time, $current_time));

				    for ($i = 0; $i <= 12; $i++) {    
				    	$stats[$time->format('Y/n')] = array('linkedin' => 0, 'facebook' => 0, 'twitter' => 0);
     					$time->add(new DateInterval('P1M'));
				    }
				    $lrows = count($linkedin_data);
				    for ($i=0; $i < $lrows; $i++) {
				    	$stats[$linkedin_data[$i]->ldat]['linkedin'] = $linkedin_data[$i]->lcnt;
				    }
				    $frows = count($facebook_data);
				    for ($i=0; $i < $frows; $i++) {
				    	$stats[$facebook_data[$i]->fdat]['facebook'] = $facebook_data[$i]->fcnt;
				    }
				    $trows = count($twitter_data);
				    for ($i=0; $i < $trows; $i++) {
				    	$stats[$twitter_data[$i]->tdat]['twitter'] = $twitter_data[$i]->tcnt;
				    }
				break;
		}

		$statictscs = array();

		//echo $stats['2014-06-20']['linkedin'];		

		foreach ($stats as $date => $stat) {

    		$statictscs[$date] = array('date'=>$date, 'linkedin'=>$stat['linkedin'], 'facebook'=>$stat['facebook'], 'twitter'=>$stat['twitter']);
    		//$date_graph[$date] = $date;
    		//$linkedin_graph[$date] = $stat['linkedin'];
    		//$facebook_graph[$date] = $stat['facebook'];
    		//$twitter_graph[$date] = $stat['twitter'];
  		}		

		$this->layout->nest('styles', 'social.stats_page_styles');
		$this->layout->content = View::make('social.stats_page')->with('statictscs',$statictscs);
	}
}