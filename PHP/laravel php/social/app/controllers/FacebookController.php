<?php

class FacebookController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'layouts.base';

	public function page()
	{
		$user = Auth::user();
		$facebookpost = array();
		$facebookpost_1day = array();
		$facebookpost_2day = array();
		$facebookpost_3day = array();
		if($user->facebookPost()){
			$facebookpost_1day = $user->facebookPost()->where('date_from',date('Y-m-d', strtotime(' +1 day')))->get();
			$facebookpost_2day = $user->facebookPost()->where('date_from',date('Y-m-d', strtotime(' +2 day')))->get();
			$facebookpost_3day = $user->facebookPost()->where('date_from',date('Y-m-d', strtotime(' +3 day')))->get();
			//$facebookpost = $user->facebookPost()->where('date_from',date('Y-m-d'))->where('time','>',time())->get();
			$facebookpost = $user->facebookPost()->where('date_from',date('Y-m-d'))->where('time','>',date("h:i", time()))->get();
		}
		$this->layout->nest('styles', 'social.facebook_page_styles');
		$this->layout->content = View::make('social.facebook_page')->with('facebookpost',$facebookpost)->with('facebookpost_1day',$facebookpost_1day)
			->with('facebookpost_2day',$facebookpost_2day)->with('facebookpost_3day',$facebookpost_3day);
	}
	
	public function CreateFPost()
	{
		$user = Auth::user();
		$this->layout->nest('styles', 'social.create_facebook_styles');

		$this->layout->content = View::make('social.facebook_create_post')->with('user',$user);
	}

	public function EditFPost($id = 0)
	{
		if($id == 0)return Redirect::to('create_fpost');
		$user = Auth::user();
		$this->layout->nest('styles', 'social.create_facebook_styles');
		$this->layout->content = View::make('social.facebook_edit_post')->with('user',$user)->with('id',$id);
	}

	public function postCreateFPost()
	{
		$filename = Input::get('filename');
		$post = Input::get('post');
		$dateFrom = Input::get('dateFrom');
		$time = Input::get('time');
		$user = Auth::user();
		$facebookPost = new FacebookPost;
		$facebookPost->user()->associate($user);
		$facebookPost->date_from = $dateFrom;
		$facebookPost->time = $time;
		$facebookPost->post = $post;
		$facebookPost->image = $filename;
		$facebookPost->active = true;
		$facebookPost->save();
		return ('ok');
	}

	public function postEditFPost()
	{
		$id = Input::get('id');
		$filename = Input::get('filename');
		$post = Input::get('post');
		$dateFrom = Input::get('dateFrom');
		$time = Input::get('time');
		$user = Auth::user();
		$facebookPost = FacebookPost::find($id);
		$facebookPost->date_from = $dateFrom;
		$facebookPost->time = $time;
		$facebookPost->post = $post;
		$facebookPost->image = $filename;
		$facebookPost->active = true;
		$facebookPost->save();
		return ('ok');
	}

}