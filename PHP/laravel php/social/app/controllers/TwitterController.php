<?php

class TwitterController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'layouts.base';

	public function page()
	{
		$user = Auth::user();
		$twitterpost = array();
		$twitterpost_1day = array();
		$twitterpost_2day = array();
		$twitterpost_3day = array();
		if($user->twitterPost()){
			$twitterpost_1day = $user->twitterPost()->where('date_from',date('Y-m-d', strtotime(' +1 day')))->get();
			$twitterpost_2day = $user->twitterPost()->where('date_from',date('Y-m-d', strtotime(' +2 day')))->get();
			$twitterpost_3day = $user->twitterPost()->where('date_from',date('Y-m-d', strtotime(' +3 day')))->get();
			$twitterpost = $user->twitterPost()->where('date_from',date('Y-m-d'))->where('time','>',date("h:i", time()))->get();
		}
		//var_dump($user->id);
		//var_dump($user->twitterPost()->where('date_from',date('Y-m-d'))->where('time','>',time())->get());
		//Log::debug(DB::getQueryLog());
		//var_dump(date("h:i", time()));
		$this->layout->nest('styles', 'social.twitter_page_styles');
		$this->layout->content = View::make('social.twitter_page')->with('twitterpost',$twitterpost)->with('twitterpost_1day',$twitterpost_1day)
			->with('twitterpost_2day',$twitterpost_2day)->with('twitterpost_3day',$twitterpost_3day);
			
	}
	
	public function CreateTweet()
	{
		$user = Auth::user();
		$this->layout->nest('styles', 'social.create_tweet_styles');

		$this->layout->content = View::make('social.twitter_create_tweet')->with('user',$user);
	}

	public function EditTweet($id = 0)
	{
		if($id == 0)return Redirect::to('create_tweet');
		$user = Auth::user();
		$this->layout->nest('styles', 'social.create_tweet_styles');
		$this->layout->content = View::make('social.twitter_edit_tweet')->with('user',$user)->with('id',$id);
	}

	public function postCreateTweet()
	{
		$filename = Input::get('filename');
		$tweet = Input::get('tweet');
		$dateFrom = Input::get('dateFrom');
		$time = Input::get('time');
		$user = Auth::user();
		$twitterPost = new TwitterPost;
		$twitterPost->user()->associate($user);
		$twitterPost->date_from = $dateFrom;
		$twitterPost->time = $time;
		$twitterPost->post = $tweet;
		$twitterPost->image = $filename;
		$twitterPost->active = true;
		$twitterPost->save();
		return ('ok');
	}

	public function postEditTweet()
	{
		$id = Input::get('id');
		$filename = Input::get('filename');
		$tweet = Input::get('tweet');
		$dateFrom = Input::get('dateFrom');
		$time = Input::get('time');
		$user = Auth::user();
		$twitterPost = TwitterPost::find($id);
		$twitterPost->date_from = $dateFrom;
		$twitterPost->time = $time;
		$twitterPost->post = $tweet;
		$twitterPost->image = $filename;
		$twitterPost->active = true;
		$twitterPost->save();
		return ('ok');
	}

}