<?php

class RemindersController extends BaseController {

	protected $layout = 'layouts.base';

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		$this->layout->content = View::make('password.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		$input = Input::only('email');
		$input['email'] = strtolower($input['email']);

		switch ($response = Password::remind($input))
		{
			case Password::INVALID_USER:
				return Redirect::back()->withErrors(Lang::get($response))->withInput(Input::only('email'));

			case Password::REMINDER_SENT:
				return Redirect::back()->withErrors(Lang::get($response));
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		$this->layout->content = View::make('password.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only('email', 'password', 'password_confirmation', 'token');
		$credentials['email'] = strtolower($credentials['email']);
		
		$rules = array(
			'password'              => 'required|confirmed|between:6,32',
			'password_confirmation' => 'required|between:6,32'
		);
		$validator = Validator::make($credentials, $rules);

		if ($validator->fails()) {
			return Redirect::back()
				->withErrors($validator)
				->withInput(Input::only('email'));
		}

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);
			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->withErrors(Lang::get($response))->withInput(Input::only('email'));

			case Password::PASSWORD_RESET:
				return Redirect::to('/')->withErrors("Password was reset");
		}
	}

}