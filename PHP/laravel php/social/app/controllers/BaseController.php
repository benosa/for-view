<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if (!is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		}
		if (Auth::check()) {
			$this->layout->header = View::make('header')->withUser(Auth::user());
		} else {
			$this->layout->header = View::make('header');
		}
		$this->layout->footer = View::make('footer');
		$this->layout->messages = View::make('messages');
	}

}

