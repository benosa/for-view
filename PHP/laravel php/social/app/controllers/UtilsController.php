<?php

class UtilsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'layouts.base';

	public function deletePost()
	{
		$postId = Input::get('postid');
		$postType = Input::get('type');
		if($postType == 'twitter'){
			$post = TwitterPost::where('id',$postId)->update(array('active' => false));
			return 'ok';
		}else if($postType == 'facebook'){
			$post = FacebookPost::where('id',$postId)->update(array('active' => false));
			return 'ok';
		}else if($postType == 'linkedin'){
			$post = LinkedinPost::where('id',$postId)->update(array('active' => false));
			return 'ok';
		}
		return 'false';
	}

	public function exportCSV($type)
	{
		//$type = Input::get('type');
		//var_dump($type);
		$output = fopen("php://output", 'w');
		$user = Auth::user();
		$user_id = Auth::user()->getUserId();
		$posts = array();
		if($type == 'twitter' && $user->TwitterPost){
			$posts = $user->TwitterPost->all();
		}
		if($type == 'facebook' && $user->FacebookPost){
			$posts = $user->FacebookPost->all();
		}
		if ($type == 'linkedin' && $user->LinkedinPost) {
			$posts = $user->LinkedinPost->all();
		}
		if(sizeof($posts) != 0){
				foreach ($posts as $post) {
    			if ($post->active == 1 && $post->user_id == $user_id) {
    				$str = $post->date_from.','.$post->time.','.$post->post."\r\n";
    				fwrite($output, $str);
    				}
			}

			@fclose($output); 
        	$headers = array(
            	'Content-Type' => 'text/csv',
            	'Content-Disposition' => 'attachment; filename="'. $user_id .'_Export.csv"',
            	);
        	$tmp = readfile("php://output");
        	//return Response::make(rtrim($tmp, "\n"), 200, $headers);
        	return Response::make($tmp, 200, $headers);
        }
        //return Response::make('false', 200);
	}
}