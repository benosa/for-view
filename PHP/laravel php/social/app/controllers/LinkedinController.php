<?php

class LinkedinController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'layouts.base';

	public function page()
	{
		$user = Auth::user();
		$linkedinpost = array();
		$linkedinpost_1day = array();
		$linkedinpost_2day = array();
		$linkedinpost_3day = array();
		if($user->LinkedInPost){
			$linkedinpost_1day = $user->linkedInPost()->where('date_from',date('Y-m-d', strtotime(' +1 day')))->get();
			$linkedinpost_2day = $user->linkedInPost()->where('date_from',date('Y-m-d', strtotime(' +2 day')))->get();
			$linkedin_3day = $user->linkedInPost()->where('date_from',date('Y-m-d', strtotime(' +3 day')))->get();
			//$linkedinpost = $user->linkedInPost()->where('date_from',date('Y-m-d'))->where('time','>',time())->get();
			$linkedinpost = $user->linkedInPost()->where('date_from',date('Y-m-d'))->where('time','>',date("h:i", time()))->get();
		}
		$this->layout->nest('styles', 'social.linkedin_page_styles');
		$this->layout->content = View::make('social.linkedin_page')->with('linkedinpost',$linkedinpost)->with('linkedinpost_1day',$linkedinpost_1day)
			->with('linkedinpost_2day',$linkedinpost_2day)->with('linkedinpost_3day',$linkedinpost_3day);
	}
	
	public function CreateLPost()
	{
		$user = Auth::user();
		$this->layout->nest('styles', 'social.create_linkedin_styles');
		$this->layout->content = View::make('social.linkedin_create_post')->with('user',$user);
	}

	public function EditLPost($id = 0)
	{
		if($id == 0)return Redirect::to('create_lpost');
		$user = Auth::user();
		$this->layout->nest('styles', 'social.create_linkedin_styles');
		$this->layout->content = View::make('social.linkedin_edit_post')->with('user',$user)->with('id',$id);
	}

	public function postCreateLPost()
	{
		$filename = Input::get('filename');
		$post = Input::get('post');
		$dateFrom = Input::get('dateFrom');
		$time = Input::get('time');
		$user = Auth::user();
		$linkedinPost = new LinkedInPost;
		$linkedinPost->user()->associate($user);
		$linkedinPost->date_from = $dateFrom;
		$linkedinPost->time = $time;
		$linkedinPost->post = $post;
		$linkedinPost->image = $filename;
		$linkedinPost->active = true;
		$linkedinPost->save();
		return ('ok');
	}

	public function postEditLPost()
	{
		$id = Input::get('id');
		$filename = Input::get('filename');
		$post = Input::get('lpost');
		$dateFrom = Input::get('dateFrom');
		$time = Input::get('time');
		$user = Auth::user();
		$linkedinPost = LinkedinPost::find($id);
		$linkedinPost->date_from = $dateFrom;
		$linkedinPost->time = $time;
		$linkedinPost->post = $post;
		$linkedinPost->image = $filename;
		$linkedinPost->active = true;
		$linkedinPost->save();
		return ('ok');
	}	

}