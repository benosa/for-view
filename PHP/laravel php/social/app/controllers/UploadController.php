<?php
class UploadController extends BaseController {
	protected $layout = 'layouts.base';
	public function postImage()
	{
		$file = Input::file('file'); // your file upload input field in the form should be named 'file'
		$destinationPath = 'uploads/'.str_random(8);
		$filename = $file->getClientOriginalName();
		//$extension =$file->getClientOriginalExtension(); //if you need extension of the file
		$uploadSuccess = Input::file('file')->move($destinationPath, $filename);
		 
		if( $uploadSuccess ) {
			return $destinationPath.'/'.$filename;
		   //return Response::json('success', 200); // or do a redirect with some message that file was uploaded
		} else {
		   return Response::json('error', 400);
		}
	}
}