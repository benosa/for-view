<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses' => 'HomeController@showWelcome'));

Route::get('login', array('uses' => 'UserSessionController@createPage', 'before' => 'guest'));
Route::post('login', array('uses' => 'UserSessionController@create', 'before' => array('guest', 'csrf')));

Route::get('logout', array('uses' => 'UserSessionController@destroy', 'before' => 'auth'));

Route::get('register', array('uses' => 'UserController@createPage', 'before' => 'guest'));
Route::post('register', array('uses' => 'UserController@create', 'before' => array('guest', 'csrf')));

Route::get('register/social', array('uses' => 'UserController@createSocialPage', 'before' => 'auth'));
Route::post('register/social', array('uses' => 'UserController@createSocial', 'before' => array('auth', 'csrf')));

Route::get('register/facebook', array('uses' => 'UserController@createFacebookPage', 'before' => 'auth'));
Route::post('register/facebook', array('uses' => 'UserController@createFacebook', 'before' => array('auth', 'csrf')));

Route::get('register/twitter', array('uses' => 'UserController@createTwitterPage', 'before' => 'auth'));
Route::post('register/twitter', array('uses' => 'UserController@createTwitter', 'before' => array('auth', 'csrf')));

Route::get('register/gplus', array('uses' => 'UserController@createGPlusPage', 'before' => 'auth'));
Route::post('register/gplus', array('uses' => 'UserController@createGPlus', 'before' => array('auth', 'csrf')));

Route::get('register/linkedin', array('uses' => 'UserController@createLinkedInPage', 'before' => 'auth'));
Route::post('register/linkedin', array('uses' => 'UserController@createLinkedIn', 'before' => array('auth', 'csrf')));
//Route::get('register/linkedin', array('uses' => 'UserController@createTwitterPage', 'before' => 'auth'));
//Route::post('register/linkedin', array('uses' => 'UserController@createTwitter', 'before' => array('auth', 'csrf')));
Route::get('stats', array('uses' => 'StatsController@page', 'before' => 'auth'));
Route::get('stats/{period}', array('uses' => 'StatsController@page', 'before' => 'auth'));

Route::get('register/next', array('uses' => 'UserController@createNext', 'before' => 'auth'));

Route::get('register/success', array('uses' => 'UserController@createSuccessPage', 'before' => 'auth'));
Route::post('register/success', array('uses' => 'UserController@createSuccess', 'before' => array('auth', 'csrf')));

Route::get('profile', array('uses' => 'UserController@show', 'before' => 'auth'));

Route::get('remind-password', array('uses' => 'RemindersController@getRemind', 'before' => 'guest'));
Route::post('remind-password', array('uses' => 'RemindersController@postRemind', 'before' => array('guest', 'csrf')));

Route::get('reset-password/{token}', array('uses' => 'RemindersController@getReset', 'before' => 'guest'));
Route::post('reset-password', array('uses' => 'RemindersController@postReset', 'before' => array('guest', 'csrf')));

Route::get('linkedin', array('uses' => 'LinkedinController@page', 'before' => 'auth'));
Route::get('create_lpost', array('uses' => 'LinkedinController@CreateLPost', 'before' => 'auth'));
Route::get('edit_lpost/{id}', array('uses' => 'LinkedinController@EditLPost', 'before' => 'auth'));
Route::post('postLinkedin', array('uses' => 'LinkedinController@postCreateLPost', 'before' => array('auth', 'csrf')));
Route::post('postEditLPost', array('uses' => 'LinkedinController@postEditLPost', 'before' => array('auth', 'csrf')));

Route::get('facebook', array('uses' => 'FacebookController@page', 'before' => 'auth'));
Route::get('create_fpost', array('uses' => 'FacebookController@CreateFPost', 'before' => 'auth'));
Route::get('edit_fpost/{id}', array('uses' => 'FacebookController@EditFPost', 'before' => 'auth'));
Route::post('postFacebook', array('uses' => 'FacebookController@postCreateFPost', 'before' => array('auth', 'csrf')));
Route::post('postEditFPost', array('uses' => 'FacebookController@postEditFPost', 'before' => array('auth', 'csrf')));

Route::get('twitter', array('uses' => 'TwitterController@page', 'before' => 'auth'));
Route::get('create_tweet', array('uses' => 'TwitterController@CreateTweet', 'before' => 'auth'));
Route::get('edit_tweet/{id}', array('uses' => 'TwitterController@EditTweet', 'before' => 'auth'));
Route::post('postTweet', array('uses' => 'TwitterController@postCreateTweet', 'before' => array('auth', 'csrf')));
Route::post('postEditTweet', array('uses' => 'TwitterController@postEditTweet', 'before' => array('auth', 'csrf')));

Route::post('postImage', array('uses' => 'UploadController@postImage', 'before' => array('auth', 'csrf')));
Route::post('deletePost', array('uses' => 'UtilsController@deletePost', 'before' => array('auth', 'csrf')));
Route::get('/exportCSV/{type}', array('uses' => 'UtilsController@exportCSV', 'before' => 'auth'));

Route::get('search_users',array('uses' => 'AdminController@page', 'before' => 'auth'));
//Route::get('export', array('uses' => 'UtilsController@get_export', 'before' => 'auth'));