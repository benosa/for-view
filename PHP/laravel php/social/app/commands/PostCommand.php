<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Guzzle\Http\Exception\BadResponseException;
use League\OAuth2\Client\Exception\IDPException as IDPException;
use Guzzle\Service\Client as GuzzleClient;
use League\OAuth2\Client\Token\AccessToken as AccessToken;

class PostCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'post';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Posting messages in social networks';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	private function postTweet($post){
		$user_id = $post->user_id;
		$message = $post->post;
		$imgdb = $post->image;
		$image = '/var/www5social/public/'.$imgdb;
		$name = basename($image);
		$twetterAccount = User::find($user_id)->twitterAccount;
		$twitter_id = $twetterAccount->twitter_id;
		$oauth_token = $twetterAccount->oauth_token;
		$oauth_token_secret = $twetterAccount->oauth_token_secret;
		if(strlen($message) < 140){
		    try {
			    $tmhOAuth = new \tmhOAuthExample(array('consumer_key' => Config::get('api.twitter_app_id'),'consumer_secret' => Config::get('api.twitter_app_secret'),
				    'token' => $oauth_token, $oauth_token_secret, 'secret' => $oauth_token_secret,));
			    if(strlen($imgdb) > 3){
				$params = array();
				$params['media[]'] = '@'.$image;
				if (null !==  $message) {
				    $params['status'] = $message;
				}
				$response = $tmhOAuth->user_request(array(
				     'method' => 'POST',
				     'url' => $tmhOAuth->url("/1.1/statuses/update_with_media"),
				     'params' => $params,
				     'multipart' => true
				));
			    }else{
				$code = $tmhOAuth->user_request(array(
				    'method' => 'POST',
				    'url' => $tmhOAuth->url('/1.1/statuses/update'),
				    'params' => array(
				      'status' => 'Something for the weekend'
				    )
				));
				//$tmhOAuth->render_response();
			    }
			    
			    return true;
			} catch (Exception $e) {
			    return false;
			    //$this->error('Twitter OAuth error! '.'user id: '.$user_id.' Message: '.$message.' Error: '.$e);
			}
		}
	    return false;
	}
	
/*
	private function getContents($url) {
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,$url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $content = curl_exec ($ch);
	    curl_close ($ch);
	    return $content;
	}
	
	private function getUserId($access_token){
	    $url = "https://graph.facebook.com/me?access_token=". $access_token;
	    var_dump($url);die();
	    $userInfo = $this->getContents($url);
	    $userInfo = json_decode($userInfo, true);
	    return $userInfo;
	}
*/
	private function postFacebook($post){
		$user_id = $post->user_id;
		$message = $post->post;
		$imgdb = $post->image;
		$image = (strlen($imgdb) > 3) ? '@'.'/var/www5social/public/'.$imgdb : NULL;
		$name = basename($image);
		$facebookAccount = User::find($user_id)->facebookAccount;
		$facebook_id = $facebookAccount->facebook_id;
		$access_token = $facebookAccount->access_token;
		if($message != NULL) {
		    if($image != NULL){
			$facebook = new Facebook(array(
			    'appId' => Config::get('api.facebook_app_id'), 
			    'secret' => Config::get('api.facebook_app_secret'),
			    'fileUpload' => true,
			'cookie' => true));
			$facebook->setAccessToken($access_token);
			$attachment = array('message' => $message,'name' => $name,'picture' => $image);
		    }else{
			$facebook = new Facebook(array(
			    'appId' => Config::get('api.facebook_app_id'), 
			    'secret' => Config::get('api.facebook_app_secret'),
			'cookie' => true));
			$attachment = array('message' => $message);
		    }
		    try {
			if($image != NULL){
			    $result = $facebook->api('/'. $facebook_id . '/photos/','post',$attachment);
			}else{
			    $result = $facebook->api('/'. $facebook_id . '/feed/','post',$attachment);
			}
            		if(isset($result['id'])) 
            		    return true;
        		else 
            		    return false;
            	    } catch(FacebookApiException $e) {
        		return $e->getMessage();
        	    }
        	}
        	return false;
	}	

	private function postLinkedin($post){
		$user_id = $post->user_id;
		$message = $post->post;
		$imgdb = $post->image;
		$image = (strlen($imgdb) > 3) ? '@'.'/var/www5social/public/'.$imgdb : NULL;
		$name = basename($image);
		$linkedinAccount = User::find($user_id)->LinkedInAccount;
		$access_token = $linkedinAccount->accessToken;
		$endOfLife = $linkedinAccount->endOfLife;
		$shareUrl = "https://api.linkedin.com/v1/people/~/shares?oauth2_access_token={$access_token}";
		$xml = "<share>
			<comment>Check out the LinkedIn Share API!</comment>
			<content>
		    	    <title>LinkedIn Developers Documentation On Using the Share API</title>
		            <description>Leverage the Share API to maximize engagement on user-generated content on LinkedIn</description>
		            <submitted-url>https://developer.linkedin.com/documents/share-api</submitted-url>
		            <submitted-image-url>http://m3.licdn.com/media/p/3/000/124/1a6/089a29a.png</submitted-image-url>
		        </content>
		        <visibility>
		    	    <code>anyone</code>
		        </visibility>
		    </share>";
		try
		    {
			$client = new GuzzleClient();
			$request = $client->post($shareUrl,array('content-type' => 'text/xml; charset=utf-8'),array());
			$request->setBody($xml);
			$response = $request->send();
		    }
		catch (Guzzle\Http\Exception\BadResponseException $e)
		{
			echo 'Error: ' . $e->getMessage();
		}
		return false;
	}
	
	public function fire()
	{
		$tweetArray = TwitterPost::where('date_from',date('Y-m-d'))->where('time','>',date('h:i',time()))->where('active',true)->get();
		foreach($tweetArray as $post){
		    $this->info($post->post);
		    $result = $this->postTweet($post);
		    if($result == true){
			$_post = TwitterPost::find($post->id);
			$_post->active = false;
			//$_post->save();
		    }
		}
		$facebookArray = FacebookPost::where('date_from',date('Y-m-d'))->where('time','>',date('h:i',time()))->where('active',true)->get();
		foreach($facebookArray as $post){
		    $result = $this->postFacebook($post);
		    if($result == true){
			$_post = FacebookPost::find($post->id);
			$_post->active = false;
			//$_post->save();
		    }
		}
		$linkedinArray = LinkedInPost::where('date_from',date('Y-m-d'))->where('time','>',date('h:i',time()))->where('active',true)->get();
		foreach($linkedinArray as $post){
		    $result = $this->postLinkedin($post);
		    if($result == true){
			$_post = LinkedInPost::find($post->id);
			$_post->active = false;
			//$_post->save();
		    }
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
