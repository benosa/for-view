<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdAndTokenToTwitterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('twitter_accounts', function($table)
		{
			$table->string('twitter_id', 12);
			$table->string('oauth_token');
			$table->string('oauth_token_secret');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('twitter_accounts', function($table)
		{
		    $table->dropColumn('twitter_id');
		    $table->dropColumn('oauth_token');
		    $table->dropColumn('oauth_token_secret');
		});
	}

}
