<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEndOfLifeToLinkedinAccounts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('linkedin_accounts', function(Blueprint $table)
		{
			$table->dateTime('endOfLife');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('linkedin_accounts', function(Blueprint $table)
		{
			$table->dropColumn('endOfLife');
		});
	}

}
