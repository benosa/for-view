<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterPostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('twitter_post', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->date('date_from');
			$table->date('date_to');
			$table->time('time');
			$table->text('post');
			$table->string('image');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('twitter_post');
	}

}
