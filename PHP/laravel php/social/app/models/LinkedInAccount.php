<?php

class LinkedInAccount extends Eloquent {

	protected $table = 'linkedin_accounts';

    public function user()
    {
        return $this->belongsTo('User');
    }
}