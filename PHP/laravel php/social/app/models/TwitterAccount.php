<?php

class TwitterAccount extends Eloquent {

	protected $table = 'twitter_accounts';

    public function user()
    {
        return $this->belongsTo('User');
    }
}