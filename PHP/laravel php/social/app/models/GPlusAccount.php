<?php

class GPlusAccount extends Eloquent {

	protected $table = 'gplus_accounts';

    public function user()
    {
        return $this->belongsTo('User');
    }
}