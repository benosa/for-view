<?php

class FacebookAccount extends Eloquent {

	protected $table = 'facebook_accounts';

    public function user()
    {
        return $this->belongsTo('User');
    }
}