<?php

class LinkedinPost extends Eloquent {

	protected $table = 'linkedin_post';

    public function user()
    {
        return $this->belongsTo('User');
    }
    public function getActivePosts()
    {
    	return $this->where('active', true);
    }
    public function getExportPosts()
    {
    	//return $this->where('active', true);
    	return $this->hasManyThrough('date_from')->where('active', true);
    }
}