<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = array('email', 'password');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');
	public function getRememberToken()
	{
	    return $this->remember_token;
	}
	    
	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}
	        
	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
    
    public function isAdmin()
    {
	return $this->is_admin;
    }
    public function facebookAccount()
    {
        return $this->hasOne('FacebookAccount');
    }

    public function twitterAccount()
    {
        return $this->hasOne('TwitterAccount');
    }
    
    public function twitterPost()
    {
        return $this->hasOne('TwitterPost')->where('active', true);
    }

    public function facebookPost()
    {
        return $this->hasOne('FacebookPost')->where('active', true);
    }

    public function linkedInPost()
    {
        return $this->hasOne('LinkedInPost')->where('active', true);
    }

    public function gPlusAccount()
    {
        return $this->hasOne('GPlusAccount');
    }

    public function linkedInAccount()
    {
        return $this->hasOne('LinkedInAccount');
    }

    public function getUserId()
	{
	  return $this->id;
	}

}