<?php

class TwitterPost extends Eloquent {

	protected $table = 'twitter_post';

    public function user()
    {
        return $this->belongsTo('User');
    }
    public function getActivePosts()
    {
    	return $this->where('active', true);
    }
    public function getExportPosts()
    {
    	//return $this->where('active', true);
    	return $this->hasManyThrough('date_from')->where('active', true);
    }
}