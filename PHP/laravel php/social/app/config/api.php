<?php

return array(

	'facebook_app_id' => '',

	'facebook_app_secret' => '',

	'twitter_app_id' => '',

	'twitter_app_secret' => '',
	
	'linkedin_client_id'     => '',
	
	'linkedin_client_secret' => '',

);
