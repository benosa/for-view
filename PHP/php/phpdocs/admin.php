<?php
    if($_POST['log']==true){
       if($_POST['login']=='' && md5($_POST['password']) == '')
       $IS_ADMIN = true;
    }
    if($_POST['get']==true){
    require_once 'classes/phpexel/Classes/PHPExcel.php';
    if($_POST['rep']==true)require_once 'config/config.php';
    if($_POST['rep2']==true)require_once 'config/config2.php';
    if($_POST['rep3']==true)require_once 'config/config3.php';
    require_once('classes/MysqliDb.php');
    $db = new MysqliDb(HOST, LOGIN, PASSWORD, DB);
    //echo "SELECT * FROM `step1` WHERE `regDate` >= ".$_POST['date_from']." AND `regDate` <= ".$_POST['date_to']."";
    $b = $db->query("SELECT * FROM `step1` WHERE `regDate` >= ".$_POST['date_from']." AND `regDate` <= ".$_POST['date_to']."");
    //print_r($b);

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $active_sheet = $objPHPExcel->getActiveSheet();
    //Ориентация страницы и  размер листа
    $active_sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
    $active_sheet->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    //Поля документа
    $active_sheet->getPageMargins()->setTop(1);
    $active_sheet->getPageMargins()->setRight(0.75);
    $active_sheet->getPageMargins()->setLeft(0.75);
    $active_sheet->getPageMargins()->setBottom(1);
    //Название листа
    $active_sheet->setTitle("Лист-отчет");
    //Шапа и футер
    $active_sheet->getHeaderFooter()->setOddHeader("&CШапка Отчета по регистрации");
    $active_sheet->getHeaderFooter()->setOddFooter('&L&B'.$active_sheet->getTitle().'&RСтраница &P из &N');
    //Настройки шрифта
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
    //ширина столбцов----------------------------------------------------
    $active_sheet->getColumnDimension('A')->setWidth(20);
    $active_sheet->getColumnDimension('B')->setWidth(20);
    $active_sheet->getColumnDimension('C')->setWidth(20);
    $active_sheet->getColumnDimension('D')->setWidth(20);
    $active_sheet->getColumnDimension('E')->setWidth(20);
    //ширина столбцов----------------------------------------------------
    //заполнение данніми-------------------------------------------------
    $active_sheet->mergeCells('A1:E1');
    $active_sheet->getRowDimension('1')->setRowHeight(40);
    $active_sheet->setCellValue('A1','gf2045.com');
    $active_sheet->mergeCells('A2:E2');
    $active_sheet->setCellValue('A2','Пользователи прошедшие первую стадию регистрации');
    $active_sheet->mergeCells('A4:C4');
    $active_sheet->setCellValue('A4','Выборка с и до');
    //===================================================================
    //Записываем данные в ячейку
    $date = date('d-m-Y');
    $active_sheet->setCellValue('D4',$_POST['date_from']." - ".$_POST['date_to']);
    //Устанавливает формат данных в ячейке - дата
    $active_sheet->getStyle('D4')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
    //===================================================================
    //Создаем шапку таблички данных
    $active_sheet->setCellValue('A6','Имя');
    $active_sheet->setCellValue('B6','Фамилия');
    $active_sheet->setCellValue('C6','Организация');
    $active_sheet->setCellValue('D6','Эл. почта');
    $active_sheet->setCellValue('E6','Дата регистрации');

    //В цикле проходимся по элементам массива и выводим все в соответствующие ячейки
    $row_start = 7;
    $i = 0;
    foreach($b as $item) {
        $row_next = $row_start + $i;

        $active_sheet->setCellValue('A'.$row_next,$item['fName']);
        $active_sheet->setCellValue('B'.$row_next,$item['lName']);
        $active_sheet->setCellValue('C'.$row_next,$item['orgName']);
        $active_sheet->setCellValue('D'.$row_next,$item['eMail']);
        $active_sheet->setCellValue('E'.$row_next,$item['regDate']);

        $i++;
    }
    //массив стилей
    $style_wrap = array(
        //рамки
        'borders'=>array(
	    //внешняя рамка
	    'outline' => array(
	        'style'=>PHPExcel_Style_Border::BORDER_THICK
	    ),
	    //внутренняя
	    'allborders'=>array(
	        'style'=>PHPExcel_Style_Border::BORDER_THIN,
	        'color' => array(
		    'rgb'=>'696969'
	        )
	    )
        )
    );
    //применяем массив стилей к ячейкам
    $active_sheet->getStyle('A1:E'.($i+6))->applyFromArray($style_wrap);
    //Стили для верхней надписи строка 1
    $style_header = array(
        //Шрифт
        'font'=>array(
	    'bold' => true,
	    'name' => 'Times New Roman',
	    'size' => 20
        ),
        //Выравнивание
        'alignment' => array(
	    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
	    'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
        ),
        //Заполнение цветом
        'fill' => array(
	    'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
	    'color'=>array(
	        'rgb' => 'CFCFCF'
	    )
        )

    );

    $active_sheet->getStyle('A1:E1')->applyFromArray($style_header);

    //Стили для слогана компании – вторая строка
    $style_slogan = array(
        //шрифт
        'font'=>array(
	    'bold' => true,
	    'italic' => true,
	    'name' => 'Times New Roman',
	    'size' => 13,
	    'color'=>array(
	        'rgb' => '8B8989'
	    )

        ),
    //выравнивание
        'alignment' => array(
	    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
	    'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
	),
    //заполнение цветом
        'fill' => array(
	    'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
	    'color'=>array(
	        'rgb' => 'CFCFCF'
	    )
        ),
    //рамки
        'borders' => array(
	    'bottom' => array(
	        'style'=>PHPExcel_Style_Border::BORDER_THICK
	    )
        )
    );
    $active_sheet->getStyle('A2:E2')->applyFromArray($style_slogan);

    //Стили для текта возле даты
    $style_tdate = array(
    //выравнивание
        'alignment' => array(
	    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
        ),
    //заполнение цветом
        'fill' => array(
	    'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
	    'color'=>array(
	        'rgb' => 'CFCFCF'
	    )
        ),
    //рамки
        'borders' => array(
	    'right' => array(
	        'style'=>PHPExcel_Style_Border::BORDER_NONE
	    )
        )
    );
    $active_sheet->getStyle('A4:C4')->applyFromArray($style_tdate);

    //Стили для даты
    $style_date = array(
        //заполнение цветом
        'fill' => array(
	    'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
	    'color'=>array(
	        'rgb' => 'CFCFCF'
	    )
        ),
    //рамки
        'borders' => array(
	    'left' => array(
	        'style'=>PHPExcel_Style_Border::BORDER_NONE
	    )
        ),
    );
    $active_sheet->getStyle('E4')->applyFromArray($style_date);
    $active_sheet->getStyle('D4')->applyFromArray($style_date);
    //Стили для шапочки прайс-листа
    $style_hprice = array(
        //выравнивание
        'alignment' => array(
	    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
        ),
        //заполнение цветом
        'fill' => array(
	    'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
	    'color'=>array(
	        'rgb' => 'CFCFCF'
	    )
        ),
        //Шрифт
        'font'=>array(
	    'bold' => true,
	    'italic' => true,
	    'name' => 'Times New Roman',
	    'size' => 10
        ),
    );
    $active_sheet->getStyle('A6:E6')->applyFromArray($style_hprice);
    //стили для данных в таблице прайс-листа
    $style_price = array(
        'alignment' => array(
	    'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
        )
    );
$active_sheet->getStyle('A7:E'.($i+6))->applyFromArray($style_price);
    /* */
    header("Content-Type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename='simple.xls'");
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

//exit();
  }
?>
<!doctype html>
<html>
<head>
       <meta charset=utf-8>
</head>
<body>
<!-- Before -->

<!-- After -->
<?php
    if(isset($IS_ADMIN) AND $IS_ADMIN == true){
?>
<div style="width:510px;">
<div style="width:500px;float:left;border-width: 1px; border-style: dotted; border-color: blue;margin-top:20px;">
<div style="color:red;margin-top:-10px;margin-left:10px;background-color:white;width:60px;"><b>STEP1</b></div>
<form action="" method="post">
        <div style="float:left;">
            <div>
                Date from
            </div>
            <div>
                <input name="date_from" type="date" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;">
            <div>
                Date to
            </div>
            <div>
                <input name="date_to" type="date" />
                <input name="get" type="hidden" value="true" />
		<input name="rep" type="hidden" value="true" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;padding-left:30px;margin-top:20px;">
            <input type="Submit" value="Get Report" />
        </div>
</form>
</div>
<div style="width:500px;float:left;border-width: 1px; border-style: dotted; border-color: blue;margin-top:20px;">
<div style="color:red;margin-top:-10px;margin-left:10px;margin-left:10px;background-color:white;width:60px;"><b>VIP</b></div>
<form action="" method="post">
        <div style="float:left;">
            <div>
                Date from
            </div>
            <div>
                <input name="date_from" type="date" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;">
            <div>
                Date to
            </div>
            <div>
                <input name="date_to" type="date" />
                <input name="get" type="hidden" value="true" />
		<input name="rep2" type="hidden" value="true" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;padding-left:30px;margin-top:20px;">
            <input type="Submit" value="Get Report" />
        </div>
</form>
</div>
<div style="width:500px;float:left;border-width: 1px; border-style: dotted; border-color: blue;margin-top:20px;">
<div style="color:red;margin-top:-10px;margin-left:10px;margin-left:10px;background-color:white;width:60px;"><b>MEDIA</b></div>
<form action="" method="post">
        <div style="float:left;">
            <div>
                Date from
            </div>
            <div>
                <input name="date_from" type="date" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;">
            <div>
                Date to
            </div>
            <div>
                <input name="date_to" type="date" />
                <input name="get" type="hidden" value="true" />
		<input name="rep3" type="hidden" value="true" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;padding-left:30px;margin-top:20px;">
            <input type="Submit" value="Get Report" />
        </div>

</form>
    </div>
</div>
<?php
}else{
  ?>
   <form action="" method="post">
    <div style="width:500px;">
        <div style="float:left;">
            <div>
                login:
            </div>
            <div>
                <input name="login" type="text" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;">
            <div>
                Password:
            </div>
            <div>
                <input name="password" type="password" />
                <input name="log" type="hidden" value="true" />
            </div>
        </div>
        <div style="float:left;padding-left:30px;padding-left:30px;margin-top:20px;">
            <input type="Submit" value="login" />
        </div>
    </div>
</form>

<?php
}
?>
</body>
</html>