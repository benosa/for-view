<?php
  require_once 'config/config.php';
  require_once('classes/MysqliDb.php');

$db = new MysqliDb(HOST, LOGIN, PASSWORD, DB);
 $fName = $_POST['fName'];
 $lName = $_POST['lName'];
 $orgName = $_POST['orgName'];
 $eMail = $_POST['eMail'];
 $insertData = array(
	'fName' => $fName,
	'lName' => $lName,
    'orgName' => $orgName,
    'eMail' => $eMail
);
$results = $db->insert(TABLE_NAME, $insertData);

header('Location: https://');
?>