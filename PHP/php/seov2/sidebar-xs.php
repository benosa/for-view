		<div id="main-container" class="menu-smm-container visible-phone hidden-new">
		<div class="sidebar-xs" style="z-index:2;" data-spy="affix" data-offset-top="190" data-offset-bottom="200">
			<div class="row">
				<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 review-menu-smm">
									
					<a data-scroll href="#overall-perf"><span class="fa-list-alt fa-list-alt-s webreview-smm pull-left"></span></a>
					
					<a data-scroll href="#w3c-panel"><span class="w3-icon-smm pull-left">W3</span></a>
					
					<a data-scroll href="#gps-panel"><span class="fa fa-dashboard fa-dashboard-smm pull-left"></span></a>
					
					<a data-scroll href="#iat-panel"><span class="fa fa-picture-o fa-picture-o-smm pull-left"></span></a>
					
					<a data-scroll href="#cuc-panel"><span class="fa fa-chain fa-chain-smm pull-left"></span></a>
					
					<a data-scroll href="#gpr-panel"><span class="pr-icon-smm pull-left">PR</span></a>
					
					<a data-scroll href="#cpi-panel"><span class="number-icon-smm pull-left">#</span></a>
					
					<a data-scroll href="#cuc-panel"><span class="gchain-icon-smm pull-left">G<span class="fa fa-chain fa-gchain-smm "></span></span></a>
					
					<a data-scroll href="#robots-panel"><span class="fa-star star-xs pull-left"></span></a>
					
					<a data-scroll href="#sitemap-panel"><span class="fa fa-check-circle fa-check-circle-xs pull-left"></span></a>

					<a data-scroll href="#pt-panel"><span class="fa fa-sitemap fa-sitemap-smm fa-sitemap-xs pull-left"></span></a>
					
					<a data-scroll href="#meta-d-panel"><span class="fa fa-file-o fa-file-o-xs pull-left"></span></a>
					
					<a data-scroll href="#meta-k-panel"><span class="seo-icon-smm seo-icon-xs pull-left">>_</span></a>
					
					<a data-scroll href="#wc-panel"><span class="seo2-icon-smm seo2-icon-xs pull-left"><\></span></a>
					
					<a data-scroll href="#twitter-panel"><span class="seo3-icon-smm seo3-icon-xs pull-left">W#</span></a>
										
					<a data-scroll href="#facebook-panel"><span class="fa fa-twitter fa-twitter-smm pull-left"></span></span></a>
					
					<a data-scroll href="#google-panel"><span class="fa fa-facebook fa-facebook-smm pull-left"></span></a>
					
					<a data-scroll href="#"><span class="fa fa-google-plus fa-google-plus-smm pull-left"></span></a>
					
				</div>
			</div>
		</div>
		</div>