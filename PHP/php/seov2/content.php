<!--panel 1-->
	<div class="row">
		<div class="panel" id="overall-perf">
		  <div class="panel-heading overal-performance-head"><span class="fa fa-list-alt panel-iceo-awesome pull-left"></span><span class="visible-phone pull-left">Overall</span><span class="hidden-phone pull-left">Overall Performance of</span><b class="donaimname"><?php echo $url ?></b></div>
		  <div class="panel-body overal-performance-body">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-3">
					<div class="radial-progress overal-radial">
						<input class="knob" data-fgColor="#0073cc" data-thickness=".1" data-width="130" data-height="130" readonly value="<?php echo $totalScore ?>">
						<script>
							$(function($) {
							$(".knob").knob();
							$(".knob").css('font-size','25px').css('color','#666666').css('font-family','Segoe UI').css('font-weight','100').css('display','inline-block');
							});
						</script>
					</div>
					
				</div>
				
				
				<div class="col-md-3 col-sm-3 col-md-offset-0 hidden-phone progress-bars-op">
					<div class="progress-bars">
					<span class="progress-label-left">Code:</span>
					<span class="progress-label-right1"><?php echo $codeScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-1" role="progressbar" style="width: <?php echo $codeScore ?>%;">
							<span class="sr-only"><?php echo $codeScore ?>% Complete</span>
						</div>
					</div>
					</div>
					
					<div class="progress-bars">
					<span class="progress-label-left">Search Engines:</span>
					<span class="progress-label-right2"><?php echo $searchEngineScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-2" role="progressbar" style="width: <?php echo $searchEngineScore ?>%;">
							<span class="sr-only"><?php echo $searchEngineScore ?>% Complete</span>
						</div>
					</div>
					</div>
					
					<div class="progress-bars">
					<span class="progress-label-left">SEO:</span>
					<span class="progress-label-right3"><?php echo $SEOScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-3" role="progressbar" style="width: <?php echo $SEOScore ?>%;">
							<span class="sr-only"><?php echo $SEOScore ?>% Complete</span>
						</div>
					</div>
					</div>
					
					<div class="progress-bars">
					<span class="progress-label-left">Social Media:</span>
					<span class="progress-label-right4"><?php echo $socialScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-4" role="progressbar" style="width: <?php echo $socialScore ?>%;">
							<span class="sr-only"><?php echo $socialScore ?>% Complete</span>
						</div>
					</div>
					</div>
				</div>
				
				<div class="col-xs-4 col-xs-offset-1 visible-phone pull-right progress-bars-op pb-relative" style="width:200px;">
					<div class="progress-bars">
					<span class="progress-label-left"style="font-size: 18px;">Code:</span>
					<span class="progress-label-right1" style="padding-left: 46%;"><?php echo $codeScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-1" role="progressbar" style="width: <?php echo $codeScore ?>%;">
							<span class="sr-only"><?php echo $codeScore ?>% Complete</span>
						</div>
					</div>
					</div>
					
					<div class="progress-bars">
					<span class="progress-label-left"style="font-size: 18px;">Search Engines:</span>
					<span class="progress-label-right2" style="padding-left: 0%;"><?php echo $searchEngineScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-2" role="progressbar" style="width: <?php echo $searchEngineScore ?>%;">
							<span class="sr-only"><?php echo $searchEngineScore ?>% Complete</span>
						</div>
					</div>
					</div>
					
					<div class="progress-bars">
					<span class="progress-label-left"style="font-size: 18px;">SEO:</span>
					<span class="progress-label-right3" style="padding-left: 53%;"><?php echo $SEOScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-3" role="progressbar" style="width: <?php echo $SEOScore ?>%;">
							<span class="sr-only"><?php echo $SEOScore ?>% Complete</span>
						</div>
					</div>
					</div>
					
					<div class="progress-bars">
					<span class="progress-label-left"style="font-size: 18px;">Social Media:</span>
					<span class="progress-label-right4" style="padding-left: 11%;"><?php echo $socialScore ?><span style="font-size: 12px;">%</span></span>
					<div class="progress bar-progress">
				  		<div class="progress-bar bar-4" role="progressbar" style="width: <?php echo $socialScore ?>%;">
							<span class="sr-only"><?php echo $socialScore ?>% Complete</span>
						</div>
					</div>
					</div>
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-0 col-md-offset-0 col-sm-offset-1 display-image main-preview hidden-phone">
					 <img class="apple-display" width="191px" src="<?php echo $img ?>" alt="preview">
				</div>
		    </div>
		  </div>
		</div>
	</div>

	<?php if(count($allstat) > 0): ?>
		<div class="row">
			<div class="competitors">Your competitors</div>
			<table class="table table-bordered table-hover table-condensed table-responsive">
				<thead>
					<tr>
						<th style="width:80px">Preview</th>
						<th>Website</th>
						<th>PageRank</th>
						<th>Alexa Rank</th>
						<th>Code</th>
						<th>Search Engines</th>
						<th>SEO</th>
						<th>Social</th>
						<th>Total Score</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($allstat as $key=>$value): ?>
						<?php $allstat = getScore($key, $allstat) ?>
					<?php endforeach ?>
					<?php foreach ($allstat as $key=>$value): ?>
						<tr>
							<td style="width:80px;"><img src="<?php echo $value['img'] ?>" width="70px"/></td>
							<?php $compUrls = parse_url($key); ?>
							<td><a href="<?php echo $key ?>" target="_blank"><?php echo $compUrls['host'] ?></a></td>
							<td><?php echo $value['getGoogleToolbarPageRank'] ?></td>
							<td><?php echo $value['getAlexaGlobalRank'] ?></td>
							<td><?php echo $value['codeScore'] ?></td>
							<td><?php echo $value['searchEngineScore'] ?></td>
							<td><?php echo $value['SEOScore'] ?></td>
							<td><?php echo $value['socialScore'] ?></td>
							<td><?php echo $value['totalScore'] ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>

		</div>
	<?php endif; ?>

<?php include ('panels.php'); ?>