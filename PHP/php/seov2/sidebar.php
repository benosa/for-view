
				<div class="sidebar" data-spy="affix" data-offset-top="190">
				<div class="accordion-group">
					<div class="panel-heading sidebar-head websitereview"><a data-scroll href="#overall-perf">
						<span class="fa fa-list-alt pull-left"></span>
						<div class="headingtext">WEBSITE REVIEW</div></a>
					</div>
				</div>
				
				<div class="accordion-group">
					<div class="panel-heading sidebar-head codereview"><a class="accordion-toggle" data-scroll href="#w3c-panel" data-parent="#accordion">
					<span class="glyphicon glyphicon-chevron-right pull-right"></span>
					<div class="headingtext">CODE</div></a>
					</div>
				</div>
				
				<div class="accordion-group">
					<div class="panel-heading sidebar-head se-review"><a class="accordion-toggle" data-scroll data-parent="#accordion" href="#gpr-panel">
					<span class="glyphicon glyphicon-chevron-right pull-right"></span>
						<div class="headingtext">SEARCH ENGINES</div></a>
					</div>
				</div>
				
				<div class="accordion-group">
					<div class="panel-heading sidebar-head seo-review"><a class="accordion-toggle" data-scroll data-parent="#accordion" href="#robots-panel">
					<span class="glyphicon glyphicon-chevron-right pull-right"></span>
						<div class="headingtext">SEO</div></a>
					</div>
				</div>
				
				<div class="accordion-group">
					<div class="panel-heading sidebar-head sm-review"><a class="accordion-toggle" data-scroll data-parent="#accordion" href="#twitter-panel">
					<span class="glyphicon glyphicon-chevron-right pull-right"></span>
						<div class="headingtext">SOCIAL MEDIA</div></a>
					</div>
				</div>
				
				<div class="accordion-group">
					<div class="panel-heading sidebar-head websitereview optimizesite">
						<a data-scroll href="#">
						<span class="fa fa-star star-sidebar pull-left"></span>
						<div class="headingtext">OPTIMIZE THIS SITE!</div></a>
					</div>
				</div>
				<div class="accordion-group">
					<div class="panel-heading sidebar-head websitereview optimizesite">
						<a data-scroll href="/getPdf.php">
						<span class="fa fa-star star-sidebar pull-left"></span>
						<div class="headingtext">GET PDF</div></a>
					</div>
				</div>
				</div>
