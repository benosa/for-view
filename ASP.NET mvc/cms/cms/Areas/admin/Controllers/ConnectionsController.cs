﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using ebay.Helpers;
using ebay.Areas.admin.Models;

namespace ebay.Areas.admin.Controllers
{
    public class ConnectionsController : Controller
    {
        //
        // GET: /admin/Settings/
        public ActionResult Index()
        {
            //List<AdminMenuModel> lm;
            //lm = DbContext<AdminMenuModel>.Current.OrderBy(x => x.IdMenu).ToList() as List<AdminMenuModel>;
            //ViewBag.TopMenu = lm;
            return View();
        }
        public ActionResult Connections()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Connections")).SubMenu.Find(x => x.Name.Contains("Connections")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
    }
}