﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using ebay.Helpers;
using ebay.Areas.admin.Models;

namespace ebay.Areas.admin.Controllers
{
    public class AccountingController : Controller
    {
        //
        // GET: /admin/Settings/
        public ActionResult Index()
        {
            //List<AdminMenuModel> lm;
            //lm = DbContext<AdminMenuModel>.Current.OrderBy(x => x.IdMenu).ToList() as List<AdminMenuModel>;
            //ViewBag.TopMenu = lm;
            return View();
        }
        public ActionResult Manager()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Manager")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Escrow()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Escrow")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Withdarawals()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Withdarawals")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult CreditCards()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Credit Cards")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult BankAccount()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Bank Account")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Reports()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Reports")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Currencies()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Accounting")).SubMenu.Find(x => x.Name.Contains("Currencies")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
    }
}