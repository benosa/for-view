﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using ebay.Helpers;
using ebay.Areas.admin.Models;

namespace ebay.Areas.admin.Controllers
{
    public class DistributionController : Controller
    {
        //
        // GET: /admin/Settings/
        public ActionResult Index()
        {
            //List<AdminMenuModel> lm;
            //lm = DbContext<AdminMenuModel>.Current.OrderBy(x => x.IdMenu).ToList() as List<AdminMenuModel>;
            //ViewBag.TopMenu = lm;
            return View();
        }
        public ActionResult Auctions()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Auctions")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Attachments()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Attachments")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult BidManager()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Bid Manager")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Verification()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Verification")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Referrals()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Referrals")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult BulkEmail()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Bulk Email")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Categories()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Categories")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Feeds()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Distribution")).SubMenu.Find(x => x.Name.Contains("Feeds")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        
    }
}