﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using ebay.Helpers;
using ebay.Areas.admin.Models;

namespace ebay.Areas.admin.Controllers
{
    public class SettingsController : Controller
    {
        //
        // GET: /admin/Settings/
        public ActionResult Index()
        {
            //List<AdminMenuModel> lm;
            //lm = DbContext<AdminMenuModel>.Current.OrderBy(x => x.IdMenu).ToList() as List<AdminMenuModel>;
            //ViewBag.TopMenu = lm;
            return View();
        }
        public ActionResult Global()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x=>x.Name.Contains("Settings")).SubMenu.Find(x=>x.Name.Contains("Global")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Marketplace()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Marketplace")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult PayModules()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Pay Modules")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Emails()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Emails")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Maintenance()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Maintenance")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Subscription()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Subscription")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Automation()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Automation")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Feedback()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Settings")).SubMenu.Find(x => x.Name.Contains("Feedback")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
	}
}