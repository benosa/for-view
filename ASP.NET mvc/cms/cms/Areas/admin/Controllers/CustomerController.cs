﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using ebay.Helpers;
using ebay.Areas.admin.Models;

namespace ebay.Areas.admin.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /admin/Settings/
        public ActionResult Index()
        {
            //List<AdminMenuModel> lm;
            //lm = DbContext<AdminMenuModel>.Current.OrderBy(x => x.IdMenu).ToList() as List<AdminMenuModel>;
            //ViewBag.TopMenu = lm;
            return View();
        }
        public ActionResult Management()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Customer")).SubMenu.Find(x => x.Name.Contains("Management")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Skills()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Customer")).SubMenu.Find(x => x.Name.Contains("Skills")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Abuse()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Customer")).SubMenu.Find(x => x.Name.Contains("Abuse")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }
        public ActionResult Nonprofits()
        {
            List<AdminMenuModel> lm;
            lm = DbContext<AdminMenuModel>.Current.ToList<AdminMenuModel>().Find(x => x.Name.Contains("Customer")).SubMenu.Find(x => x.Name.Contains("Nonprofits")).SubMenu;
            ViewBag.GlobalSubMenu = lm;
            return View();
        }


    }
}