#!bin/bash

function search_array() {
    index=0
    while [ "$index" -lt "${#arr[@]}" ]; do
        #if [ "${arr[$index]}" = "$1" ]; then
        if [[ "${arr[$index]}" =~ "$1" ]]; then
            echo $index
            return
        fi
        let "index++"
	done
        echo ""
}

# 1. Create ProgressBar function
# 1.1 Input is currentState($1) and totalState($2)
function ProgressBar {
# Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")

# 1.2 Build progressbar strings and print the ProgressBar line
# 1.2.1 Output example:
# 1.2.1.1 Progress : [########################################] 100%
printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"

}

# Variables
#_start=1

# This accounts as the "totalState" variable for the ProgressBar function
#_end=`wc -l production.log`

# Proof of concept
#for number in $(seq ${_start} ${_end})
#do
#    sleep 0.1
#    ProgressBar ${number} ${_end}
#done


#arr=`cat production.log | grep -P -o '"(lat)"=>"([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))", "(lon)"=>"([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))"' | sed 's/"//g' | sed 's/lon/lng/g' | sed 's/=>/:/g' | sed 's//,count:1/g'`
val=`(cat ./production.log | grep -P -o '"(lat)"=>"([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))", "(lon)"=>"([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))"' | sed 's/"//g' | sed 's/lon/lng/g' | sed 's/=>/:/g' | awk '{print $0", count:1"}' | grep "$1" | grep "$2" | sort | uniq -c |  awk '{gsub("count:1", "count:"$1, $0);print "{"$2 $3 $4"}"}' | awk '{printf "%s,",$0} END {print ""}')`
val2=`cat log2.txt | grep -P -o '"(lat)"=>"([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))", "(lon)"=>"([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))"' | sed 's/"//g' | sed 's/=>/:/g' | sed 's/lat://g' | sed 's/lon://g' | sed 's/,//g' |  awk '{printf ("lat:%.2f,  lng:%.2f, count:1\n", $1, $2)'} | grep "$1" | grep "$2" | sort | uniq -c |  awk '{gsub("count:1", "count:"$1, $0);print "{"$2 $3 $4"}"}' | awk '{printf "%s,",$0} END {print ""}' | sed 's/^\(.*\).$/\1/' >> data.js && echo "heatmap.setData(testData);" >> data.js`
set -- $val
printf "$1\n"
printf "var testData = {\n" >> data.js
printf "	max: 8,\n" >> data.js
printf '	data:[' >> data.js

printf '};\n' >> data.js

printf 'heatmap.setData(testData);'  >> data.js
printf "\n"
##printf '\nFinished!\n'
