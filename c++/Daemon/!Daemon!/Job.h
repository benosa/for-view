#pragma once
#include "HostRecord.h"
#include "source_proxy.h"

#include "Poco/String.h"
#include "Poco/Format.h"
#include "Poco/Exception.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include <Poco/Net/HTTPCredentials.h>
#include "Poco/StreamCopier.h"
#include "Poco/NullStream.h"
#include "Poco/Path.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"
#include <Poco/Util/Application.h>
#include <Poco/Runnable.h>
#include "Poco/RegularExpression.h"
#include "Poco/Net/IPAddress.h"
#include "Poco/NumberFormatter.h"
#include <vector>

/*
* Poco JSON
*/
#include <Poco/JSON/JSON.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Dynamic/Var.h>

using namespace std;
using namespace Poco::JSON;

using Poco::Runnable;
using Poco::Util::Application;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::NullOutputStream;
using Poco::StreamCopier;
using Poco::Path;
using Poco::URI;
using Poco::Exception;
using Poco::RegularExpression;
using Poco::NumberFormatter;
using Poco::format;


extern Poco::Timestamp t1;

class Job : public Runnable
{
public:

	Job(Poco::SharedPtr<HostRecord> * hr,Poco::SharedPtr<source_proxy>* scr);
	~Job(void);
	void run();
private:
	Poco::SharedPtr<source_proxy>* _scr;
	bool Job::doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response,std::string host_port);
	Poco::SharedPtr<HostRecord> * _hr;
	HTTPClientSession* session;
};
