#include "WorkerThreadPool.h"


WorkerThreadPool::WorkerThreadPool(void)
{
	 //tp = new ThreadPool(334,334);
	tp = new ThreadPool(50,500);
}


WorkerThreadPool::~WorkerThreadPool(void)
{
	tp->joinAll();
    delete tp;
}

WorkerThreadPool& WorkerThreadPool::getInstance()
{
   static WorkerThreadPool instance;
   return instance;
}