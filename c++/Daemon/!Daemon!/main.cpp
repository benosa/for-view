/*
*	Personal include
*/
#include "WorkerThreadPool.h"
#include "Job.h"
#include "HostRecord.h"
#include "source_proxy.h"
/*
*	Data include
*/
#include "Poco/Data/LOB.h"
#include "Poco/Data/Session.h"
#include "Poco/Data/StatementImpl.h"

/*
*	ODBC Postgresql include
*/
#include "Poco/Data/ODBC/ODBC.h"
#include "Poco/Data/ODBC/Connector.h"
#include "Poco/Data/ODBC/Utility.h"
#include "Poco/Data/ODBC/Diagnostics.h"
#include "Poco/Data/ODBC/ODBCException.h"
#include "Poco/Data/SessionPool.h"

/*
*	HTTP  include
*/
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include <Poco/Net/HTTPCredentials.h>
#include "Poco/Net/IPAddress.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/StreamCopier.h"

#include "Poco/Path.h"
#include "Poco/URI.h"

/*
*	Utils  include
*/
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Task.h"
#include "Poco/TaskManager.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/StreamCopier.h"
#include "Poco/NullStream.h"
#include "Poco/Exception.h"
#include "Poco/RegularExpression.h"
#include "Poco/NumberFormatter.h"
#include "Poco/String.h"
#include "Poco/Format.h"

/*
*	Timer  include
*/
#include "Poco/Timer.h"
#include "Poco/Thread.h"
/*
*	STD  include
*/
#include <iostream>
#include <list>
#include <iterator>



using Poco::Util::Application;
using Poco::Util::ServerApplication;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::OptionCallback;
using Poco::Util::HelpFormatter;
using Poco::Task;
using Poco::TaskManager;
using Poco::DateTimeFormatter;
using namespace Poco::Data;
using namespace Poco::Data::Keywords;
using Poco::Data::ODBC::ConnectionException;
using Poco::Data::ODBC::StatementException;
using Poco::Data::ODBC::StatementDiagnostics;
using Poco::Data::CLOB;
using Poco::Data::ODBC::Utility;
using Poco::Data::ODBC::ODBCException;


using Poco::format;
using Poco::NotFoundException;
using Poco::Util::Application;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::StreamCopier;
using Poco::Path;
using Poco::URI;
using Poco::Exception;
using Poco::RegularExpression;
using Poco::NumberFormatter;

using Poco::Timer;
using Poco::TimerCallback;

#define POCO_ODBC_TEST_DATABASE_SERVER "localhost"
#ifdef POCO_ODBC_USE_MAMMOTH_NG
	#define POSTGRESQL_ODBC_DRIVER "Mammoth ODBCng Beta"
#elif defined (POCO_ODBC_UNICODE)
	#define POSTGRESQL_ODBC_DRIVER "PostgreSQL Unicode"
	#define POSTGRESQL_DSN "PocoDataPgSQLTestW"
#else
	#define POSTGRESQL_ODBC_DRIVER "PostgreSQL ODBC Driver(ANSI)"
	#define POSTGRESQL_DSN "PocoDataPgSQLTest"
#endif

#define POSTGRESQL_SERVER POCO_ODBC_TEST_DATABASE_SERVER
#define POSTGRESQL_PORT    "5432"
#define POSTGRESQL_DB      "proxy"
#define POSTGRESQL_UID     "postgres"
#define POSTGRESQL_PWD     "test"
#define POSTGRESQL_VERSION "9.3"

#ifdef POCO_OS_FAMILY_WINDOWS
const std::string _libDir = "C:\\\\Program Files\\\\PostgreSQL\\\\" POSTGRESQL_VERSION "\\\\lib\\\\";
#else
const std::string _libDir = "/usr/local/pgsql/lib/";
#endif


std::string _driver = POSTGRESQL_ODBC_DRIVER;
std::string _dsn = POSTGRESQL_DSN;
std::string _uid = POSTGRESQL_UID;
std::string _pwd = POSTGRESQL_PWD;
std::string _connectString = 
	"DRIVER=" POSTGRESQL_ODBC_DRIVER ";"
	"DATABASE=" POSTGRESQL_DB ";"
	"SERVER=" POSTGRESQL_SERVER ";"
	"PORT=" POSTGRESQL_PORT ";"
	"UID=" POSTGRESQL_UID ";"
	"PWD=" POSTGRESQL_PWD ";"
	"SSLMODE=prefer;"
	"LowerCaseIdentifier=0;"
	"UseServerSidePrepare=0;"
	"ByteaAsLongVarBinary=1;"
	"BI=0;"
	"TrueIsMinus1=0;"
	"DisallowPremature=0;"
	"UpdatableCursors=0;"
	"LFConversion=1;"
	"CancelAsFreeStmt=0;"
	"Parse=0;"
	"BoolsAsChar=1;"
	"UnknownsAsLongVarchar=0;"
	"TextAsLongVarchar=1;"
	"UseDeclareFetch=0;"
	"Ksqo=1;"
	"Optimizer=1;"
	"CommLog=0;"
	"Debug=0;"
	"MaxLongVarcharSize=8190;"
	"MaxVarcharSize=254;"
	"UnknownSizes=0;"
	"Socket=8192;"
	"Fetch=100;"
	"ConnSettings=;"
	"ShowSystemTables=0;"
	"RowVersioning=0;"
	"ShowOidColumn=0;"
	"FakeOidIndex=0;"
	"ReadOnly=0;";


static std::list<HostRecord>* Hosts = NULL;
static bool Lock = false;
std::map<int,std::list<Poco::SharedPtr<HostRecord>>> m;
static bool run_send = false;
std::list<Poco::SharedPtr<Timer>> l_Timers;

/*struct source_proxy{
		int server_id;
		std::string name;
		std::string main_url;
		std::string login;
		std::string password;
		std::string list_url;
		std::string live_time_url;
		int life_time;
		Poco::Timestamp* t;
		bool rotation;
	};
	*/

SessionPool pool("ODBC", _connectString);

Poco::Timestamp t1;

class TimerWriteToPostgress{
public:
	Poco::SharedPtr<Poco::Data::Session>  _pSession;
	std::string SQLStr,SQLStr_begin,SQLStr_end;
	int serv_id;
	//Poco::Timestamp* t1;
	Poco::SharedPtr<source_proxy>* new_source_proxy;

	TimerWriteToPostgress(){
		serv_id=0;
		//t1 = new Poco::Timestamp(0);
	}
	
	void onTimer(Poco::Timer& timer){
		run_send = true;
		new_source_proxy->get()->t->update();
		//new_source_proxy.t = t1;
		cout<<"Updating server name is "<<(new_source_proxy->get())->name<<endl;
		if(serv_id != 0){
			TimerUpdateProxyList();
			if(new_source_proxy->get()->rotation = 1){
				(new_source_proxy->get())->life_time = 600-5;
			}else{
				(new_source_proxy->get())->life_time = 900;
			}
		}else{
			//����� �� ��������� � ������������� - ������, ��� ���-�� ����� �� ���;
		}

	}
	void TimerUpdateProxyList(){
		std::string str = (new_source_proxy->get())->list_url;
		URI uri(str);
		std::string path(uri.getPathAndQuery());
		if (path.empty()) path = "/";
		HTTPClientSession session(uri.getHost(), uri.getPort());
		HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
		HTTPResponse response;
		std::vector<std::string> rList = doRequest(session, request, response);
		std::list<Poco::SharedPtr<HostRecord>>* Hosts  = new std::list<Poco::SharedPtr<HostRecord>>();
		HostRecord* rec;
		//Lock = true;
		//WorkerThreadPool::getInstance().tp->stopAll();
		for(std::vector<std::string>::iterator it = rList.begin(),it_end = rList.end();it != it_end;++it){
			Poco::Net::SocketAddress sa(*it);
			std::string host = sa.host().toString();
			UINT16 _port = sa.port();
			std::string port = NumberFormatter::format(sa.port());
			rec = new HostRecord();
			rec->host_port = host+":"+port;
			rec->server_id = serv_id;
			Poco::SharedPtr<HostRecord> pHr(rec);
			Hosts->push_back(pHr);
			Poco::Mutex mu;
			mu.lock();
				std::map<int,std::list<Poco::SharedPtr<HostRecord>>>::iterator iter = m.find(serv_id);
				m.erase(iter);
				m[serv_id] = *Hosts;
			mu.unlock();
			//Lock = false;
		}
	}
	std::vector<std::string> doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response)
	{
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		std::vector<std::string> _tmpLHr;
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			RegularExpression re2("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}\\b",Poco::RegularExpression::RE_EXTRA |
				Poco::RegularExpression::RE_MULTILINE);
			std::stringstream ss;
			Poco::StreamCopier::copyStream(rs, ss);
			Poco::RegularExpression::Match match;
			match.offset = 0;
			
			while(0 != re2.match(ss.str(), match.offset, match))
			{
				std::string foundStr(ss.str().substr(match.offset, match.length));
				_tmpLHr.push_back(foundStr);
				match.offset += match.length;
			}
			return _tmpLHr;
		}
		else
		{
			Poco::NullOutputStream null;
			StreamCopier::copyStream(rs, null);
			return _tmpLHr;
		}
	}

};




inline std::list<Poco::SharedPtr<HostRecord>>* getPackFromDatabase(){
	bool beenHere = false;
	HostRecord* rec = new HostRecord;
	std::list<Poco::SharedPtr<HostRecord>>* host_list = new std::list<Poco::SharedPtr<HostRecord>>();
	//int id;
	std::string host;
	std::string port;
	Poco::SharedPtr<Poco::Data::Session>  _pSession;
	if (!beenHere)
	{
		for(std::map<int,std::list<Poco::SharedPtr<HostRecord>>>::iterator it = m.begin(),end = m.end();it != end; ++it){
			std::list<Poco::SharedPtr<HostRecord>>* temp_host_list = &(*it).second;

			host_list->insert(host_list->end(), temp_host_list->begin(), temp_host_list->end());
		}
	}
	beenHere = true;
	return host_list;
}

class PostTask: public Task
{
public:
	~PostTask(){}
	PostTask(): Task("PostTask")
	{

	}
	void runTask()
	{
		Poco::Net::SocketAddress sa("localhost", 3232);
		Poco::Net::StreamSocket socket(sa);
		socket.setReceiveTimeout(300);

		//Poco::Net::SocketStream str(socket);
		std::string str1;
		//str >> str1;
		while(1){
			if(run_send){
				Poco::Mutex mu;
				Poco::ScopedLock<Poco::Mutex> l(mu);
				/*for(int i = 0; i < Hosts->size();i++){
					cout<<((HostRecord*)Hosts[i])->
				}*/
				for(std::map<int,std::list<Poco::SharedPtr<HostRecord>>>::iterator it = m.begin(),end = m.end();it != end; ++it){
					std::list<Poco::SharedPtr<HostRecord>>& hr = it->second;
					for(std::list<Poco::SharedPtr<HostRecord>>::iterator it1 = hr.begin(),end = hr.end();it1 != end; ++it1){
						if((*it1)->called == true){
							HostRecord& hr = *it1->get();
							/*#format of queries{ proxy_host: "192.168.0.1", 
								proxy_port : 3421, : country : "USA", geo_city : "North Hermina", 
								geo_state : "Minnesota", external_ip : "192.168.0.1", 
								lat : -65.43481659073895, lon : 34.69032278286153, dynamic : true, 
								status : "on", expires_at : Time.now + 15.min }*/
							Poco::Net::SocketAddress ss(hr.host_port);
							//socket.connect();
							//std::string tempstr;
							//Poco::FIFOBuffer fifo();
							std::string rst = 
								"{ proxy_host: \"" + ss.host().toString() + "\", \
								   proxy_port: \"" + Poco::NumberFormatter::format(ss.port()) + "\", \
								   country: \"" + hr.country + "\", \
								   geo_city: \"" + hr.geo_city + "\", \
								   geo_state: \"" + hr.geo_state + "\", \
							       eternal_ip: \"" + hr.external_ip + "\",\
								   lat: " + Poco::NumberFormatter::format(hr.lat) + ", \
								   lon: " + Poco::NumberFormatter::format(hr.lon) + ", \
								   status: \"" + hr.status + "\", \
								   expires_at: " + Poco::NumberFormatter::format(hr.expires_at) + "}";
							char buffer[1000];
							
							while (1){
								socket.sendBytes(rst.data(), rst.size());
								//socket.receiveBytes(&fifo);
								sleep(5);
								const int availableBytes = socket.available();
								if (availableBytes > 0) {
									unsigned int len = socket.receiveBytes(buffer, 1000, 0);
									buffer[len] = 0;
								}
								cout << buffer << endl;
								if (buffer == "ok")break;
							}
						}
					}
				}
			}
		}
	}
	
};

class MainTask: public Task
{
public:
	typedef Poco::Data::ODBC::Utility::DriverMap Drivers;

	~MainTask(){
		Poco::Thread::sleep(5000);
		//timer.stop();
	}
	MainTask(): Task("MainTask")
	{
		Drivers _drivers;
		ODBC::Connector::registerConnector();
		Utility::drivers(_drivers);
	}
	
	void runTask()
	{
		Update();
		Poco::Timestamp t1(0);
		while(true){
			
			if(/*!Lock && */t1.elapsed() >= 60000000){
				t1.update();
				std::list<Poco::SharedPtr<HostRecord>>* Hosts;
				Poco::Mutex mu;
				mu.lock();
				Hosts = getPackFromDatabase();
					std::list<Poco::SharedPtr<HostRecord>>::iterator cur_begin = Hosts->begin();
					std::list<Poco::SharedPtr<HostRecord>>::iterator cur_end = Hosts->end();
						Job* worker;
						int i = 0;
						for(std::list<Poco::SharedPtr<HostRecord>>::iterator iterator = cur_begin,end = cur_end;iterator != cur_end; ++iterator){ //�������� 334 ������ ���
							worker = new Job(&(*iterator),map_ptr_for_speed_access_source[(*iterator)->server_id]);
							try{
								WorkerThreadPool::getInstance().tp->start(*worker);
								int s = WorkerThreadPool::getInstance().tp->available();
								cout << "I am create "<<i<<" worker!!!"<<endl;
								cout << "Avaible " << WorkerThreadPool::getInstance().tp->available()<<" workers"<<endl;
							}catch(...){
								cout << "Error"<<endl;
							}
							cur_begin = iterator;
							i++;
						}
						mu.unlock();
						WorkerThreadPool::getInstance().tp->joinAll();
						i=0;
						sleep(1000);
			}
		}
	}
private:
	std::vector<Poco::SharedPtr<source_proxy>*> proxy_source;
	std::map<int,Poco::SharedPtr<source_proxy>*> map_ptr_for_speed_access_source;
	//std::list<Poco::SharedPtr<source_proxy>*> temp_list_shared_ptr;
	Poco::SharedPtr<source_proxy>* new_source_proxy;
	std::vector<std::string> proxy_list;
	
	void Update(){
		Poco::SharedPtr<Poco::Data::Session>  _pSession;
		try
		{
			_pSession = new Session(pool.get());
			_pSession->setFeature("autoCommit", true);
		}catch (ConnectionException& ex)
		{
			/*
			*	����� �� ����������� ������� � ���� � ��������� �� ���� � �������
			*/
			Application::instance().logger().information("!!! WARNING: Connection failed. MySQL tests will fail !!!" );
			Application::instance().logger().information(ex.displayText());
			Application::instance().logger().information("Application now exit!");
			//Application::instance().~Application();
		}

		if (_pSession && _pSession->isConnected()) 
			/*
			* �����, ���� ���� ���������� � ����� - �� ����� �������� ��������� ������ ��������������
			*/
			Application::instance().logger().information("*** Connected to ("+ _connectString + ")");
			Statement select(*_pSession);

			source_proxy* spy;// = new source_proxy();
			//Poco::SharedPtr<source_proxy>* _new_source_proxy;
			
			//new_source_proxy  = _new_source_proxy;

			//source_proxy* sprt = *(new_source_proxy->get());
			//source_proxy* sp = sprt;
			
			//source_proxy* sp = new source_proxy;
			source_proxy* scp = new source_proxy;
			select << "SELECT id,name,main_url,login,password,list_url,live_time_url,rotation FROM proxy_sources WHERE rotation = 'true';",
				into(scp->server_id),into(scp->name),into(scp->main_url),into(scp->login),into(scp->password),
				into(scp->list_url),into(scp->live_time_url),into(scp->rotation),
			range(0, 1); //  iterate over result set one row at a time

			while (!select.done())
			{
				select.execute();
				spy = new source_proxy();
				spy = scp;
				new_source_proxy = new Poco::SharedPtr<source_proxy>(spy);
				proxy_source.push_back(new_source_proxy);
				//temp_list_shared_ptr.push_back(new_source_proxy);
				map_ptr_for_speed_access_source[new_source_proxy->get()->server_id] = new_source_proxy;
			}
			/*
			* ���������� ������ ����������
			* ������ ������� �� ����� �������������� ������
			*/
			std::vector<std::string> proxy_list;
			for(std::vector<Poco::SharedPtr<source_proxy>*>::iterator it_begin = proxy_source.begin(),it_end = proxy_source.end();it_begin != it_end;++it_begin){
				try{
				std::string str = ((*it_begin)->get())->list_url;
				URI uri(str);
				std::string path(uri.getPathAndQuery());
				if (path.empty()) path = "/";
				HTTPClientSession session(uri.getHost(), uri.getPort());
				HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
				HTTPResponse response;
				
				/*
				*	����� �������� ��� ������ ������� ������������� � ����
				*/
					proxy_list = doRequest(session, request, response,((*it_begin)->get())->rotation);
					//���� �� ��������� ����� � proxy_list ������ ������� ������� ����� ��������
					std::list<Poco::SharedPtr<HostRecord>>* Hosts  = new std::list<Poco::SharedPtr<HostRecord>>();
					HostRecord* rec;
					/*
					* ���� �������� ����� �������� ������� - �� �������� ����� �� ��������� �������
					*/
					if(((*it_begin)->get())->rotation){
						str = ((*it_begin)->get())->live_time_url;
						URI uri_for_time(str);
						std::string path_for_time(uri_for_time.getPathAndQuery());
						if (path_for_time.empty()) path_for_time = "/";
						HTTPClientSession session_for_time(uri_for_time.getHost(), uri_for_time.getPort());
						HTTPRequest request_for_time(HTTPRequest::HTTP_GET, path_for_time, HTTPMessage::HTTP_1_1);
						HTTPResponse response_for_time;
						int _ss = doRequestTime(session_for_time, request_for_time, response_for_time);
							((*it_begin)->get())->life_time =  _ss;
						if(_ss > 0){
							Timer* t = new Timer(_ss*1000+5,600*1000);
							Poco::SharedPtr<Timer> pTm(t);
							l_Timers.push_back(pTm);
							TimerWriteToPostgress* te = new TimerWriteToPostgress();
							Poco::Mutex mu;
							//Poco::ScopedLock<Poco::Mutex> m(mu);
							mu.lock();
							te->serv_id = ((*it_begin)->get())->server_id;
							te->new_source_proxy = *it_begin;
							(te->new_source_proxy->get())->t = new Poco::Timestamp();
							(te->new_source_proxy->get())->t->update();
							t->start(TimerCallback<TimerWriteToPostgress>(*te,&TimerWriteToPostgress::onTimer));	
							mu.unlock();
							Poco::Thread::sleep(1000);
						}
					}else{
							((*it_begin)->get())->life_time = 900;
							Timer* t = new Timer(900*1000,900*1000);
							Poco::SharedPtr<Timer> pTm(t);
							l_Timers.push_back(pTm);
							TimerWriteToPostgress* te = new TimerWriteToPostgress();
							Poco::Mutex mu;
							//Poco::ScopedLock<Poco::Mutex> m(mu);
							mu.lock();
							te->serv_id = ((*it_begin)->get())->server_id;
							te->new_source_proxy = *it_begin;
							(te->new_source_proxy->get())->t = new Poco::Timestamp();
							(te->new_source_proxy->get())->t->update();
							t->start(TimerCallback<TimerWriteToPostgress>(*te,&TimerWriteToPostgress::onTimer));	
							mu.unlock();
							Poco::Thread::sleep(1000);
					}
					/*
					* ���������� ���� ��������� ������� � ���������� ��� � ���
					*/
					for(std::vector<std::string>::iterator it = proxy_list.begin(),it_end = proxy_list.end();it != it_end;++it){
						Poco::Net::SocketAddress sa(*it);
						std::string host = sa.host().toString();
						UINT16 _port = sa.port();
						std::string port = NumberFormatter::format(sa.port());
						rec = new HostRecord();
						rec->host_port = host+":"+port;
						rec->server_id = ((*it_begin)->get())->server_id;
						Poco::SharedPtr<HostRecord> pHr(rec);
						Hosts->push_back(pHr);
						m[rec->server_id] = *Hosts;
				
						//m.
					}	
					}catch(...){
					}
			}
			

	}
	int doRequestTime(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response){
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			RegularExpression re2("\\b\\d{1,3}\\b",Poco::RegularExpression::RE_EXTRA | Poco::RegularExpression::RE_MULTILINE);
			std::stringstream ss;
			Poco::StreamCopier::copyStream(rs, ss);
			Poco::RegularExpression::Match match;
			match.offset = 0;
			std::string tmp = "";
			while(0 != re2.match(ss.str(), match.offset, match))
			{
				std::string foundStr(ss.str().substr(match.offset, match.length));
				tmp = foundStr;
				match.offset += match.length;
			}
			return Poco::NumberParser::parse(tmp);
		}
		return 0;
	}
	std::vector<std::string> doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response, bool rotation)
	{
		//bool b = (rotation == "1") ? true : false;
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		std::vector<std::string>* strl = new std::vector<std::string>();
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			if(rotation){
				std::copy(
					std::istream_iterator<std::string>(rs), 
					std::istream_iterator<std::string>(), 
					std::back_inserter(*strl));
			}else{
				RegularExpression re2("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}\\b",Poco::RegularExpression::RE_EXTRA |
					Poco::RegularExpression::RE_MULTILINE);
				std::stringstream ss;
				Poco::StreamCopier::copyStream(rs, ss);
				Poco::RegularExpression::Match match;
				match.offset = 0;
				while(0 != re2.match(ss.str(), match.offset, match))
				{
					std::string foundStr(ss.str().substr(match.offset, match.length));
					strl->push_back(foundStr);
					match.offset += match.length;
				}
			}
			return *strl;
		}
		else
		{
			Poco::NullOutputStream null;
			StreamCopier::copyStream(rs, null);
			return *strl;
		}
	}
};


class SampleServer: public ServerApplication
{
public:
	SampleServer(): _helpRequested(false)
	{
	}
	
	~SampleServer()
	{
	}

protected:
	void initialize(Application& self)
	{
		loadConfiguration(); // load default configuration files, if present
		ServerApplication::initialize(self);
		logger().information("starting up");
	}
		
	void uninitialize()
	{
		logger().information("shutting down");
		ServerApplication::uninitialize();
	}

	void defineOptions(OptionSet& options)
	{
		ServerApplication::defineOptions(options);
		
		options.addOption(
			Option("help", "h", "display help information on command line arguments")
				.required(false)
				.repeatable(false)
				.callback(OptionCallback<SampleServer>(this, &SampleServer::handleHelp)));
	}

	void handleHelp(const std::string& name, const std::string& value)
	{
		_helpRequested = true;
		displayHelp();
		stopOptionsProcessing();
	}

	void displayHelp()
	{
		HelpFormatter helpFormatter(options());
		helpFormatter.setCommand(commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("A sample server application that demonstrates some of the features of the Util::ServerApplication class.");
		helpFormatter.format(std::cout);
	}

	int main(const std::vector<std::string>& args)
	{
		if (!_helpRequested)
		{
			TaskManager tm;
			tm.start(new MainTask);
			tm.start(new PostTask);
			waitForTerminationRequest();
			tm.cancelAll();
			tm.joinAll();
		}
		return Application::EXIT_OK;
	}
	
private:
	bool _helpRequested;
};


POCO_SERVER_MAIN(SampleServer)
