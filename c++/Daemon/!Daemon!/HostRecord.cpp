#include "HostRecord.h"


HostRecord::HostRecord(void)
{
	int id = 0;
	std::string host_port = "NULL";
	char is_blocked;
	std::string external_ip = "NULL";
	std::string status = "NULL";
	std::string country = "NULL";
	std::string geo_state = "NULL";
	std::string geo_city = "NULL";
	float lat = 0;
	float lon = 0;
	DateTime  expires_at = 0;
	int requests_count = 0;
	called = false;
}


HostRecord::~HostRecord(void)
{
}

std::string HostRecord::toString(){
	std::string str = "";
	StringTokenizer t1(host_port, ":");
	std::string str1 = t1[0];
	std::string str2 = t1[1];
	str = "('"+ Poco::NumberFormatter::format(id)+"','"+str1+"','"+str2+"','"+external_ip+"','"+status+"','"+country+"','"+geo_state+"','"+geo_city+
		"','"+Poco::NumberFormatter::format(lat)+"','"+Poco::NumberFormatter::format(lon)+
		"','"+Poco::DateTimeFormatter::format(expires_at)+"','"+Poco::NumberFormatter::format(requests_count)+"')";
	return str;
}