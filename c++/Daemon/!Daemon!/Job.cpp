#include "Job.h"
#include <typeinfo>
#define part_url "http://193.188.254.38/ip.php"

inline string GetValue(Object::Ptr aoJsonObject, const char *aszKey) {
    Poco::Dynamic::Var loVariable;
    string lsReturn;
    string lsKey(aszKey);
    loVariable = aoJsonObject->get(lsKey);
    lsReturn = loVariable.convert<std::string>();
    return lsReturn;
}

Job::Job(Poco::SharedPtr<HostRecord> * hr,Poco::SharedPtr<source_proxy>* scr)
{
	if(hr == NULL){
		//������������� �������� - ��������� �����
		this->~Job();
	}
	_scr = scr;
	_hr = hr;
}


Job::~Job(void)
{
	delete session;
}

void Job::run()
{
	Poco::Util::Application &app = Application::instance();
	try
	{
		Poco::Net::SocketAddress sa((_hr->get())->host_port);
		std::string _host = sa.host().toString();
		Poco::UInt16 _port = sa.port();
		URI uri(part_url);
		std::string path(uri.getPathAndQuery());
		if (path.empty()) path = "/";
		session = new HTTPClientSession(uri.getHost(), uri.getPort());
		HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
		session->setProxy(_host, _port);
		session->setKeepAliveTimeout(Poco::Timespan(1000000));
		HTTPResponse response;
		if (!doRequest(*session, request, response,(_host+NumberFormatter::format(_port))))
		{
			/*
			* ����� ��������� ������ �������
			*/
		}
	}
	catch (Exception& exc)
	{
		(_hr->get())->status = "Off";
		app.logger().information(exc.displayText());
		return;
	}
}

bool Job::doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response,std::string host_port)
{
	Parser loParser;
    Poco::Dynamic::Var loParsedJson;
    Poco::Dynamic::Var loParsedJsonResult;
    Object::Ptr loJsonObject;
	session.sendRequest(request);
	std::istream& rs = session.receiveResponse(response);
	cout << "Proxy Id is "<< Poco::NumberFormatter::format((_hr->get())->server_id) << endl;
	try{
		if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
			try{
				loParsedJson = loParser.parse(rs);
				loParsedJsonResult = loParser.result();
				loJsonObject = loParsedJsonResult.extract<Object::Ptr>();
			}
			catch(Poco::JSON::JSONException& e){
				Application::instance().logger().information("!!!!!!!!!!!!!!!!!");
				Application::instance().logger().information(e.displayText());
				Application::instance().logger().information(e.what());
				Application::instance().logger().information("!!!!!!!!!!!!!!!!!");
			}
				
			std::string ext_ip = GetValue(loJsonObject, "ext_ip");
			std::string country = GetValue(loJsonObject, "country");
			std::string state = GetValue(loJsonObject, "state");
			std::string city = GetValue(loJsonObject, "city");
			std::string latitude = GetValue(loJsonObject, "latitude");
			std::string longitude = GetValue(loJsonObject, "longitude");
			Poco::Net::SocketAddress sa((_hr->get())->host_port);
			std::string host = sa.host().toString();
		
			(_hr->get())->external_ip = ext_ip;
			(_hr->get())->country = country;
			(_hr->get())->geo_state = state;
			(_hr->get())->geo_city = city;
			(_hr->get())->lat = Poco::NumberParser::parseFloat(latitude);
			(_hr->get())->lon = Poco::NumberParser::parseFloat(longitude);
			(_hr->get())->expires_at = (_scr->get())->life_time - ((_scr->get())->t->elapsed()/1000000);
			(_hr->get())->called = true;
			cout << "Proxy Host:Port is "<< host_port<< "; Proxy real_ip is "<<(_hr->get())->external_ip
				<<"; Proxy expired_at: "<<Poco::NumberFormatter::format((_hr->get())->expires_at) << endl;
		}else{
			(_hr->get())->status = "Off";
			Poco::Net::HTTPResponse::HTTPStatus status = response.getStatus();
			cout << status << endl;
		}
	}catch(...){
		(_hr->get())->status = "Off";
		return false;
	}
		return true;
}