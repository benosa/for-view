#include "PostTask.h"
#include "Poco/Util/ServerApplication.h"


PostTask::PostTask(void): Task("PostTask")
	{
		connectionHandler = new ConnectionHandler(_host_t, _port_t);
		if (!connectionHandler->connect()) {
			int i = false;
			Poco::Util::Application::instance().logger().error("Cannot connect to " + _host_t +
				":" + Poco::NumberFormatter::format(_port_t));
			//std::cerr << "Cannot connect to " << _host_t << ":" << _port_t << std::endl;
			//std::string stt = "Cannot connect to " + (std::string)_host_t + ':' +Poco::NumberFormatter::format(_port_t);
			//poco_bugcheck_msg("i has invalid value");
		}
	}


PostTask::~PostTask(void)
{

	delete connectionHandler;
	Poco::Util::Application::instance().logger().fatal("Entering destructor for PostTask");
}

void PostTask::runTask()
	{
		std::string str1;
		while(1){
			while (!q.empty()){
				try{
					HostRecord& hr = q.front();
					if(hr.host_port != "" || !hr.host_port.empty()){
							
										Poco::Net::SocketAddress ss(hr.host_port);
					std::string rst = 
						"{ \"proxy_host\": \"" + ss.host().toString() + "\",\"proxy_port\": \"" + 
						Poco::NumberFormatter::format(ss.port()) + "\", \"country\": \"" + 
						hr.country + "\",  \"geo_city\": \"" + 
						hr.geo_city + "\", \"geo_state\": \"" + 
					    hr.geo_state + "\", \"external_ip\": \"" + 
						hr.external_ip + "\", \"lat\": " + 
						Poco::NumberFormatter::format(hr.lat) + ", \"lon\": " + 
						Poco::NumberFormatter::format(hr.lon) + ", \"dynamic\": \"" + 
						Poco::NumberFormatter::format(hr.dynamic) + "\" , \"status\": \"" + 
						hr.status + "\", \"expires_at\": " + 
						Poco::NumberFormatter::format(hr.expires_at) + "}";
					if(ss.host().toString() != "" && Poco::NumberFormatter::format(ss.port()) != "" && 
						hr.country != "" && hr.geo_city != "" && hr.geo_state != "" && hr.external_ip != ""){
							if (!connectionHandler->sendLine(rst)) {
								Poco::Util::Application::instance().logger().error("Disconnected. Could not connect to remote ruby server. Exiting...\n");
								Poco::Util::Application::instance().logger().error("Trying to connect. Now...\n");
								//std::cout << "Disconnected. Exiting...\n" << std::endl;
								//std::cout << "Trying to connect. Now...\n" << std::endl;
								try{
									if(!connectionHandler->connect())new Poco::Exception("Disconnected.");
								}catch(Exception e){
									Poco::Util::Application::instance().logger().error("Could not connect to remote ruby server. Exiting...\n");
									//std::cout << "Could not connect. Exiting...\n" << std::endl;
									//poco_bugcheck_msg("Could not connect to remote ruby server");
								}
							}
							q.pop();
							std::string _rt;
							sleep(5);
							if (!connectionHandler->getLine(_rt) ) {
								Poco::Util::Application::instance().logger().error("Could not connect to remote ruby server. Exiting...\n");
								//std::cout << "Could not connect. Exiting...\n" << std::endl;
								//poco_bugcheck_msg("Could not connect to remote ruby server");
							}
							if(_rt == "ok")cout<<"Sending OK!"<<endl;
						}else{
							q.pop();
						}
					}
					}catch (const std::exception& exc)	{
						if(q.size() > 0 )q.pop();
					}catch(...){
						if(q.size() > 0 )q.pop();
						//cout<<e.displayText()<<endl;
						//Poco::Util::Application::instance().logger().fatal(e.displayText());
					}
				}
			}
	}
