#include "WorkerThreadPool.h"
extern int minCapacity, maxCapacity;

WorkerThreadPool::WorkerThreadPool(void)
{
	 //tp = new ThreadPool(334,334);
	tp = new ThreadPool(minCapacity,maxCapacity);
}


WorkerThreadPool::~WorkerThreadPool(void)
{
	tp->joinAll();
    delete tp;
}

WorkerThreadPool& WorkerThreadPool::getInstance()
{
   static WorkerThreadPool instance;
   return instance;
}