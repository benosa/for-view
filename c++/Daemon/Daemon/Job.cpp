#include "Job.h"
#include <typeinfo>
extern std::string part_url;
extern int __DEBUG;

inline string GetValue(Object::Ptr aoJsonObject, const char *aszKey) {
    Poco::Dynamic::Var loVariable;
    string lsReturn;
    string lsKey(aszKey);
    loVariable = aoJsonObject->get(lsKey);
    lsReturn = loVariable.convert<std::string>();
    return lsReturn;
}

Job::Job(Poco::SharedPtr<HostRecord> * hr,Poco::SharedPtr<source_proxy>* scr)
{
	//session = NULL;
	if(hr == NULL){
		Application::instance().logger().error("Job: HOST RECORD IS NULL!");
		this->~Job();
	}
	_scr = scr;
	_hr = hr;
}


Job::~Job(void)
{
	//if(session != NULL)	delete session;
	//delete this;
}

void Job::run()
{
	Poco::Util::Application &app = Application::instance();
	
		if((_hr->get())->host_port == "" || (_hr->get())->host_port.empty()){
			int i = 0;
			i++;
			throw;
		}
	try
		{
			if((_hr->get())->host_port == "46.165.208.182:30184"){
				int i = 0;
				i++;
			}
		Poco::Net::SocketAddress sa((_hr->get())->host_port);
		std::string _host = sa.host().toString();
		Poco::UInt16 _port = sa.port();
		URI uri(part_url);
		std::string path(uri.getPathAndQuery());
		if (path.empty()) path = "/";
		HTTPClientSession* session = new HTTPClientSession(uri.getHost(), uri.getPort());
		HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
		session->setProxy(_host, _port);
		//session->setKeepAliveTimeout(Poco::Timespan(1000000));
		HTTPResponse response;
		if (!doRequestJob(*session, request, response,(_host+NumberFormatter::format(_port))))
		{
			/*
			* ����� ��������� ������ �������
			*/
		}
		delete session;
		/*Poco::FastMutex fm;
		Poco::ScopedLockWithUnlock<Poco::FastMutex> scfm(fm);*/
			q.push(*_hr->get());
		/*scfm.unlock();*/
	}
	/*catch (const std::exception& exc)
	{
		(_hr->get())->status = "Off";
		app.logger().error(exc.what());
		return;
	}
	*/
	catch (Exception exc)
	{
		(_hr->get())->status = "Off";
		app.logger().error(exc.what());
		return;
	}
}

bool Job::doRequestJob(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response,std::string host_port)
{
	Parser loParser;
    Poco::Dynamic::Var loParsedJson;
    Poco::Dynamic::Var loParsedJsonResult;
    Object::Ptr loJsonObject;
	session.reset();
	session.sendRequest(request);
	std::istream& rs = session.receiveResponse(response);
	//std::string str;
	//cout << "Proxy Id is "<< Poco::NumberFormatter::format((_hr->get())->server_id) << endl;
	try{
		if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
			try{
				std::string contents;
				StreamCopier::copyToString(rs,contents);
				if(contents  == "" || contents.empty()){
					int i =0;
					i++;
				}
				loParsedJson = loParser.parse(contents);
				loParsedJsonResult = loParser.result();
				loJsonObject = loParsedJsonResult.extract<Object::Ptr>();
			}
			catch(Poco::JSON::JSONException& e){
				std::string contents;
				StreamCopier::copyToString(rs,contents);
				//rs.close();
				Application::instance().logger().error(e.displayText(),contents);
			}
				
			std::string ext_ip = GetValue(loJsonObject, "ext_ip");
			std::string country = GetValue(loJsonObject, "country");
			std::string state = GetValue(loJsonObject, "state");
			std::string city = GetValue(loJsonObject, "city");
			std::string latitude = GetValue(loJsonObject, "latitude");
			std::string longitude = GetValue(loJsonObject, "longitude");
			Poco::Net::SocketAddress sa((_hr->get())->host_port);
			std::string host = sa.host().toString();
		
			(_hr->get())->external_ip = ext_ip;
			(_hr->get())->country = country;
			(_hr->get())->geo_state = state;
			(_hr->get())->geo_city = city;
			(_hr->get())->dynamic = _scr->get()->rotation,
			(_hr->get())->status = "On";
			(_hr->get())->lat = Poco::NumberParser::parseFloat(latitude);
			(_hr->get())->lon = Poco::NumberParser::parseFloat(longitude);
			(_hr->get())->expires_at = (_scr->get())->life_time - ((_scr->get())->t->elapsed()/1000000);
			(_hr->get())->called = true;
			//cout << "Proxy Host:Port is "<< host_port<< "; Proxy real_ip is "<<(_hr->get())->external_ip<<"; Proxy expired_at: "<<Poco::NumberFormatter::format((_hr->get())->expires_at) << endl;
			if(__DEBUG == 1){
				if(__SHOW_REQUESTS_PROXY == 1)
				Application::instance().logger().information("Proxy Host:Port is "+ host_port +
					"; Proxy real_ip is " + (_hr->get())->external_ip + "; Proxy expired_at: " + Poco::NumberFormatter::format((_hr->get())->expires_at) );
			}
		}else{
			(_hr->get())->status = "Off";
			Poco::Net::HTTPResponse::HTTPStatus status = response.getStatus();
			Application::instance().logger().debug("Job rquest status code not OK -" + Poco::NumberFormatter::format(status));
		}
	}catch(Exception& e){
		(_hr->get())->status = "Off";
		Application::instance().logger().error(e.displayText());
		return false;
	}
		return true;
}