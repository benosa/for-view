#pragma once
#include "source_proxy.h"
#include "HostRecord.h"

#include "Poco/SharedPtr.h"
#include "Poco/Data/Session.h"
#include "Poco/Timer.h"
#include "Poco/NullStream.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include <Poco/Net/HTTPCredentials.h>
#include "Poco/Net/SocketAddress.h"
#include "Poco/RegularExpression.h"
#include "Poco/StreamCopier.h"
#include "Poco/Path.h"
#include "Poco/URI.h"

using namespace std;
using Poco::format;
using Poco::NotFoundException;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::Path;
using Poco::URI;
using Poco::Exception;
using Poco::RegularExpression;
using Poco::NumberFormatter;
using Poco::StreamCopier;

extern std::map<int,std::list<Poco::SharedPtr<HostRecord>>*> m;

class TimerWriteToPostgress{
public:
	Poco::SharedPtr<Poco::Data::Session>  _pSession;
	std::string SQLStr,SQLStr_begin,SQLStr_end;
	int serv_id;
	Poco::SharedPtr<source_proxy>* new_source_proxy;
	TimerWriteToPostgress();
	~TimerWriteToPostgress();
	void onTimer(Poco::Timer& timer);
	void TimerUpdateProxyList();
	std::vector<std::string> doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response);
	

};