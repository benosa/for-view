#include "TimerWriteToPostgress.h"
#include "Poco/Util/ServerApplication.h"

TimerWriteToPostgress::TimerWriteToPostgress(void)
{
	serv_id=0;
}


TimerWriteToPostgress::~TimerWriteToPostgress(void)
{
}
void TimerWriteToPostgress::onTimer(Poco::Timer& timer){
		//run_send = true;
		new_source_proxy->get()->t->update();
		//new_source_proxy.t = t1;
		//cout<<"Updating server name is "<<(new_source_proxy->get())->name<<endl;
		Poco::Util::Application::instance().logger().information("Updating server name is " + (new_source_proxy->get())->name);
		if(serv_id != 0){
			TimerUpdateProxyList();
			if(new_source_proxy->get()->rotation == 1){
				(new_source_proxy->get())->life_time = 600-5;
			}else{
				(new_source_proxy->get())->life_time = 900;
			}
		}else{
			//����� �� ��������� � ������������� - ������, ��� ���-�� ����� �� ���;
		}

	}
void TimerWriteToPostgress::TimerUpdateProxyList(){
		std::string str = (new_source_proxy->get())->list_url;
		URI uri(str);
		std::string path(uri.getPathAndQuery());
		if (path.empty()) path = "/";
		HTTPClientSession session(uri.getHost(), uri.getPort());
		HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
		HTTPResponse response;
		std::vector<std::string> rList = doRequest(session, request, response);
		Poco::SharedPtr<std::list<Poco::SharedPtr<HostRecord>>>* Hosts  = new Poco::SharedPtr<std::list<Poco::SharedPtr<HostRecord>>>();
		HostRecord* rec;
		std::list<Poco::SharedPtr<HostRecord>>* lPtr;
		lPtr = new std::list<Poco::SharedPtr<HostRecord>>();
		for(std::vector<std::string>::iterator it = rList.begin(),it_end = rList.end();it != it_end;++it){
			Poco::Net::SocketAddress sa(*it);
			std::string host = sa.host().toString();
			Poco::UInt16 _port = sa.port();
			std::string port = NumberFormatter::format(sa.port());
			rec = new HostRecord();
			rec->host_port = host+":"+port;
			rec->server_id = serv_id;
			Poco::SharedPtr<HostRecord> pHr(rec);
			lPtr->push_back(pHr);
		}
		Poco::FastMutex mu;
			Poco::ScopedLockWithUnlock<Poco::FastMutex> ull(mu);
			std::map<int,std::list<Poco::SharedPtr<HostRecord>>*>::iterator iter = m.find(serv_id);
			std::list<Poco::SharedPtr<HostRecord>>* tPtr = m[serv_id];
			m[serv_id] = lPtr;
			//delete tPtr;	
			ull.unlock();
	}

std::vector<std::string> TimerWriteToPostgress::doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response)
	{
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		std::vector<std::string> _tmpLHr;
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			RegularExpression re2("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}\\b",Poco::RegularExpression::RE_EXTRA |
				Poco::RegularExpression::RE_MULTILINE);
			std::stringstream ss;
			Poco::StreamCopier::copyStream(rs, ss);
			Poco::RegularExpression::Match match;
			match.offset = 0;
			
			while(0 != re2.match(ss.str(), match.offset, match))
			{
				std::string foundStr(ss.str().substr(match.offset, match.length));
				_tmpLHr.push_back(foundStr);
				match.offset += match.length;
			}
			return _tmpLHr;
		}
		else
		{
			Poco::NullOutputStream null;
			StreamCopier::copyStream(rs, null);
			return _tmpLHr;
		}
	}