#pragma once
#include <cstdio>
#include <string>
#include <Poco/DateTime.h>
#include "Poco/StringTokenizer.h"
#include "Poco/String.h" // for cat
#include "Poco/NumberFormatter.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/Timestamp.h"

using Poco::StringTokenizer;
using Poco::cat;

using Poco::DateTime;
class HostRecord
{
public:
	HostRecord(void);
	~HostRecord(void);
	std::string toString();
public:
	int id;
	std::string host_port;
	char is_blocked;
	std::string external_ip;
	bool dynamic;
	std::string status;
	std::string country;
	std::string geo_state;
	std::string geo_city;
	double lat;
	double lon;
	time_t expires_at;
	int requests_count;
	int server_id;
	bool called;
};
