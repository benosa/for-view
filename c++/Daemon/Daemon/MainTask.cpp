#include "MainTask.h"


MainTask::MainTask(void): Task("MainTask")
	{
		Drivers _drivers;
		ODBC::Connector::registerConnector();
		Utility::drivers(_drivers);
		Application::instance().logger().information("Starting Main Task");
	}


MainTask::~MainTask(void)

{
	for(int i = 0; i < l_Timers.size(); i++){
		l_Timers[i].get()->stop();
		delete (Timer*)l_Timers[i];
	}
	Application::instance().logger().information("Ending Main Task");
}

inline std::list<Poco::SharedPtr<HostRecord>>* getPackFromDatabase(){
	//HostRecord* rec = new HostRecord;
	std::list<Poco::SharedPtr<HostRecord>>* host_list = new std::list<Poco::SharedPtr<HostRecord>>();
	std::string host;
	std::string port;
	Poco::SharedPtr<Poco::Data::Session>  _pSession;
	
	for(std::map<int,std::list<Poco::SharedPtr<HostRecord>>*>::iterator it = m.begin(),end = m.end();it != end; ++it){
		std::list<Poco::SharedPtr<HostRecord>>* temp_host_list = it->second;
		host_list->insert(host_list->end(), temp_host_list->begin(), temp_host_list->end());
	}
	if(__DEBUG == 1){
		Poco::FastMutex mu;
		mu.lock();
		Application::instance().logger().information("**************************************************");
		Application::instance().logger().information("**Function getPackFromDatabase() return list******");
		Application::instance().logger().information("**************************************************");
		if(__SHOW_ARRAYS_OF_PROXIES == 1){
			for(std::list<Poco::SharedPtr<HostRecord>>::iterator iter = host_list->begin(), end = host_list->end();iter != end;++iter){
				Application::instance().logger().information(iter->get()->toString());
			}
		}else{
			Application::instance().logger().information("**list proxy size is " + Poco::NumberFormatter::format(host_list->size()) + " **");
		}
		Application::instance().logger().information("**************************************************");
		Application::instance().logger().information("**End function getPackFromDatabase() return list**");
		Application::instance().logger().information("**************************************************");
		mu.unlock();
	}
	return host_list;
}

void MainTask::runTask(){
		Update();
		Poco::Timestamp t1(0);
		while(true){
			if(t1.elapsed() >= 60000000){
				t1.update();
				std::list<Poco::SharedPtr<HostRecord>>* Hosts;
				Hosts = getPackFromDatabase();
					std::list<Poco::SharedPtr<HostRecord>>::iterator cur_begin = Hosts->begin();
					std::list<Poco::SharedPtr<HostRecord>>::iterator cur_end = Hosts->end();
						Job* worker;
						int i = 0;
						std::vector<Job*> lstd;
						for(std::list<Poco::SharedPtr<HostRecord>>::iterator iterator = cur_begin,end = cur_end;iterator != cur_end; ++iterator){ //�������� 334 ������ ���
							worker = new Job(&(*iterator),map_ptr_for_speed_access_source[(*iterator)->server_id]);
							lstd.push_back(worker);
							try{
								WorkerThreadPool::getInstance().tp->start(*worker);
								int s = WorkerThreadPool::getInstance().tp->available();
								if(__DEBUG == 1){
									Application::instance().logger().get("Info").information("I am create "+ Poco::NumberFormatter::format(i) + " worker!!!");
									Application::instance().logger().get("Info").information("Avaible "+ Poco::NumberFormatter::format(WorkerThreadPool::getInstance().tp->available()) + " workers");
								}
							}catch(Exception& e){
								Application::instance().logger().fatal(e.displayText() + ". Could not create thread");
							}
							cur_begin = iterator;
							i++;
							break;
						}
						WorkerThreadPool::getInstance().tp->joinAll();
						for(int i = 0; i < lstd.size();i++){
							delete lstd[i];
						}
						delete Hosts;
						WorkerThreadPool::getInstance().tp->collect();
						i=0;
						sleep(1000);
			}
		}
	}

void MainTask::Update(){
		Poco::SharedPtr<Poco::Data::Session>  _pSession;
		try
		{
			_pSession = new Session(pool->get());
			Poco::Data::Session* sss = new Session(pool.get()->get());
			_pSession->setFeature("autoCommit", true);
		}catch (ConnectionException& ex)
		{
			Application::instance().logger().error("!!! WARNING: Connection yo PG failed. !!!" );
			Application::instance().logger().error(ex.displayText());
			//rethrow_exception(ex);
		}
		if (_pSession && _pSession->isConnected()) {
			//Application::instance().logger().information("*** Connected to ("+ _connectString + ")");
			Statement select(*_pSession);
			source_proxy* spy;
			source_proxy scp;
			select << "SELECT id,name,main_url,login,password,list_url,live_time_url,rotation FROM proxy_sources WHERE rotation IS TRUE AND name = 'Server2'  LIMIT 1;",
				into(scp.server_id),into(scp.name),into(scp.main_url),into(scp.login),into(scp.password),
				into(scp.list_url),into(scp.live_time_url),into(scp.rotation),
			range(0, 1);
			while (!select.done())
			{
				select.execute();
				spy = new source_proxy();
				*spy = scp;
				new_source_proxy = new Poco::SharedPtr<source_proxy>(spy);
				proxy_source.push_back(new_source_proxy);
				map_ptr_for_speed_access_source[new_source_proxy->get()->server_id] = new_source_proxy;
			}
			std::vector<std::string> proxy_list;
			for(std::vector<Poco::SharedPtr<source_proxy>*>::iterator it_begin = proxy_source.begin(),it_end = proxy_source.end();it_begin != it_end;++it_begin){
				try{
					std::string str = ((*it_begin)->get())->list_url;
					URI uri(str);
					std::string path(uri.getPathAndQuery());
					if (path.empty()) path = "/";
					HTTPClientSession* session = new HTTPClientSession(uri.getHost(), uri.getPort());
					HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
					HTTPResponse response;
					proxy_list = doRequest(*session, request, response,((*it_begin)->get())->rotation);
					if(__DEBUG = 1){
						Poco::FastMutex mu;
						mu.lock();
							Application::instance().logger().information(((*it_begin)->get())->name + "have" + Poco::NumberFormatter::format(proxy_list.size())+ "proxies!!! (Class: MainTask(Func Update()))");
						mu.unlock();
						}
					if(proxy_list.size() == 0){
						if(__DEBUG = 1){
							Poco::FastMutex mu;
							mu.lock();
								Application::instance().logger().error(((*it_begin)->get())->name + "not have proxies!!! (Class: MainTask(Func Update()))");
							mu.unlock();
						}
						//delete proxy_list;
						continue;
					}
					delete session;
					std::list<Poco::SharedPtr<HostRecord>>* Hosts  = new std::list<Poco::SharedPtr<HostRecord>>();
					HostRecord* rec;
					if(((*it_begin)->get())->rotation){
						str = ((*it_begin)->get())->live_time_url;
						URI uri_for_time(str);
						std::string path_for_time(uri_for_time.getPathAndQuery());
						if (path_for_time.empty()) path_for_time = "/";
						HTTPClientSession session_for_time(uri_for_time.getHost(), uri_for_time.getPort());
						HTTPRequest request_for_time(HTTPRequest::HTTP_GET, path_for_time, HTTPMessage::HTTP_1_1);
						HTTPResponse response_for_time;
						int _ss = doRequestTime(session_for_time, request_for_time, response_for_time);
						((*it_begin)->get())->life_time =  _ss;
						if(_ss <=  0){
							_ss = 900;
							((*it_begin)->get())->life_time = 900;
						}
							Timer* t = new Timer(_ss*1000+5,600*1000);
							Poco::SharedPtr<Timer> pTm(t);
							l_Timers.push_back(pTm);
							TimerWriteToPostgress* te = new TimerWriteToPostgress();
							te->serv_id = ((*it_begin)->get())->server_id;
							te->new_source_proxy = *it_begin;
							(te->new_source_proxy->get())->t = new Poco::Timestamp();
							(te->new_source_proxy->get())->t->update();
							t->start(TimerCallback<TimerWriteToPostgress>(*te,&TimerWriteToPostgress::onTimer));	
							Poco::Thread::sleep(1000);
						//}
					}else{
						((*it_begin)->get())->life_time = 900;
						Timer* t = new Timer(900*1000,900*1000);
						Poco::SharedPtr<Timer> pTm(t);
						l_Timers.push_back(pTm);
						TimerWriteToPostgress* te = new TimerWriteToPostgress();
						te->serv_id = ((*it_begin)->get())->server_id;
						te->new_source_proxy = *it_begin;
						(te->new_source_proxy->get())->t = new Poco::Timestamp();
						(te->new_source_proxy->get())->t->update();
						t->start(TimerCallback<TimerWriteToPostgress>(*te,&TimerWriteToPostgress::onTimer));	
						Poco::Thread::sleep(1000);
					}
					std::list<Poco::SharedPtr<HostRecord>>* list_temp  = new std::list<Poco::SharedPtr<HostRecord>>();
					//Poco::SharedPtr<std::list<Poco::SharedPtr<HostRecord>>>*  lPtr = new Poco::SharedPtr<std::list<Poco::SharedPtr<HostRecord>>>(list_temp);
					for(std::vector<std::string>::iterator it = proxy_list.begin(),it_end = proxy_list.end();it != it_end;++it){
						Poco::Net::SocketAddress sa(*it);
						std::string host = sa.host().toString();
						Poco::UInt16 _port = sa.port();
						std::string port = NumberFormatter::format(sa.port());
						rec = new HostRecord();
						rec->host_port = host+":"+port;
						rec->server_id = ((*it_begin)->get())->server_id;
						Poco::SharedPtr<HostRecord>* pHr = new Poco::SharedPtr<HostRecord>(rec);
						list_temp->push_back(*pHr);
					}
					m[rec->server_id] = list_temp;
				}catch(Exception& e){
					Application::instance().logger().error(e.displayText());
				}
			}
		}
	}

	int MainTask::doRequestTime(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response){
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			RegularExpression re2("\\b\\d{1,3}\\b",Poco::RegularExpression::RE_EXTRA | Poco::RegularExpression::RE_MULTILINE);
			std::stringstream ss;
			Poco::StreamCopier::copyStream(rs, ss);
			Poco::RegularExpression::Match match;
			match.offset = 0;
			std::string tmp = "";
			while(0 != re2.match(ss.str(), match.offset, match))
			{
				std::string foundStr(ss.str().substr(match.offset, match.length));
				tmp = foundStr;
				match.offset += match.length;
			}
			return Poco::NumberParser::parse(tmp);
		}
		return 0;
	}


	std::vector<std::string> MainTask::doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response, bool rotation)
	{
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		std::vector<std::string>* strl = new std::vector<std::string>();
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			if(rotation){
				std::copy(
					std::istream_iterator<std::string>(rs), 
					std::istream_iterator<std::string>(), 
					std::back_inserter(*strl));
			}else{
				RegularExpression re2("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}\\b",Poco::RegularExpression::RE_EXTRA |
					Poco::RegularExpression::RE_MULTILINE);
				std::stringstream ss;
				Poco::StreamCopier::copyStream(rs, ss);
				string ssss;
				ssss = ss.str();
				if(ssss == "User not found"){
					return *strl;
				}
				Poco::RegularExpression::Match match;
				match.offset = 0;
				while(0 != re2.match(ss.str(), match.offset, match))
				{
					std::string foundStr(ss.str().substr(match.offset, match.length));
					strl->push_back(foundStr);
					match.offset += match.length;
				}
			}
			if(__DEBUG == 1){
				Poco::FastMutex mu;
				mu.lock();
				Application::instance().logger().information("**************************************************");
				Application::instance().logger().information("**Function doRequest in MainTask return list******");
				Application::instance().logger().information("**************************************************");
				if(__SHOW_ARRAYS_OF_PROXIES == 1){
					for(int i = 0; i < strl->size();i++){
						Application::instance().logger().information((*strl)[i]);
					}
				}else{
					Application::instance().logger().information("** list size is " + Poco::NumberFormatter::format(strl->size()) + "**");
				}
				Application::instance().logger().information("**************************************************");
				Application::instance().logger().information("**********End  in Main TaskdoRequest return list**");
				Application::instance().logger().information("**************************************************");
				mu.unlock();
			}
			return *strl;
		}
		else
		{

			Poco::NullOutputStream null;
			StreamCopier::copyStream(rs, null);
			return *strl;
		}
	}