#pragma once
#include "source_proxy.h"
#include <map>
#include <list>
#include <queue>
#include "HostRecord.h"
#include "connectionHandler.h"
#include "Poco/SharedPtr.h"
#include "Poco/Timer.h"
#include "Poco/Task.h"

using namespace std;
using Poco::format;
using Poco::NotFoundException;
using Poco::Exception;
using Poco::NumberFormatter;
using Poco::Task;

extern std::string _host_t;
extern int _port_t;

extern std::map<int,std::list<Poco::SharedPtr<HostRecord>>*> m;
extern std::queue<HostRecord> q;
//extern bool runable;

class PostTask: public Task
{
public:
	~PostTask();
	Poco::Net::SocketAddress* sa;
	Poco::Net::StreamSocket* socket;
	ConnectionHandler* connectionHandler;
	PostTask();
	void runTask();
};