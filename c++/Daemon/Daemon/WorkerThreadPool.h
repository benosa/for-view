#pragma once
#include <Poco/ThreadPool.h>
using Poco::ThreadPool;

class WorkerThreadPool
 {
  private:
        WorkerThreadPool();
        ~WorkerThreadPool();
  public:
        ThreadPool * tp;
        static WorkerThreadPool& getInstance();
 };
