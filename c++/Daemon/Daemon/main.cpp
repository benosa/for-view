/*
*	Personal include
*/
#include "MainTask.h"
#include "PostTask.h"
#include "Poco/FileChannel.h"
#include <queue>
#include "Poco/Util/PropertyFileConfiguration.h"
#include "Poco/FormattingChannel.h"
#include "Poco/PatternFormatter.h"


using Poco::AutoPtr;
using Poco::Util::PropertyFileConfiguration;
AutoPtr<PropertyFileConfiguration> pConf;


std::map<int,std::list<Poco::SharedPtr<HostRecord>>*> m;
std::queue<HostRecord> q;
Poco::AutoPtr<SessionPool> pool;
std::string _host_t;
std::string part_url;
int _port_t;
int __DEBUG;
int __SHOW_ARRAYS_OF_PROXIES;
int __SHOW_REQUESTS_PROXY;
int minCapacity, maxCapacity;
//bool runable = false;
//static bool run_send = false;


/*
*	Utils  include
*/
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Task.h"
#include "Poco/TaskManager.h"
#include "Poco/DateTimeFormatter.h"

/*
*	Timer  include
*/
//#include "Poco/Timer.h"
//#include "Poco/Thread.h"
/*
*	STD  include
*/


using Poco::Util::Application;
using Poco::Util::ServerApplication;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::OptionCallback;
using Poco::Util::HelpFormatter;
using Poco::Task;
using Poco::TaskManager;
using Poco::DateTimeFormatter;

using Poco::Util::Application;

using Poco::Exception;
using Poco::RegularExpression;
using Poco::NumberFormatter;

using Poco::Timer;
using Poco::TimerCallback;








class SampleServer: public ServerApplication
{
public:
	SampleServer(): _helpRequested(false)
	{
	}
	
	~SampleServer()
	{
	}

protected:
	void initialize(Application& self)
	{
		loadConfiguration("../etc/DaemonProxy.properties");
		//loadConfiguration();
		ServerApplication::initialize(self);
		/*AutoPtr<Poco::PatternFormatter> formater(new Poco::PatternFormatter);
		formater->setProperty("pattern", "%Y-%m-%d %H:%M:%S %s: %t");
		AutoPtr<Poco::FormattingChannel> infofile(new Poco::FormattingChannel(formater, AutoPtr<Poco::FileChannel>(new Poco::FileChannel("../log/DaemonProxyInfo.log"))));
		AutoPtr<Poco::FormattingChannel> errorfile(new Poco::FormattingChannel(formater, AutoPtr<Poco::FileChannel>(new Poco::FileChannel("../log/DaemonProxyError.log"))));
		logger().create("Info",infofile);
		logger().create("Error",errorfile);
		logger().setChannel(errorfile);*/
		logger().information("DaemonProxy starting up");
	}
		
	void uninitialize()
	{
		logger().information("DaemonProxy stopped");
		ServerApplication::uninitialize();
	}

	void defineOptions(OptionSet& options)
	{
		ServerApplication::defineOptions(options);
		
		options.addOption(
			Option("help", "h", "display help information on command line arguments")
				.required(false)
				.repeatable(false)
				.callback(OptionCallback<SampleServer>(this, &SampleServer::handleHelp)));
	}

	void handleHelp(const std::string& name, const std::string& value)
	{
		_helpRequested = true;
		displayHelp();
		stopOptionsProcessing();
	}

	void displayHelp()
	{
		HelpFormatter helpFormatter(options());
		helpFormatter.setCommand(commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("A sample server application that demonstrates some of the features of the Util::ServerApplication class.");
		helpFormatter.format(std::cout);
	}

	int main(const std::vector<std::string>& args)
	{
		if (!_helpRequested)
		{
			//Poco::Util::LayeredConfiguration& l = 	Application::instance().config();
			std::string _connectString = Application::instance().config().getString("_connectString"); 
			_host_t = Application::instance().config().getString("HOST_DAEMON_POLIPO");
			_port_t =  Application::instance().config().getInt("PORT_DAEMON_POLIPO");
			part_url = Application::instance().config().getString("IP_URL");
			__DEBUG = Application::instance().config().getInt("DEBUG");
			__SHOW_ARRAYS_OF_PROXIES = Application::instance().config().getInt("SHOW_ARRAYS_OF_PROXIES");
			__SHOW_REQUESTS_PROXY = Application::instance().config().getInt("SHOW_REQUESTS_PROXY");
			minCapacity = Application::instance().config().getInt("minCapacityThreadPool");
			maxCapacity = Application::instance().config().getInt("maxCapacityThreadPool");
			if(minCapacity <= 0 ||  maxCapacity <=0){
				logger().error("ThreadPool Properties not found");
				throw;
			}
			pool = new SessionPool("ODBC",_connectString);
			TaskManager tm;
			tm.start(new MainTask);
			tm.start(new PostTask);
			waitForTerminationRequest();
			tm.cancelAll();
			tm.joinAll();
		}
		return Application::EXIT_OK;
	}
	
private:
	bool _helpRequested;
};


POCO_SERVER_MAIN(SampleServer)
