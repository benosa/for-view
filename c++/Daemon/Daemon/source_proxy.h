#pragma once
#include <string>
#include <Poco/Timestamp.h>
class source_proxy
{
public:
	source_proxy(void);
	~source_proxy(void);
public:
	int server_id;
	std::string name;
	std::string main_url;
	std::string login;
	std::string password;
	std::string list_url;
	std::string live_time_url;
	int life_time;
	Poco::Timestamp* t;
	bool rotation;
};

