#pragma once
/*
*	Personal include
*/
#include "TimerWriteToPostgress.h"
#include "WorkerThreadPool.h"
#include "Job.h"
#include "HostRecord.h"
#include "source_proxy.h"
#include "connectionHandler.h"
#include "PostTask.h"
/*
*	Data include
*/
#include "Poco/Data/LOB.h"
#include "Poco/Data/Session.h"
#include "Poco/Data/StatementImpl.h"

/*
*	ODBC Postgresql include
*/
#include "Poco/Data/ODBC/ODBC.h"
#include "Poco/Data/ODBC/Connector.h"
#include "Poco/Data/ODBC/Utility.h"
#include "Poco/Data/ODBC/Diagnostics.h"
#include "Poco/Data/ODBC/ODBCException.h"
#include "Poco/Data/SessionPool.h"

/*
*	HTTP  include
*/
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include <Poco/Net/HTTPCredentials.h>
#include "Poco/Net/IPAddress.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/Socket.h"
#include "Poco/Net/SocketReactor.h"
#include "Poco/NObserver.h"
#include "Poco/Net/SocketNotification.h"

#include "Poco/StreamCopier.h"

#include "Poco/Path.h"
#include "Poco/URI.h"

/*
*	Utils  include
*/
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Task.h"
#include "Poco/TaskManager.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/StreamCopier.h"
#include "Poco/NullStream.h"
#include "Poco/Exception.h"
#include "Poco/RegularExpression.h"
#include "Poco/NumberFormatter.h"
#include "Poco/String.h"
#include "Poco/Format.h"

/*
*	Timer  include
*/
#include "Poco/Timer.h"
#include "Poco/Thread.h"
/*
*	STD  include
*/
#include <iostream>
#include <list>
#include <iterator>

using Poco::Net::ReadableNotification;
using Poco::Net::ShutdownNotification;
using Poco::Util::Application;
using Poco::Util::ServerApplication;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::OptionCallback;
using Poco::Util::HelpFormatter;
using Poco::Task;
using Poco::TaskManager;
using Poco::DateTimeFormatter;
using namespace Poco::Data;
using namespace Poco::Data::Keywords;
using Poco::Data::ODBC::ConnectionException;
using Poco::Data::ODBC::StatementException;
using Poco::Data::ODBC::StatementDiagnostics;
using Poco::Data::CLOB;
using Poco::Data::ODBC::Utility;
using Poco::Data::ODBC::ODBCException;


using Poco::format;
using Poco::NotFoundException;
using Poco::Util::Application;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::StreamCopier;
using Poco::Path;
using Poco::URI;
using Poco::Exception;
using Poco::RegularExpression;
using Poco::NumberFormatter;

using Poco::Timer;
using Poco::TimerCallback;

extern Poco::AutoPtr<SessionPool> pool;
extern int __DEBUG;
extern int __SHOW_ARRAYS_OF_PROXIES;

class MainTask: public Task
{
public:
	std::vector<Poco::SharedPtr<Timer>> l_Timers;
	
public:
	typedef Poco::Data::ODBC::Utility::DriverMap Drivers;
	~MainTask();
	MainTask();
	void runTask();
	
private:
	std::vector<Poco::SharedPtr<source_proxy>*> proxy_source;
	std::map<int,Poco::SharedPtr<source_proxy>*> map_ptr_for_speed_access_source;
	//std::list<Poco::SharedPtr<source_proxy>*> temp_list_shared_ptr;
	Poco::SharedPtr<source_proxy>* new_source_proxy;
	std::vector<std::string> proxy_list;
	
	void Update();
	int doRequestTime(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response);
	std::vector<std::string> doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response, bool rotation);
};