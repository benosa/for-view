#include "Cell.h"
#include "Player.h"

CCell::~CCell(){
	int i = 0;
	i++;
}

void CCell::InjectNPC(CNpc* npc){
	for (int i = 0; i < Players.size(); i++){
		Players[i]->UpdateAfterAddNPCToCell(npc);
	}
	NPC.push_back(npc);
	NPCMap[npc->id] = npc;
}

void CCell::InjectPlayer(Player* player){
	Players.push_back(player);
	PlayersMap[player->id] = player;
}

void CCell::InjectLut(CLut* npc){
	Lut.push_back(npc);
}

void CCell::RemoveNPC(CNpc* npc){
	for (int i = 0; i < Players.size(); i++){
		Players[i]->UpdateAfterRemoveNPCFromCell(npc);
	}
	auto it = std::find(NPC.begin(), NPC.end(), npc);
	if (it != NPC.end())
		NPC.erase(it);
	PlayersMap.erase(npc->id);
}

void CCell::RemovePlayer(Player* player){
	//std::vector<Poco::SharedPtr<CNpc, Poco::ReferenceCounter, NullReleasePolicy<CNpc>>>::iterator iter;
	auto it = std::find(Players.begin(), Players.end(), player);
	if (it != Players.end())
		Players.erase(it);
	PlayersMap.erase(player->id);
}

void CCell::RemoveLut(CLut* lut){
	//std::vector<Poco::SharedPtr<CLut, Poco::ReferenceCounter, NullReleasePolicy<CLut>>>::iterator iter;
	auto it = std::find(Lut.begin(), Lut.end(), lut);
	if (it != Lut.end())
		Lut.erase(it);
}

std::vector<CNpc*>* CCell::getArrayNPC(){
	return &NPC;
}