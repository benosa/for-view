#include "World.h"

CWorld::CWorld(Poco::OSP::BundleContext::Ptr _pContext)
{
	
}

CWorld::CWorld()
{

}


CWorld::~CWorld()
{
	//delete cpwt;
	//Worlds = pService->LoadWorlds();
	for (const auto& item : *WorldsMap) {
		Poco::SharedPtr<Worlds> tw = item.second;
		for (int z = 0; z < tw->Continents.size(); z++){
			for (int y = 0; y < tw->Continents[z]->Locations.size(); y++){
				this->cpwt->deleteObserver(tw->Continents[z]->Locations[y]->Location);
			}
		}
	}
	//����� ����� ������� ��� ����������� ����
}

void CWorld::Start(){
	this->PulsarTimerStart();
	this->LoadWorlds();
}
void CWorld::LoadMap(){

}
void CWorld::LoadNPC(){

}
void CWorld::LoadItems(){
	
}
void CWorld::PulsarTimerStart(){
	cpwt = Poco::SharedPtr<CPulsarWorldTimer>(new CPulsarWorldTimer());
}
void CWorld::LoadWorlds(){
	WorldsMap = this->parse();
	for (const auto& item : *WorldsMap) {
		Poco::SharedPtr<Worlds> tw = item.second;
		for (int z = 0; z < tw.get()->Continents.size(); z++){
			for (int y = 0; y < tw.get()->Continents[z]->Locations.size(); y++){
				this->cpwt->addObserver(tw.get()->Continents[z]->Locations[y]->Location);
			}
		}
	}
}

void CWorld::reinitializate(){
	//cpwt->
}

Poco::SharedPtr<map<string, Poco::SharedPtr<Worlds>>> CWorld::parse(){
	Poco::SharedPtr<map<string, Poco::SharedPtr<Worlds>>> mapworlds = Poco::SharedPtr<map<string, Poco::SharedPtr<Worlds>>>(new map<string, Poco::SharedPtr<Worlds>>());
	Poco::SharedPtr<vector<Document::Ptr>> vworlds = pService->LoadWorlds();
	for (int i = 0; i < vworlds->size(); i++){
		Poco::SharedPtr<Worlds> pw = Poco::SharedPtr<Worlds>(new Worlds());
		/*
		============================ ����� ��������� ���� - ��������� �� ========================================
		*/
		//auto ss = (*vworlds)[i]->get<__int32>("_id");
		//auto ii = ss->type();
		//Poco::MongoDB::ObjectId p = Poco::MongoDB::ObjectId("000000000000000000000000");
		//string vch = p.toString();
		//Poco::MongoDB::ObjectId::Ptr pts = (Poco::MongoDB::ObjectId::Ptr)ss;
		//auto sss = ss.get();
		pw->id = (*vworlds)[i]->get("_id")->toString();
		pw->name = (*vworlds)[i]->get("Name")->type() == 10 ? "" : (*vworlds)[i]->get<std::string>("Name");
		pw->info = (*vworlds)[i]->get("Info")->type() == 10 ? "" : (*vworlds)[i]->get<std::string>("Info");
		//pw->ID = (*vworlds)[i]->get<int>("ID");
		(*mapworlds)[pw->id] = pw;
		/*
		============================ ����� ��������� ���������� ��� ���� - ��������� �� ========================================
		*/
		Poco::MongoDB::Array::Ptr values = (*vworlds)[i]->get<Poco::MongoDB::Array::Ptr>("Continents");
		for (int i1 = 0; i1 < values->size(); i1++){
			Document::Ptr gl = values->get<Document::Ptr>(i1);
			std::string $id = gl[0].get("$id")->toString();
			std::string $dbref = gl[0].get("$ref")->toString();
			$dbref = $dbref.substr(1, $dbref.size() - 2);
			Poco::SharedPtr<vector<Document::Ptr>> vcontinents = pService->LoadContinents($dbref, $id);
			for (int i2 = 0; i2 < vcontinents->size(); i2++){
				Poco::SharedPtr<GameContinents> pl = Poco::SharedPtr<GameContinents>(new GameContinents());
				pl->id = (*vcontinents)[i2]->get("_id")->toString();
				pl->name = (*vcontinents)[i2]->get("Name")->type() == 10 ? "" : (*vcontinents)[i2]->get<std::string>("Name");
				pl->info = (*vcontinents)[i2]->get("Info")->type() == 10 ? "" : (*vcontinents)[i2]->get<std::string>("Info");
				pl->ID = (*vcontinents)[i2]->get<int>("ID");
				/*
				============================ ����� ��������� ������� ��� ���������� - ��������� �� ========================================
				*/
				Poco::MongoDB::Array::Ptr values_loc = (*vcontinents)[i2]->get<Poco::MongoDB::Array::Ptr>("Locations");
				for (int i3 = 0; i3 < values_loc->size(); i3++){
					Document::Ptr lgl = values_loc->get<Document::Ptr>(i3);// ->get(i);
					std::string $lid = lgl[0].get("$id")->toString();
					std::string $ldbref = lgl[0].get("$ref")->toString();
					$ldbref = $ldbref.substr(1, $ldbref.size() - 2);
					Poco::SharedPtr<vector<Document::Ptr>> vlocations = pService->LoadLocations($ldbref, $lid);
					for (int i4 = 0; i4 < vlocations->size(); i4++){
						Poco::SharedPtr<GameLocations> lpl = Poco::SharedPtr<GameLocations>(new GameLocations());
						lpl->id = (*vlocations)[i4]->get("_id")->toString();
						lpl->name = (*vlocations)[i4]->get("Name")->type() == 10 ? "" : (*vlocations)[i4]->get<std::string>("Name");
						lpl->info = (*vlocations)[i4]->get("Info")->type() == 10 ? "" : (*vlocations)[i4]->get<std::string>("Info");
						lpl->ID = (*vlocations)[i4]->get<int>("ID");
						Document::Ptr loc = (*vlocations)[i4]->get<Document::Ptr>("Location");
						lpl->Location = Poco::SharedPtr<CLocation>(new CLocation());
						lpl->Location->pContext = pContext;
						lpl->Location->id = loc->get("_id")->toString();
						//������� ����� �� ������� � ���� �� - ��� ���� ����� �� �������� ���� ��� ������ ����������� � ����� ���
						lpl->Location->ILocation = lpl->Location;
						Poco::MongoDB::Array::Ptr values_cell = loc->get<Poco::MongoDB::Array::Ptr>("CellArray");
						int NPCCountID = 0;
						for (int i5 = 0; i5 < values_cell->size(); i5++){
							Document::Ptr cgl = values_cell->get<Document::Ptr>(i5);// ->get(i);
							std::string $cid = cgl[0].get("$id")->toString();
							std::string $cdbref = cgl[0].get("$ref")->toString();
							$cdbref = $cdbref.substr(1, $cdbref.size() - 2);
							Poco::SharedPtr<vector<Document::Ptr>> vcells = pService->LoadCells($cdbref, $cid);
							for (int i6 = 0; i6 < vcells->size(); i6++){
								CCell* cell_p = (new CCell());
								cell_p->id = (*vcells)[i6]->get("_id")->toString();
								cell_p->info = /*((*vcells)[i6]->get("info")->type() == 10) ? new Poco::MongoDB::Array :*/ (*vcells)[i6]->get<Poco::MongoDB::Array::Ptr>("info");
								cell_p->type = (*vcells)[i6]->get<int>("type");
								cell_p->napravlenie = (*vcells)[i6]->get<int>("napravlenie");
								cell_p->ID = (*vcells)[i6]->get<int>("ID");
								cell_p->ICurrentWorld = pw;
								Document::Ptr tmpp = (*vcells)[i6]->get("North")->type() == 10 ? NULL : (*vcells)[i6]->get<Document::Ptr>("North");
								if (!tmpp.isNull())cell_p->North = tmpp->get("$id")->type() == 10 ? "000000000000000000000000" : tmpp->get("$id")->toString();
								tmpp = (*vcells)[i6]->get("South")->type() == 10 ? NULL : (*vcells)[i6]->get<Document::Ptr>("South");
								if (!tmpp.isNull())cell_p->South = tmpp->get("$id")->type() == 10 ? "000000000000000000000000" : tmpp->get("$id")->toString();
								tmpp = (*vcells)[i6]->get("East")->type() == 10 ? NULL : (*vcells)[i6]->get<Document::Ptr>("East");
								if (!tmpp.isNull())cell_p->East = tmpp->get("$id")->type() == 10 ? "000000000000000000000000" : tmpp->get("$id")->toString();
								tmpp = (*vcells)[i6]->get("West")->type() == 10 ? NULL : (*vcells)[i6]->get<Document::Ptr>("West");
								if (!tmpp.isNull())cell_p->West = tmpp->get("$id")->type() == 10 ? "000000000000000000000000" : tmpp->get("$id")->toString();
								tmpp = (*vcells)[i6]->get("Up")->type() == 10 ? NULL : (*vcells)[i6]->get<Document::Ptr>("Up");
								if (!tmpp.isNull())cell_p->Up = tmpp->get("$id")->type() == 10 ? "000000000000000000000000" : tmpp->get("$id")->toString();
								tmpp = (*vcells)[i6]->get("Down")->type() == 10 ? NULL : (*vcells)[i6]->get<Document::Ptr>("Down");
								if (!tmpp.isNull())cell_p->Down = tmpp->get("$id")->type() == 10 ? "000000000000000000000000" : tmpp->get("$id")->toString();
								/*
								============================ ����� ��������� NPC � ������� - ��������� �� ========================================
								��� ������� ����� ��� ������� ���������� � ������ �������������
								*/
								Poco::MongoDB::Array::Ptr values_NPC = (*vcells)[i6]->get<Poco::MongoDB::Array::Ptr>("NPC");
								for (int i_npc = 0; i_npc < values_NPC->size(); i_npc++){
									Document::Ptr NPC_doc = values_NPC->get<Document::Ptr>(i_npc);
									CNpc* npci = new CNpc(NPC_doc);
									//������� ����� �� ��� � ���� �� - ��� ���� ����� �� �������� ���� ��� ������ ����������� � ����� ���
									{
										npci->setINPC(npci);
										npci->CurrentCell = cell_p;
										npci->CurCell = cell_p->id;
									}
									//������� ��� � ������ ��� ������
									cell_p->InjectNPC(npci);
									npci->IID = NPCCountID;
									NPCCountID++;
									//������ ��� � ������� ��� �������
									lpl->Location->NPCArray.push_back(npci);
								}
								pw->Cells[cell_p->id] = cell_p;
								//pw->vecCCellsArray.push_back(cell_p);

								cell_p->ICell = cell_p;
								//������� map �����
								lpl->Location->CCellsArray[cell_p->id] = cell_p;
								//������ ����� ��� ���������� ������ �����
								lpl->Location->vecCCellsArray.push_back(cell_p);
							}
						}
						/*
						============================ ����� ��������� NPC � �������� - ��������� �� ========================================
						����� ��� ������� �������� ��� �������
						�� ������ ��������� �����������
						*/
						if (lpl->Location->vecCCellsArray.size() > 0 || lpl->Location->CCellsArray.size() > 0){
							Array::Ptr NPCarrp = (*vlocations)[i4]->get<Array::Ptr>("NPCIDList");
							for (int npcarri = 0; npcarri < NPCarrp->size(); npcarri++){
								Document::Ptr npcptr = NPCarrp->get<Document::Ptr>(npcarri);
								std::string $npcptrid = npcptr[0].get("$id")->toString();
								std::string $npcptrdbref = npcptr[0].get("$ref")->toString();
								$npcptrdbref = $npcptrdbref.substr(1, $npcptrdbref.size() - 2);
								Poco::SharedPtr<vector<Document::Ptr>> vnpcs = pService->LoadNpcs($npcptrdbref, $npcptrid);
								for (int i7 = 0; i7 < vnpcs->size(); i7++){
									Document::Ptr pdoc = (*vnpcs)[i7];
									//��������� ��� �������
									CNpc* npcnpcptr = new CNpc(pdoc);
									//�������� ��������� ��� ���������� ��� � �����������
									Poco::SharedPtr<NPCContainer> NPCCont = Poco::SharedPtr<NPCContainer>(new NPCContainer());
									//���������� ��� �� ������� ����� ����
									NPCCont->endCountNPC = npcnpcptr->getCountAllNPC();
									NPCCont->curCountNPC = npcnpcptr->getCurCountNPC();
									//������ ����������� ����� ������ ������� ���
									NPCCont->period = npcnpcptr->getPeriodRegeneration();
									//�������� � ������� ��� �� �������
									for (int i8 = 0; i8 < NPCCont->endCountNPC; i8++){
										CNpc* tNPC = new CNpc();
										*tNPC = *npcnpcptr;
										tNPC->setINPC(tNPC);
										//�������� ��� � ������ ������� ��������� � ����������
										NPCCont->NPCArray.push_back(tNPC);
										//����� �������� ��� �������������� ������� ��� �� �������
										NPCCont->curCountNPC++;
										//�������, ���� ���� ����� ��� �������
										tNPC->IID = NPCCountID;
										NPCCountID++;
										//��� �������� ������� �������� ��� � ������
										lpl->Location->NPCArray.push_back(tNPC);
										//��������� ������������ ��� � �������.
										{
											if (tNPC->CurCell == "000000000000000000000000"){
												CCell* NextCell = lpl->Location->vecCCellsArray[rand() % lpl->Location->vecCCellsArray.size()];
												tNPC->CurrentCell = NextCell;
												tNPC->CurCell = NextCell->id;
												NextCell->InjectNPC(tNPC);
											}
											else{
												CCell* NextCell = lpl->Location->CCellsArray[tNPC->CurCell];
												tNPC->CurrentCell = NextCell;
												NextCell->InjectNPC(tNPC);
											}
										}
									}
									//��������� ������� � ������ ����������� ��� �������
									lpl->Location->NPCArrayContainer.push_back(NPCCont);
									//����� ���������� ��� � ������ ��� ������� ��� ��� - ���������
								}

							}

						}
						/*
						================== �������� ��������� ������� � ��������� =============
						*/
						pl->Locations.push_back(lpl);
						/*
						================== �������� ��������� ������� � ��������� =============
						*/
					}

				}
				/*
				================== �������� ���������� ��������� � ��� =============
				*/
				pw->Continents.push_back(pl);
				/*
				================== �������� ���������� ��������� � ��� =============
				*/
				
			}
		}
	}
	return mapworlds;
}