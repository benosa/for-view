#pragma once
#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"

#include "Poco/NotificationCenter.h"
#include "Poco/Notification.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/AutoPtr.h"
#include "Poco/Exception.h"
#include "Poco/Net/NetException.h"
#include "Poco/SharedPtr.h"
#include "Poco/Format.h"
#include "Poco/NumberFormatter.h"
#include "Poco/DateTime.h"
#include <iostream>
#include <list>
#include <vector>
#include <queue>
#include "World.h"
#include "helper.h"
#include "Player.h"
#include "RequestCommand.h"
#include "ReceiveCommand.h"
#include "DBGameService.h"
#include "Poco/OSP/BundleContext.h"
#include "MessageCommand.h"
#include <functional>
#include "IPlayerController.h"



using Poco::format;
using Poco::NumberFormatter;
using Poco::DateTime;
using Poco::NotificationCenter;
using Poco::Notification;
using Poco::Observer;
using Poco::NObserver;
using Poco::AutoPtr;
using Poco::Net::ConnectionResetException;
using Poco::SharedPtr;
using Poco::OSP::BundleContext;
using namespace std::placeholders;



class CPlayerController : public Target
{

public:
	CWorld* World;
	DBGameService::Ptr pService;
	Player* p;

	Player* IPlayer;
	BundleContext::Ptr pContext;
	IPlayerController* IPlayerControllerHandler;
public:
	void run();
	void getUserIdFromClientAndFillPlayer();
	bool sendBytes(const char bytes[], int bytesToWrite);
public:
	CPlayerController(Poco::Net::StreamSocket& _socket, CWorld* _World, DBGameService::Ptr, BundleContext::Ptr);
	~CPlayerController();
	void handleBase(BaseNotification* pNf);
	void UpdateClientAfterAddNpcToCell(CNpc* ptr);
	void UpdateClientAfterRemoveNpcFromCell(CNpc* ptr);
private:
	bool observered;
	Poco::Net::StreamSocket& socket;
};