#include "Player.h"
#include "Corps.h"
#include "Equipment.h"
#include "Inventory.h"
extern DBGameService::Ptr pService;

std::string Player::getLangString(std::string Lang, Array::Ptr p){
	std::string rval = "";
	try{
		if (Lang == "rus") {
			try{
				Document::Ptr ptr = p->get<Document::Ptr>(1);
				rval = ptr->get<std::string>("rus");
			}
			catch (...){
				Document::Ptr ptr = p->get<Document::Ptr>(0);
				rval = ptr->get<std::string>("eng");
			}

		}
		if (Lang == "eng") {
			try{
				Document::Ptr ptr = p->get<Document::Ptr>(0);
				rval = ptr->get<std::string>("eng");
			}
			catch (...){
				Document::Ptr ptr = p->get<Document::Ptr>(1);
				rval = ptr->get<std::string>("rus");
			}
		}
		return rval;
	}
	catch (...){
		return "Lang not found";
	}
}

void Player::parse(Document::Ptr pdoc){
	//this->curCountNPC = 0;
	//curPeriod = 0;
	this->Lang = pdoc->get("lang")->type() == 10 ? "" : pdoc->get<std::string>("lang");
	this->id = pdoc->get("_id")->toString();
	this->CurCell = pdoc->get("CurrentCell")->type() == 10 ? "000000000000000000000000" : pdoc->get("CurrentCell")->toString();
	//Array::Ptr NameArr = pdoc->get<Array::Ptr>("Name");
	//NameArr->get<Document::Ptr>();
	this->Name = pdoc->get<Array::Ptr>("Name");
	this->username = pdoc->get("username")->type() == 10 ? "" : pdoc->get("username")->toString();
	this->password = pdoc->get("password")->type() == 10 ? "" : pdoc->get("password")->toString();
	this->email = pdoc->get("email")->type() == 10 ? "" : pdoc->get("email")->toString();
	this->logged = pdoc->get<bool>("logged");
	this->banned = pdoc->get<bool>("banned");
	this->isAdmin = pdoc->get<bool>("isAdmin");
	this->Age = pdoc->get<int>("Age");
	this->Description = pdoc->get<Array::Ptr>("Description");
	this->DateBorn = pdoc->get<Poco::Timestamp>("DateBorn");
	//this->DateDied = pdoc->get("DateDied").isNull() ? (new Poco::DateTime())->timestamp() : pdoc->get<Poco::Timestamp>("DateDied");
	this->DateDied = pdoc->get("DateDied")->type() == 10 ? (new Poco::DateTime())->timestamp() : pdoc->get<Poco::Timestamp>("DateDied");

	this->ImgId = pdoc->get("ImgId")->type() == 10 ? "" : pdoc->get("ImgId")->toString();
	this->Level = pdoc->get<int>("Level");
	this->strenght = pdoc->get<int>("strenght");
	this->body = pdoc->get<int>("body");
	this->intillect = pdoc->get<int>("intillect");
	this->dexterity = pdoc->get<int>("dexterity");
	this->phisical_Damage = pdoc->get<int>("phisical_Damage");
	this->magical_Damage = pdoc->get<int>("magical_Damage");
	this->chance_to_Hit = pdoc->get<int>("chance_to_Hit");
	this->chance_to_Cast = pdoc->get<int>("chance_to_Cast");
	this->suppression = pdoc->get<int>("suppression");
	this->absorption = pdoc->get<int>("absorption");
	this->HP = pdoc->get<int>("HP");
	this->MP = pdoc->get<int>("MP");
	this->Gold = pdoc->get<int>("Gold");
	this->Platina = pdoc->get<int>("Platina");
	this->Silver = pdoc->get<int>("Silver");
	this->Expirence = pdoc->get<int>("Expirence");
	this->relaxHP = pdoc->get<int>("relaxHP");
	this->relaxMP = pdoc->get<int>("relaxMP");
	this->reflection = pdoc->get<int>("reflection");
	this->armor = pdoc->get<int>("armor");
	this->protection = pdoc->get<int>("protection");
	this->lightingResistance = pdoc->get<int>("lightingResistance");
	this->fireResistance = pdoc->get<int>("fireResistance");
	this->coldResistance = pdoc->get<int>("coldResistance");
	this->maxHeight = pdoc->get<int>("maxHeight");
	this->Karma = pdoc->get<int>("Karma");
	this->iPlayer = pdoc->get<bool>("iPlayer");
	this->iAggressor = pdoc->get<bool>("iAggressor");
	this->killed = pdoc->get<bool>("killed");
	this->IDCellPropiska = pdoc->get("IDCellPropiska")->toString();
	this->WorldID = pdoc->get("CurWorld")->toString();
	//��� ����� ����� ��� ���� ����� !!!!!!!!!!!!!!!!!
	//this->endCountNPC = pdoc->get("endCountNPC").isNull() ? 1 : NPC_doc->get<int>("endCountNPC");
	//this->isMoved = pdoc->get("isMoved").isNull() ? false : NPC_doc->get<bool>("isMoved");
	//this->period = pdoc->get("period").isNull() ? 3 : NPC_doc->get<int>("period");
	this->LocationID = pdoc->get("LocationID")->type() == 10 ? "" : pdoc->get("LocationID")->toString();

	//npci->CorpsArray = NPC_doc->get("CorpsArray");
	try{
		if (!pdoc->get("CorpsArray").isNull()){
			Document::Ptr corpspdoc = pdoc->get<Document::Ptr>("CorpsArray");
			Document::Ptr Equipmentpdoc = corpspdoc->get<Document::Ptr>("Equipment");
			/*
			============================= ���������, ��������� �������������� =======================================
			*/
			Document::Ptr robepdoc = Equipmentpdoc->get("robe")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("robe");
			Document::Ptr headwearpdoc = Equipmentpdoc->get("headwear")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("headwear");
			Document::Ptr armorpdoc = Equipmentpdoc->get("armor")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("armor");
			Document::Ptr necklacepdoc = Equipmentpdoc->get("necklace")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("necklace");
			Document::Ptr weaponpdoc = Equipmentpdoc->get("weapon")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("weapon");
			Document::Ptr shieldpdoc = Equipmentpdoc->get("shield")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("shield");
			Document::Ptr ring1pdoc = Equipmentpdoc->get("ring1")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("ring1");
			Document::Ptr ring2pdoc = Equipmentpdoc->get("ring2")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("ring2");
			Document::Ptr glovepdoc = Equipmentpdoc->get("glove")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("glove");
			Document::Ptr inexpressiblespdoc = Equipmentpdoc->get("inexpressibles")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("inexpressibles");
			Document::Ptr beltpdoc = Equipmentpdoc->get("inexpressibles")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("belt");
			Document::Ptr bootspdoc = Equipmentpdoc->get("boots")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("boots");

			//this->Corps = new CCorps();
			//this->Corps->Equipment = Poco::SharedPtr<Equipment>(new Equipment());
			//this->Corps->Inventory = Poco::SharedPtr<Inventory>(new Inventory());

			this->Corps.Equipment.robe = robepdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(robepdoc));
			this->Corps.Equipment.headwear = headwearpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(headwearpdoc));
			this->Corps.Equipment.armor = armorpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(armorpdoc));
			this->Corps.Equipment.necklace = necklacepdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(necklacepdoc));
			this->Corps.Equipment.weapon = weaponpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(weaponpdoc));
			this->Corps.Equipment.shield = shieldpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(shieldpdoc));
			this->Corps.Equipment.ring1 = ring1pdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(ring1pdoc));
			this->Corps.Equipment.ring2 = ring2pdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(ring2pdoc));
			this->Corps.Equipment.glove = glovepdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(glovepdoc));
			this->Corps.Equipment.inexpressibles = inexpressiblespdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(inexpressiblespdoc));
			this->Corps.Equipment.belt = beltpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(beltpdoc));
			this->Corps.Equipment.boots = bootspdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(bootspdoc));
			/*
			============================= ���������, ��������� ��������� ������� =======================================
			*/
			Document::Ptr Inventorypdoc = corpspdoc->get("Inventory")->type() == 10 ? NULL : corpspdoc->get<Document::Ptr>("Inventory");
			if (!Inventorypdoc.isNull()){
				this->Corps.Inventory.id = Inventorypdoc->get("_id")->type() == 10 ? "" : Inventorypdoc->get("_id")->toString();
				//this->Corps.Inventory.backpack;
				this->Corps.Inventory.countLut = Inventorypdoc->get<int>("countLut");
				this->Corps.Inventory.MaxCountLut = Inventorypdoc->get<int>("MaxCountLut");
				this->Corps.Inventory.widthLut = Inventorypdoc->get<int>("widthLut");
				this->Corps.Inventory.MaxWidthLut = Inventorypdoc->get<int>("MaxWidthLut");
			}
			/*
			============================= ���������, ��������� ������ ������� =======================================
			*/
			Array::Ptr backpack = Inventorypdoc->get<Array::Ptr>("backpack");
			if (!backpack.isNull()){
				for (int i = 0; i < backpack->size(); i++){
					Document::Ptr backpackpdoc = backpack->get<Document::Ptr>(i);
					if (!backpackpdoc.isNull()){
						this->Corps.Inventory.backpack.push_back(Poco::SharedPtr<CLut>(new CLut(backpackpdoc)));
					}
				}
			}
		}
	}
	catch (Poco::Exception& exc){
		throw std::runtime_error(exc.message() + "Error loading IPLAYER id: " + this->id);
	}
	/*
	============================= ���������, ��������� ������ ����� =======================================
	*/
	try{
		if (!pdoc->get("MagicArray").isNull()){
			Array::Ptr MagicArray = pdoc->get<Array::Ptr>("MagicArray");
			for (int i = 0; i < MagicArray->size(); i++){
				Document::Ptr magickpdoc = MagicArray->get<Document::Ptr>(i);
				if (!magickpdoc.isNull()){
					this->MagicArray.push_back(Poco::SharedPtr<CLut>(new CLut(magickpdoc)));
				}
			}
		}
	}
	catch (Poco::Exception& exc){
		throw std::runtime_error(exc.message() + "Error loading IPLAYER id: " + this->id);
	}
	/*
	============================= ���������, ��������� ������ ������� =======================================
	*/
	try{
		if (!pdoc->get("SkillsArray").isNull()){
			Array::Ptr SkillsArray = pdoc->get<Array::Ptr>("SkillsArray");
			for (int i = 0; i < SkillsArray->size(); i++){
				Document::Ptr magickpdoc = SkillsArray->get<Document::Ptr>(i);
				if (!magickpdoc.isNull()){
					//pService->LoadSpells();
					Poco::SharedPtr<CSpell> pts = Poco::SharedPtr<CSpell>(new CSpell(magickpdoc));
					//skill_id
					//this->SkillArray[magickpdoc->get()]Poco::SharedPtr<CSpell>(new CSpell(magickpdoc))); pService
				}
			}
		}
	}
	catch (Poco::Exception& exc){
		throw std::runtime_error(exc.message() + "Error loading IPLAYER id: " + this->id);
	}
}

Poco::SharedPtr<Poco::AutoPtr<Poco::XML::Document>> Player::toXML(){
	typedef Poco::AutoPtr<Poco::XML::Document> DocumentPtr;
	DocumentPtr pds = new Poco::XML::Document;
	Poco::SharedPtr<DocumentPtr> pDoc = Poco::SharedPtr<DocumentPtr>(new Poco::AutoPtr<Poco::XML::Document>(new Poco::XML::Document()));
	Poco::AutoPtr<Poco::XML::Element> pRoot = pDoc->get()->createElement("root");
	pDoc->get()->appendChild(pRoot);
	AutoPtr<XML::Element> id = pDoc->get()->createElement("id");
	AutoPtr<XML::Text> idt = pDoc->get()->createTextNode(this->id);
	id->appendChild(idt);
	pRoot->appendChild(id);
	AutoPtr<XML::Element> pCurCell = pDoc->get()->createElement("CurCell");
	AutoPtr<XML::Text> pCurCellt = pDoc->get()->createTextNode(this->CurCell);
	pCurCell->appendChild(pCurCellt);
	pRoot->appendChild(pCurCell);
	AutoPtr<XML::Element> pName = pDoc->get()->createElement("Name");
	AutoPtr<XML::Text> pNamet = pDoc->get()->createTextNode(getLangString(this->Lang, this->Name));
	pName->appendChild(pNamet);
	pRoot->appendChild(pName);
	AutoPtr<XML::Element> pusername = pDoc->get()->createElement("username");
	AutoPtr<XML::Text> pusernamet = pDoc->get()->createTextNode(this->username);
	pusername->appendChild(pusernamet);
	pRoot->appendChild(pusername);
	AutoPtr<XML::Element> ppassword = pDoc->get()->createElement("password");
	AutoPtr<XML::Text> ppasswordt = pDoc->get()->createTextNode(this->password);
	ppassword->appendChild(ppasswordt);
	pRoot->appendChild(ppassword);
	AutoPtr<XML::Element> pemail = pDoc->get()->createElement("email");
	AutoPtr<XML::Text> pemailt = pDoc->get()->createTextNode(this->email);
	pemail->appendChild(pemailt);
	pRoot->appendChild(pemail);
	AutoPtr<XML::Element> plogged = pDoc->get()->createElement("logged");
	AutoPtr<XML::Text> ploggedt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->logged));
	plogged->appendChild(ploggedt);
	pRoot->appendChild(plogged);
	AutoPtr<XML::Element> pbanned = pDoc->get()->createElement("banned");
	AutoPtr<XML::Text> pbannedt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->banned));
	pbanned->appendChild(pbannedt);
	pRoot->appendChild(pbanned);
	AutoPtr<XML::Element> pAge = pDoc->get()->createElement("Age");
	AutoPtr<XML::Text> pAget = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Age));
	pAge->appendChild(pAget);
	pRoot->appendChild(pAge);
	AutoPtr<XML::Element> pDescription = pDoc->get()->createElement("Description");
	AutoPtr<XML::Text> pDescriptiont = pDoc->get()->createTextNode(getLangString(this->Lang, this->Description));
	pDescription->appendChild(pDescriptiont);
	pRoot->appendChild(pDescription);
	AutoPtr<XML::Element> pDateBorn = pDoc->get()->createElement("DateBorn");
	AutoPtr<XML::Text> pDateBornt = pDoc->get()->createTextNode(DateTimeFormatter::format(this->DateBorn, "%e %b %Y %H:%M"));
	pDateBorn->appendChild(pDateBornt);
	pRoot->appendChild(pDateBorn);
	AutoPtr<XML::Element> pDateDied = pDoc->get()->createElement("DateDied");
	AutoPtr<XML::Text> pDateDiedt = pDoc->get()->createTextNode(DateTimeFormatter::format(this->DateDied, "%e %b %Y %H:%M"));
	pDateDied->appendChild(pDateDiedt);
	pRoot->appendChild(pDateDied);
	AutoPtr<XML::Element> pImgId = pDoc->get()->createElement("ImgId");
	AutoPtr<XML::Text> pImgIdt = pDoc->get()->createTextNode(this->ImgId);
	pImgId->appendChild(pImgIdt);
	pRoot->appendChild(pImgId);
	AutoPtr<XML::Element> pLevel = pDoc->get()->createElement("Level");
	AutoPtr<XML::Text> pLevelt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Level));
	pLevel->appendChild(pLevelt);
	pRoot->appendChild(pLevel);
	AutoPtr<XML::Element> pstrenght = pDoc->get()->createElement("strenght");
	AutoPtr<XML::Text> pstrenghtt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->strenght));
	pstrenght->appendChild(pstrenghtt);
	pRoot->appendChild(pstrenght);
	AutoPtr<XML::Element> pbody = pDoc->get()->createElement("body");
	AutoPtr<XML::Text> pbodyt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->body));
	pbody->appendChild(pbodyt);
	pRoot->appendChild(pbody);
	AutoPtr<XML::Element> pintillect = pDoc->get()->createElement("intillect");
	AutoPtr<XML::Text> pintillectt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->intillect));
	pintillect->appendChild(pintillectt);
	pRoot->appendChild(pintillect);
	AutoPtr<XML::Element> pdexterity = pDoc->get()->createElement("dexterity");
	AutoPtr<XML::Text> pdexterityt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->dexterity));
	pdexterity->appendChild(pdexterityt);
	pRoot->appendChild(pdexterity);
	AutoPtr<XML::Element> pphisical_Damage = pDoc->get()->createElement("phisical_Damage");
	AutoPtr<XML::Text> pphisical_Damaget = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->phisical_Damage));
	pdexterity->appendChild(pphisical_Damaget);
	pRoot->appendChild(pphisical_Damage);
	AutoPtr<XML::Element> pmagical_Damage = pDoc->get()->createElement("magical_Damage");
	AutoPtr<XML::Text> pmagical_Damaget = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->magical_Damage));
	pmagical_Damage->appendChild(pmagical_Damaget);
	pRoot->appendChild(pmagical_Damage);
	AutoPtr<XML::Element> pchance_to_Hit = pDoc->get()->createElement("chance_to_Hit");
	AutoPtr<XML::Text> pchance_to_Hitt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->chance_to_Hit));
	pchance_to_Hit->appendChild(pchance_to_Hitt);
	pRoot->appendChild(pchance_to_Hit);
	AutoPtr<XML::Element> pchance_to_Cast = pDoc->get()->createElement("chance_to_Cast");
	AutoPtr<XML::Text> pchance_to_Castt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->chance_to_Cast));
	pchance_to_Cast->appendChild(pchance_to_Castt);
	pRoot->appendChild(pchance_to_Cast);
	AutoPtr<XML::Element> psuppression = pDoc->get()->createElement("suppression");
	AutoPtr<XML::Text> psuppressiont = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->suppression));
	psuppression->appendChild(psuppressiont);
	pRoot->appendChild(psuppression);
	AutoPtr<XML::Element> pabsorption = pDoc->get()->createElement("absorption");
	AutoPtr<XML::Text> pabsorptiont = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->absorption));
	pabsorption->appendChild(pabsorptiont);
	pRoot->appendChild(pabsorption);
	AutoPtr<XML::Element> pHP = pDoc->get()->createElement("HP");
	AutoPtr<XML::Text> pHPt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->HP));
	pHP->appendChild(pHPt);
	pRoot->appendChild(pHP);
	AutoPtr<XML::Element> pMP = pDoc->get()->createElement("MP");
	AutoPtr<XML::Text> pMPt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->MP));
	pMP->appendChild(pMPt);
	pRoot->appendChild(pMP);
	AutoPtr<XML::Element> pGold = pDoc->get()->createElement("Gold");
	AutoPtr<XML::Text> pGoldt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Gold));
	pGold->appendChild(pGoldt);
	pRoot->appendChild(pGold);
	AutoPtr<XML::Element> pPlatina = pDoc->get()->createElement("Platina");
	AutoPtr<XML::Text> pPlatinat = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Platina));
	pPlatina->appendChild(pPlatinat);
	pRoot->appendChild(pPlatina);
	AutoPtr<XML::Element> pSilver = pDoc->get()->createElement("Silver");
	AutoPtr<XML::Text> pSilvert = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Silver));
	pSilver->appendChild(pSilvert);
	pRoot->appendChild(pSilver);
	AutoPtr<XML::Element> pExpirence = pDoc->get()->createElement("Expirence");
	AutoPtr<XML::Text> pExpirencet = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Expirence));
	pExpirence->appendChild(pExpirencet);
	pRoot->appendChild(pExpirence);
	AutoPtr<XML::Element> prelaxHP = pDoc->get()->createElement("relaxHP");
	AutoPtr<XML::Text> prelaxHPt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->relaxHP));
	prelaxHP->appendChild(prelaxHPt);
	pRoot->appendChild(prelaxHP);
	AutoPtr<XML::Element> prelaxMP = pDoc->get()->createElement("relaxMP");
	AutoPtr<XML::Text> prelaxMPt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->relaxMP));
	prelaxMP->appendChild(prelaxMPt);
	pRoot->appendChild(prelaxMP);
	AutoPtr<XML::Element> preflection = pDoc->get()->createElement("reflection");
	AutoPtr<XML::Text> preflectiont = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->reflection));
	preflection->appendChild(preflectiont);
	pRoot->appendChild(preflection);
	AutoPtr<XML::Element> parmor = pDoc->get()->createElement("armor");
	AutoPtr<XML::Text> parmort = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->armor));
	parmor->appendChild(parmort);
	pRoot->appendChild(parmor);
	AutoPtr<XML::Element> pprotection = pDoc->get()->createElement("protection");
	AutoPtr<XML::Text> pprotectiont = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->protection));
	pprotection->appendChild(pprotectiont);
	pRoot->appendChild(pprotection);
	AutoPtr<XML::Element> plightingResistance = pDoc->get()->createElement("lightingResistance");
	AutoPtr<XML::Text> plightingResistancet = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->lightingResistance));
	plightingResistance->appendChild(plightingResistancet);
	pRoot->appendChild(plightingResistance);
	AutoPtr<XML::Element> pfireResistance = pDoc->get()->createElement("fireResistance");
	AutoPtr<XML::Text> pfireResistancet = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->fireResistance));
	pfireResistance->appendChild(pfireResistancet);
	pRoot->appendChild(pfireResistance);
	AutoPtr<XML::Element> pcoldResistance = pDoc->get()->createElement("coldResistance");
	AutoPtr<XML::Text> pcoldResistancet = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->coldResistance));
	pcoldResistance->appendChild(pcoldResistancet);
	pRoot->appendChild(pcoldResistance);
	AutoPtr<XML::Element> pmaxHeight = pDoc->get()->createElement("maxHeight");
	AutoPtr<XML::Text> pmaxHeightt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->maxHeight));
	pmaxHeight->appendChild(pmaxHeightt);
	pRoot->appendChild(pmaxHeight);
	AutoPtr<XML::Element> pKarma = pDoc->get()->createElement("Karma");
	AutoPtr<XML::Text> pKarmat = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->Karma));
	pKarma->appendChild(pKarmat);
	pRoot->appendChild(pKarma);
	AutoPtr<XML::Element> piPlayer = pDoc->get()->createElement("iPlayer");
	AutoPtr<XML::Text> piPlayert = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->iPlayer));
	piPlayer->appendChild(piPlayert);
	pRoot->appendChild(piPlayer);
	AutoPtr<XML::Element> piAggressor = pDoc->get()->createElement("iAggressor");
	AutoPtr<XML::Text> piAggressort = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->iAggressor));
	piAggressor->appendChild(piAggressort);
	pRoot->appendChild(piAggressor);
	AutoPtr<XML::Element> pkilled = pDoc->get()->createElement("killed");
	AutoPtr<XML::Text> pkilledt = pDoc->get()->createTextNode(Poco::NumberFormatter::format(this->killed));
	pkilled->appendChild(pkilledt);
	pRoot->appendChild(pkilled);
	AutoPtr<XML::Element> pIDCellPropiska = pDoc->get()->createElement("IDCellPropiska");
	AutoPtr<XML::Text> pIDCellPropiskat = pDoc->get()->createTextNode(this->IDCellPropiska);
	pIDCellPropiska->appendChild(pIDCellPropiskat);
	pRoot->appendChild(pIDCellPropiska);
	AutoPtr<XML::Element> pWorldID = pDoc->get()->createElement("WorldID");
	AutoPtr<XML::Text> pWorldIDt = pDoc->get()->createTextNode(this->WorldID);
	pWorldID->appendChild(pWorldIDt);
	pRoot->appendChild(pWorldID);
	return pDoc;
}

void Player::UpdateAfterAddNPCToCell(CNpc* ptr){
	((*pController).*UpdateControllerAfterAddNPCToCell)(ptr);
}
void Player::UpdateAfterRemoveNPCFromCell(CNpc* ptr){
	((*pController).*UpdateControllerAfterRemoveNPCFromCell)(ptr);
}

Player::~Player(){
	int i = 0;
	i++;
}