#pragma once
template <class C>
class NullReleasePolicy
{
public:
	static void release(C* pObj)
		/// Delete the object.
		/// Note that pObj can be 0.
	{
		//delete[] pObj;
	}
};