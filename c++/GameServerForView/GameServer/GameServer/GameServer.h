#pragma once
#include <iostream>
#include "Poco/Net/TCPServer.h"

#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/Activity.h"
#include "Poco/Task.h"
#include "DBGameService.h"
#include "gameServerConnectionFactory.h"
#include "PlayerController.h"
#include "World.h"

using namespace std;
using Poco::OSP::BundleContext;

extern bool loop;

class GameServer
{
	public:
		CWorld World;
	public:
		GameServer();
		void start();
		void stop();
		DBGameService::Ptr pService;
		BundleContext::Ptr pContext;
		
	protected:
		void runActivity();
	private:
		Poco::Activity<GameServer> _activity;
};