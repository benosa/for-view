#include "PulsarWorldTimer.h"


CPulsarWorldTimer::CPulsarWorldTimer()
{
	TimerCallback<CPulsarWorldTimer> callback(*this, &CPulsarWorldTimer::onTimer);
	WorldTimer.setStartInterval(1000);
	WorldTimer.setPeriodicInterval(1000);
	WorldTimer.start(callback);
}


CPulsarWorldTimer::~CPulsarWorldTimer()
{
	Thread::sleep(5000);
	WorldTimer.stop();	
}

void CPulsarWorldTimer::onTimer(Timer& timer)
{
	//std::cout << "Callback called after  milliseconds." << std::endl;
	//for (int i = 0; i < PlayerList.size(); i++){
		nc.postNotification(new BaseNotification(true));
	//}
}

void CPulsarWorldTimer::addObserver(Target* tgr){
	this->nc.addObserver(
		Observer<Target, BaseNotification>(*tgr, &Target::handleBase)
	);
	//PlayerList.push_back(tgr);
}

void CPulsarWorldTimer::deleteObserver(Target* tgr){
	this->nc.removeObserver(
		Observer<Target, BaseNotification>(*tgr, &Target::handleBase)
		);
}