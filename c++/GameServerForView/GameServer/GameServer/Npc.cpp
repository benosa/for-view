#include "Npc.h"
#include "NullReleasePolicy.h"
#include "Corps.h"
#include "Inventory.h"
#include "Equipment.h"
#include "Lut.h"
#include <stdexcept>
#include "Location.h"
#include "Cell.h"
/*
������� ��� �� ����
*/

CNpc::CNpc(Document::Ptr NPC_doc){
	this->curCountNPC = 0;
	curPeriod = 0;
	this->id = NPC_doc->get("_id")->toString();
	this->CurCell = NPC_doc->get("CurrentCell")->type() == 10 ? "000000000000000000000000" : NPC_doc->get("CurrentCell")->toString();
	this->Name = NPC_doc->get<Array::Ptr>("Name");
	this->Age = NPC_doc->get<int>("Age");
	this->Description = NPC_doc->get<Array::Ptr>("Description");
	//this->DateBorn = NPC_doc->get("DateBorn");
	//this->DateDied = NPC_doc->get("DateDied");
	this->ImgId = NPC_doc->get("ImgId")->type() == 10 ? "" : NPC_doc->get("ImgId")->toString();
	this->Level = NPC_doc->get<int>("Level");
	this->strenght = NPC_doc->get<int>("strenght");
	this->body = NPC_doc->get<int>("body");
	this->intillect = NPC_doc->get<int>("intillect");
	this->dexterity = NPC_doc->get<int>("dexterity");
	this->phisical_Damage = NPC_doc->get<int>("phisical_Damage");
	this->magical_Damage = NPC_doc->get<int>("magical_Damage");
	this->chance_to_Hit = NPC_doc->get<int>("chance_to_Hit");
	this->chance_to_Cast = NPC_doc->get<int>("chance_to_Cast");
	this->suppression = NPC_doc->get<int>("suppression");
	this->absorption = NPC_doc->get<int>("absorption");
	this->HP = NPC_doc->get<int>("HP");
	this->MP = NPC_doc->get<int>("MP");
	this->Gold = NPC_doc->get<int>("Gold");
	this->Platina = NPC_doc->get<int>("Platina");
	this->Silver = NPC_doc->get<int>("Silver");
	this->Expirence = NPC_doc->get<int>("Expirence");
	this->relaxHP = NPC_doc->get<int>("relaxHP");
	this->relaxMP = NPC_doc->get<int>("relaxMP");
	this->reflection = NPC_doc->get<int>("reflection");
	this->armor = NPC_doc->get<int>("armor");
	this->protection = NPC_doc->get<int>("protection");
	this->lightingResistance = NPC_doc->get<int>("lightingResistance");
	this->fireResistance = NPC_doc->get<int>("fireResistance");
	this->coldResistance = NPC_doc->get<int>("coldResistance");
	this->maxHeight = NPC_doc->get<int>("maxHeight");
	this->Karma = NPC_doc->get<int>("Karma");
	this->iPlayer = NPC_doc->get<bool>("iPlayer");
	this->iAggressor = NPC_doc->get<bool>("iAggressor");
	this->killed = NPC_doc->get<bool>("killed");
	this->IDCellPropiska = NPC_doc->get<int>("IDCellPropiska");
	//��� ����� ����� ��� ���� ����� !!!!!!!!!!!!!!!!!
	this->endCountNPC = NPC_doc->get("endCountNPC").isNull() ? 1 : NPC_doc->get<int>("endCountNPC");
	this->isMoved = NPC_doc->get("isMoved").isNull() ? false : NPC_doc->get<bool>("isMoved");
	this->period = NPC_doc->get("period").isNull() ? 3 : NPC_doc->get<int>("period");
	this->LocationID = NPC_doc->get("LocationID")->type() == 10 ? "" : NPC_doc->get("LocationID")->toString();
	//npci->CorpsArray = NPC_doc->get("CorpsArray");
	try{
		if (!NPC_doc->get("CorpsArray").isNull()){
			Document::Ptr corpspdoc = NPC_doc->get<Document::Ptr>("CorpsArray");
			Document::Ptr Equipmentpdoc = corpspdoc->get<Document::Ptr>("Equipment");
			/*
			============================= ���������, ��������� �������������� =======================================
			*/
			Document::Ptr robepdoc = Equipmentpdoc->get("robe")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("robe");
			Document::Ptr headwearpdoc = Equipmentpdoc->get("headwear")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("headwear");
			Document::Ptr armorpdoc = Equipmentpdoc->get("armor")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("armor");
			Document::Ptr necklacepdoc = Equipmentpdoc->get("necklace")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("necklace");
			Document::Ptr weaponpdoc = Equipmentpdoc->get("weapon")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("weapon");
			Document::Ptr shieldpdoc = Equipmentpdoc->get("shield")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("shield");
			Document::Ptr ring1pdoc = Equipmentpdoc->get("ring1")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("ring1");
			Document::Ptr ring2pdoc = Equipmentpdoc->get("ring2")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("ring2");
			Document::Ptr glovepdoc = Equipmentpdoc->get("glove")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("glove");
			Document::Ptr inexpressiblespdoc = Equipmentpdoc->get("inexpressibles")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("inexpressibles");
			Document::Ptr beltpdoc = Equipmentpdoc->get("inexpressibles")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("belt");
			Document::Ptr bootspdoc = Equipmentpdoc->get("boots")->type() == 10 ? NULL : Equipmentpdoc->get<Document::Ptr>("boots");

			//this->Corps = new CCorps;
			//this->Corps->Inventory = new Inventory();
			//this->Corps->Equipment = ICEquipment::Ptr(new Equipment());
			this->Corps.Equipment.robe = robepdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(robepdoc));
			this->Corps.Equipment.headwear = headwearpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(headwearpdoc));
			this->Corps.Equipment.armor = armorpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(armorpdoc));
			this->Corps.Equipment.necklace = necklacepdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(necklacepdoc));
			this->Corps.Equipment.weapon = weaponpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(weaponpdoc));
			this->Corps.Equipment.shield = shieldpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(shieldpdoc));
			this->Corps.Equipment.ring1 = ring1pdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(ring1pdoc));
			this->Corps.Equipment.ring2 = ring2pdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(ring2pdoc));
			this->Corps.Equipment.glove = glovepdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(glovepdoc));
			this->Corps.Equipment.inexpressibles = inexpressiblespdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(inexpressiblespdoc));
			this->Corps.Equipment.belt = beltpdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(beltpdoc));
			this->Corps.Equipment.boots = bootspdoc.isNull() ? NULL : Poco::SharedPtr<CLut>(new CLut(bootspdoc));
			/*
			============================= ���������, ��������� ��������� ������� =======================================
			*/
			Document::Ptr Inventorypdoc = corpspdoc->get("Inventory")->type() == 10 ? NULL : corpspdoc->get<Document::Ptr>("Inventory");
			if (!Inventorypdoc.isNull()){
				this->Corps.Inventory.id = Inventorypdoc->get("_id")->type() == 10 ? "" : Inventorypdoc->get("_id")->toString();
				//this->Corps.Inventory.backpack;
				this->Corps.Inventory.countLut = Inventorypdoc->get<int>("countLut");
				this->Corps.Inventory.MaxCountLut = Inventorypdoc->get<int>("MaxCountLut");
				this->Corps.Inventory.widthLut = Inventorypdoc->get<int>("widthLut");
				this->Corps.Inventory.MaxWidthLut = Inventorypdoc->get<int>("MaxWidthLut");
			}
			/*
			============================= ���������, ��������� ������ ������� =======================================
			*/
			Array::Ptr backpack = Inventorypdoc->get<Array::Ptr>("backpack");
			if (!backpack.isNull()){
				for (int i = 0; i < backpack->size(); i++){
					Document::Ptr backpackpdoc = backpack->get<Document::Ptr>(i);
					if (!backpackpdoc.isNull()){
						this->Corps.Inventory.backpack.push_back(Poco::SharedPtr<CLut>(new CLut(backpackpdoc)));
					}
				}
			}
		}
	}
	catch (Poco::Exception& exc){
		throw std::runtime_error(exc.message() + "Error loading NPC id: " + this->id);
	}
	/*
	============================= ���������, ��������� ������ ����� =======================================
	*/
	try{
		if (!NPC_doc->get("MagicArray").isNull()){
			Array::Ptr MagicArray = NPC_doc->get<Array::Ptr>("MagicArray");
			for (int i = 0; i < MagicArray->size(); i++){
				Document::Ptr magickpdoc = MagicArray->get<Document::Ptr>(i);
				if (!magickpdoc.isNull()){
					this->MagicArray.push_back(Poco::SharedPtr<CLut>(new CLut(magickpdoc)));
				}
			}
		}
	}
	catch (Poco::Exception& exc){
		throw std::runtime_error(exc.message() + "Error loading NPC id: " + this->id);
	}
}

void CNpc::Update(Poco::SharedPtr<CLocation> a, Poco::OSP::BundleContext::Ptr _pContext){
	pContext = _pContext;
	//�������� ��������� �� �������
	Poco::SharedPtr<CLocation>  CurLoc = a;
	if (this->killed){
		if (curPeriod == period){
			//�������� ��������� ����������� ��� ��������� �������� � �������
			CCell* NextCell = CurLoc->vecCCellsArray[rand() % CurLoc->vecCCellsArray.size()];
			NextCell->InjectNPC(INPC);
			INPC->CurrentCell = NextCell;
			INPC->CurCell = NextCell->id;
			curPeriod = 0;
		}
		else{
			curPeriod++;
		}
	}
	if (this->getMoved()){
		if (curPeriod == period){
			
			//�������� ��������� ����������� ��� ������
			std::vector<CCell*> tmp_cell_array;
			if (this->CurrentCell->North != "000000000000000000000000")tmp_cell_array.push_back(CurLoc->CCellsArray[this->CurrentCell->North]);
			if (this->CurrentCell->South != "000000000000000000000000")tmp_cell_array.push_back(CurLoc->CCellsArray[this->CurrentCell->South]);
			if (this->CurrentCell->West != "000000000000000000000000")tmp_cell_array.push_back(CurLoc->CCellsArray[this->CurrentCell->West]);
			if (this->CurrentCell->East != "000000000000000000000000")tmp_cell_array.push_back(CurLoc->CCellsArray[this->CurrentCell->East]);
			if (this->CurrentCell->Up != "000000000000000000000000")tmp_cell_array.push_back(CurLoc->CCellsArray[this->CurrentCell->Up]);
			if (this->CurrentCell->Down != "000000000000000000000000")tmp_cell_array.push_back(CurLoc->CCellsArray[this->CurrentCell->Down]);
			tmp_cell_array.push_back(CurrentCell);

			//�������� ��������� ����������� ��� ������ �� ������
			CCell* NextCell = tmp_cell_array[rand() % tmp_cell_array.size()];
			if (NextCell != CurrentCell){
				pContext->logger().information("I NPC id: " + INPC->id + ", IID: " + Poco::NumberFormatter::format(INPC->IID) + ", timer: " + Poco::NumberFormatter::format(INPC->period) + ", goto id: " + NextCell->id + ", from id: " + CurrentCell->id);
				NextCell->InjectNPC(INPC);
				this->CurrentCell->RemoveNPC(INPC);
				this->CurrentCell = NextCell;
				this->CurCell = NextCell->id;
				
			}
			curPeriod = 0;
		}
		else{
			curPeriod++;
		}
		
	}

}

void CNpc::setINPC(CNpc* npc){
	this->INPC = npc;
}

int CNpc::getCountAllNPC(){
	return this->endCountNPC;
}

int CNpc::getCurCountNPC(){
	return this->curCountNPC;

}

int CNpc::getPeriodRegeneration(){
	return this->period;
}

bool CNpc::getMoved(){
	return this->isMoved;
}

/*void CNpc::MoveUpdate(CLocation* cl){

}*/