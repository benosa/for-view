#ifndef _PLAYER_H_
#define _PLAYER_H_
#include <string>
#include "Poco/SharedPtr.h"
#include "Poco/MongoDB/Document.h"
#include "Poco/MongoDB/Array.h"
#include "Poco/DOM/Document.h"
#include "Poco/DOM/Element.h"
#include "Poco/DOM/Text.h"
#include "Poco/DOM/AutoPtr.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
//#include "PlayerController.h"
#include "CPlayer.h"

using namespace std;
using namespace Poco;
using namespace Poco::MongoDB;


class CPlayerController;
class CNpc;
class Player: virtual public CPlayer
{
public:
	//��� ������ ������� �������
	typedef void (CPlayerController::*pfn_MyFuncType)(CNpc*);
	CPlayerController* pController;
	pfn_MyFuncType UpdateControllerAfterAddNPCToCell;
	pfn_MyFuncType UpdateControllerAfterRemoveNPCFromCell;
	void UpdateAfterAddNPCToCell(CNpc*);
	void UpdateAfterRemoveNPCFromCell(CNpc*);
	Player(){};
	~Player();
	Poco::SharedPtr<Poco::AutoPtr<Poco::XML::Document>> toXML();
	void parse(Document::Ptr);
	std::string username;
	std::string password;
	std::string email;
	std::string id;
	bool logged;
	bool banned;
	bool isAdmin;
private:
	std::string getLangString(std::string Lang, Array::Ptr p);
};
#endif