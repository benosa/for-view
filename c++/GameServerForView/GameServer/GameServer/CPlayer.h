#pragma once
#include <vector>
#include <map>
#include "Poco/DateTime.h"
#include "Poco/SharedPtr.h"
#include "Poco/MongoDB/Array.h"
#include "Poco/DynamicAny.h"
//#include "Player.h"
using namespace Poco::MongoDB;
using Poco::DynamicAny;
#include "Corps.h"
//#include "Worlds.h"
#include "Lut.h"
#include "CSpell.h"
//#include "ICWorlds.h"
//#include "Cell.h"
class Worlds;
class CCell;
class CPlayer
{
public:
	CPlayer();
	~CPlayer();
public:
	//wxSocketBase* sock;
	//typedef Poco::SharedPtr<CPlayer> Ptr;
	Worlds* CurrentWorld;
	CCell* CurrentCell;
	std::string CurCell;
	std::string id;
	std::string Lang;
	int IID;
	Array::Ptr Name;
	int Age;
	Array::Ptr Description;
	Poco::DateTime DateBorn;
	Poco::DateTime DateDied;
	std::string ImgId;
	//�������
	int Level;
	//���������
	int strenght;
	//����
	int body;
	//��������
	int intillect;
	//��������
	int dexterity;
	//���. ����
	int phisical_Damage;
	//���. ����
	int magical_Damage;
	//���� �����
	int chance_to_Hit;
	//���� ���������
	int chance_to_Cast;
	//����������
	int suppression;
	//����������
	int absorption;
	//��
	int HP;
	//��
	int MP;
	//������
	int Gold;
	//�������
	int Platina;
	//�������
	int Silver;
	//����
	int Expirence;
	//������ ��
	int relaxHP;
	//������ ��
	int relaxMP;
	//���������
	int reflection;
	//�����
	int armor;
	//������
	int protection;
	//������ �� ������
	int lightingResistance;
	//������ �� ����
	int fireResistance;
	//������ �� ������
	int coldResistance;
	//������������ ���
	int maxHeight;
	//�����
	int Karma;
	bool iPlayer;
	bool iAggressor;
	bool killed;
	std::string IDCellPropiska;
	std::string LocationID;
	std::string WorldID;
	std::vector<Poco::SharedPtr<CCorps>> CorpsArray;
	CCorps Corps;
	std::map<std::string, Poco::SharedPtr<CSpell>> SpellArray;
	std::map<std::string, Poco::SharedPtr<CSpell>> SkillArray;
	std::vector<Poco::SharedPtr<CLut>> MagicArray;
};