#include "PlayerController.h"
#include "Poco/String.h"
#include "AdminControllerHandler.h"
#include "UserControllerHandler.h"

using Poco::toUpper;
using Poco::toLower;
using Poco::toLowerInPlace;
using Poco::icompare;
//#include "CPlayer.h"

bool CPlayerController::sendBytes(const char bytes[], int bytesToWrite) {
	int tmp = 0;
	try {
		while (bytesToWrite > tmp) {
			//if (!socket->isNull())
			tmp += socket.sendBytes(bytes + tmp, bytesToWrite - tmp);
		}
	}
	catch (Poco::Exception& error) {
		std::cout << "recv failed (Error: " << error.displayText() << ')' << std::endl;
		socket.shutdownSend();
		//delete this;
		return false;
	}
	return true;
}

void CPlayerController::handleBase(BaseNotification* pNf)
{
	IPlayerControllerHandler->UpdateEvent();	
	pNf->release(); // we got ownership, so we must release
}


CPlayerController::CPlayerController(Poco::Net::StreamSocket& _socket, CWorld* _World, DBGameService::Ptr _pService, BundleContext::Ptr _pContext) :socket(_socket){
	World = _World;
	observered = false;
	pService = _pService;
	pContext = _pContext;
	p = NULL;
	try{
		this->getUserIdFromClientAndFillPlayer();
		IPlayer = p;
		if (p != NULL){
			if (p->isAdmin) { 
				IPlayerControllerHandler = new AdminControllerHandler(_socket, World, pService, pContext, p);
			}
			else{
				//IPlayerControllerHandler = new UserControllerHandler(_socket, World, pService, pContext, p);
			}
			World->cpwt->addObserver(this);//resolved delete observer
			observered = true;
			World->PlayerMap[p->id] = &socket;//resolved delete Player from PlayerMap
			this->run();
		}
		else{
			socket.close();
		}
	}
	catch (std::exception& ex){
		std::string st(ex.what());
		st = st + "Player not loading";
		pContext->logger().critical(st);
	}
}

void CPlayerController::getUserIdFromClientAndFillPlayer(){
	p = new Player();//resoved
	try{
		Poco::Timespan timeOut(10, 0);
		unsigned char* incommingBuffer;
		Poco::SharedPtr<RequestCommand> _r;
		socket.setBlocking(false);
		if (socket.poll(timeOut, Poco::Net::Socket::SELECT_READ) != false){
			int size = socket.available();
			if (socket.available() > 0){
				incommingBuffer = new unsigned char[size];
				int nBytes = -1;
				nBytes = socket.receiveBytes(incommingBuffer, size);
				Poco::SharedPtr<RequestCommand> r(new RequestCommand(incommingBuffer, size));
				_r.assign(r);
				delete incommingBuffer;
				std::string stu(r->id);
			}
		}
		Document::Ptr pdoc = pService->getUser(_r->id, true);
		
		if (!pdoc.isNull()){
			p->parse(pdoc);
		}
		else p = NULL;
	}
	catch (std::exception& exc){
		std::string st = exc.what();
		st = st + "Player not loading";
		pContext->logger().critical(st);
		p = NULL;
	}
}

CPlayerController::~CPlayerController()
{	
	if (observered){
		World->cpwt->deleteObserver(this); //resolved delete observer
	}
	map<string, Poco::Net::StreamSocket*>::iterator iter = World->PlayerMap.find(IPlayer->id);
	if (iter != World->PlayerMap.end())	World->PlayerMap.erase(iter); //resolved delete Player from PlayerMap
	//p->UpdateControllerAfterAddNPCToCell = 0;
	p->CurrentCell->RemovePlayer(p);//resolved delete player from cell;
	delete p;//resoved
	delete IPlayerControllerHandler;
	//Sleep(500);
}

void CPlayerController::run(){
	bool isOpen = true;
	Poco::Timespan timeOut(10, 0);
	std::string frame;
	if (p != NULL){

		string curcell = p->CurCell;
		p->CurrentWorld = (*World->WorldsMap)[p->WorldID];
		if (curcell != "" || curcell != "0000000000000000000000000"){
			p->CurrentCell = p->CurrentWorld->Cells[curcell];
		}
		else if (p->IDCellPropiska != "" || p->IDCellPropiska != "0000000000000000000000000"){
			p->CurrentCell = p->CurrentWorld->Cells[p->IDCellPropiska];
		}
		else{
			//throw need;
		}
		
		p->CurrentCell->InjectPlayer(p);//resolved delete player from cell;
		p->pController = this;
		p->UpdateControllerAfterAddNPCToCell = &CPlayerController::UpdateClientAfterAddNpcToCell;
		p->UpdateControllerAfterRemoveNPCFromCell = &CPlayerController::UpdateClientAfterAddNpcToCell;
		while (isOpen){
			if (socket.poll(timeOut, Poco::Net::Socket::SELECT_READ) == false){
				std::cout << "TIMEOUT!" << std::endl << std::flush;
			}
			else{
				int nBytes = 0;
				char* b;
				try{
					while (socket.available() > 0){
						b = new char[socket.available()];
						nBytes = socket.receiveBytes(b, socket.available());
						//bytes.push_back(b);
					}
					if (nBytes != 0){
						ReceiveCommand rc(b, nBytes);
						//ParseCommand(rc);
						IPlayerControllerHandler->Worker(rc);
					}
					else{
						isOpen = false;
					}
				}
				catch(...){

				}
			}
		}
	}
}

void CPlayerController::UpdateClientAfterAddNpcToCell(CNpc* ptr){
	MessageCommand cmd;
	cmd.AddNPC(p,ptr);
	sendBytes(cmd.Message.data(), cmd.Message.size());
}
void CPlayerController::UpdateClientAfterRemoveNpcFromCell(CNpc* ptr){
	MessageCommand cmd;
	cmd.RemoveNPC(ptr);
	sendBytes(cmd.Message.data(), cmd.Message.size());
}