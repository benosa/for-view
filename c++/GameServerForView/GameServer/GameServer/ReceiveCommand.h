#pragma once
#include <stdint.h>
#include "Poco/NumberParser.h"
#include "Poco/NumberFormatter.h"
#include "Poco/StringTokenizer.h"
#include "Poco/String.h" // for cat
#include "Poco/SharedPtr.h"
#include "Poco/InflatingStream.h"
#include "Poco/BinaryReader.h"
#include "Poco/MemoryStream.h"
#include "Poco/DOM/DOMParser.h"
#include "Poco/DOM/Document.h"
#include "Poco/DOM/NodeIterator.h"
#include "Poco/DOM/NodeFilter.h"
#include "Poco/DOM/AutoPtr.h"
#include "Poco/SAX/InputSource.h"
#include "Poco/Exception.h"
#include "Poco/Base64Decoder.h"
#include "Poco/StreamCopier.h"
#include <iostream>
#include <fstream>


using Poco::Base64Decoder;
using Poco::StreamCopier;
#include <iostream>


using Poco::StringTokenizer;
using Poco::cat;

using Poco::NumberFormatter;

using Poco::NumberParser;
using Poco::StringTokenizer;
using Poco::InflatingStreamBuf;



using Poco::XML::DOMParser;
using Poco::XML::InputSource;
using Poco::XML::NodeIterator;
using Poco::XML::NodeFilter;
using Poco::XML::Node;
using Poco::XML::AutoPtr;
using Poco::Exception;

class ReceiveCommand
{
public:
	typedef std::basic_string<wchar_t> wstring;
	wchar_t command;
	string message;
	string id;
public:
	ReceiveCommand(){};
	ReceiveCommand(char* buffer, int size){
		Poco::MemoryInputStream mstr(buffer,size);
		std::stringstream outputstream;
		Poco::InflatingInputStream inflater(mstr, InflatingStreamBuf::STREAM_ZLIB);
		StreamCopier::copyStream(inflater, outputstream);
		InputSource src(outputstream);
		try
		{
			DOMParser parser;
			AutoPtr<Poco::XML::Document> pDoc = parser.parse(&src);

			NodeIterator it(pDoc, Poco::XML::NodeFilter::SHOW_ALL);
			Node* pNode = it.nextNode();
			while (pNode)
			{
				if (pNode->nodeName() == "command"){
					XML::XMLString st = pNode->firstChild()->getNodeValue();
					stringstream ss;
					stringstream ostr;
					ss << pNode->firstChild()->getNodeValue();
					Base64Decoder decoder(ss);
					StreamCopier::copyStream(decoder, ostr);				
					memcpy(&command, ostr.str().data(), 2);
				}
				else if (pNode->nodeName() == "message"){
					XML::XMLString st = pNode->firstChild()->getNodeValue();
					message = st;
				}
				else if (pNode->nodeName() == "id"){
					XML::XMLString st = pNode->firstChild()->getNodeValue();
					id = st;
				}
				auto p = pNode;
				pNode = it.nextNode();
			}

		}
		catch (Exception& exc)
		{
			std::cerr << exc.displayText() << std::endl;
		}
		
	}
};