#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"

#include "Poco/OSP/BundleActivator.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/OSP/ServiceRegistry.h"
#include "Poco/ClassLibrary.h"

#include "DBGameService.h"
bool loop;
#include "GameServer.h"


using Poco::OSP::BundleActivator;
using Poco::OSP::BundleContext;
using Poco::OSP::ServiceRef;
using Poco::OSP::Service;
using Poco::Thread;
DBGameService::Ptr pService;

class GameServerBundleActivator: public BundleActivator
	/// The ParticleEffectsBundleActivator for the ParticleEffects bundle.
{
public:
	GameServer example;
	GameServerBundleActivator()
	{
	}

	~GameServerBundleActivator()
	{
		loop = false;
		example.stop();
	}

	void start(BundleContext::Ptr pContext)
	{
		ServiceRef::Ptr pServiceRef = pContext->registry().findByName("com.InstitutIt.GameServer.DBGameService");
		if (pServiceRef)
			{
				pService = pServiceRef->castedInstance<DBGameService>();
				example.pService = pService;
				example.pContext = pContext;
				std::string msg("****** ");
				msg += " ******";
				pContext->logger().information(msg);
				example.start();
			}
			else
			{
				// The service is not available
				pContext->logger().error("The DatabaseServerService is not available.");
			}
	}

	void stop(BundleContext::Ptr pContext)
	{
		loop = false;
		example.stop();
	}

private:
	//ServiceRef::Ptr _pService;
};




POCO_BEGIN_MANIFEST(BundleActivator)
	POCO_EXPORT_CLASS(GameServerBundleActivator)
POCO_END_MANIFEST