#pragma once
#include <list>
#include "Poco/NotificationCenter.h"
#include "Poco/Notification.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/AutoPtr.h"
#include "Poco/Timer.h"
#include <iostream>
#include "helper.h"
//#include "PlayerController.h"
using Poco::NotificationCenter;
using Poco::Notification;
using Poco::Observer;
using Poco::NObserver;
using Poco::AutoPtr;
using Poco::Timer;
using Poco::TimerCallback;
using Poco::Thread;

class CPulsarWorldTimer
{
public:
	CPulsarWorldTimer();
	~CPulsarWorldTimer();
public:
	void onTimer(Timer& timer);
	void addObserver(Target*);
	void deleteObserver(Target*);
public:
	std::list<Target*> PlayerList;
private:
	Timer WorldTimer;
	NotificationCenter nc;
};
