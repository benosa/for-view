#pragma once
#include "Poco/Net/TCPServer.h"

#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/OSP/BundleContext.h"
#include "DBGameService.h"
#include "PlayerController.h"

using namespace std;
using Poco::OSP::BundleContext;

class newConnection : public Poco::Net::TCPServerConnection {
public:
	BundleContext::Ptr pContext;
	newConnection(const Poco::Net::StreamSocket& s, DBGameService::Ptr _pService, CWorld* _World, BundleContext::Ptr _pContext);

	void run();
	~newConnection();
public:
	DBGameService::Ptr pService;
	/*CPlayerController* pCtrl;
	CWorld* World;*/
	//Poco::SharedPtr<CPlayerController>
	CPlayerController* pCtrl;
	CWorld* World;
};