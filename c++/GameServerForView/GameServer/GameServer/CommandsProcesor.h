#pragma once
#include <string>
#include <list>
#include <vector>
#include <queue>
//#include "CPlayer.h"
#include <functional>
#include "ReceiveCommand.h"
#include "ICPlayerController.h"
#include "Poco/OSP/BundleContext.h"

class ICPlayerController;
using Poco::OSP::BundleContext;

class CCommandsProcesor
{
public:
	//typedef void(CCommandsProcesor::*pfn_MyFuncType)();
	typedef void (CCommandsProcesor::*pfn_MyFuncType)();
	CCommandsProcesor();
	~CCommandsProcesor();
public:
	
	void ParseCommand(ReceiveCommand&);
	void MoveUpdate();
public:
	std::queue<pfn_MyFuncType> MoveArray;
	std::list<std::string> CombatArray;
	std::list<std::string> MagicArray;
	//void PushCommand(ReceiveCommand&);
public:
	ICPlayerController::Ptr p;
private:
	//BundleContext::Ptr pContext;
	void Up();
	void Down();
	void North();
	void South();
	void West();
	void East();

	void Kill();

	void Open();
	void Look();
	void LookAt();

	void InjectPlayer(Player*);

	
	void Send();
	void SendTo();
	void ISave();
	void Prompt();
	void Exit();
};