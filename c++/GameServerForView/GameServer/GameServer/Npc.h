#pragma once
#include <vector>
#include <list>
//#include "MLocation.h"
#include "Poco/MongoDB/Document.h"
#include "Poco/MongoDB/Array.h"
#include "Poco/SharedPtr.h"
#include "Poco/Any.h"
#include "Poco/DynamicAny.h"
#include "Poco/OSP/BundleContext.h"

#include "CPlayer.h"
#include "Corps.h"
//#include "Location.h"
//#include "ICLocation.h"
//#include "Magic.h"

using namespace Poco::MongoDB;
class CLocation;
class CNpc : virtual public CPlayer
{
public:
	CNpc(){};
	CNpc(Document::Ptr);
	~CNpc(){};
	void Update(Poco::SharedPtr<CLocation>, Poco::OSP::BundleContext::Ptr);
	void setINPC(CNpc*);
public:
	Poco::OSP::BundleContext::Ptr pContext;
	//Poco::SharedPtr<CCorps> Corps;
	std::vector<Poco::SharedPtr<CLut>> MagicArray;
	int getCountAllNPC();
	int getCurCountNPC();
	int getPeriodRegeneration();
	bool getMoved();
	std::string getLangString(Array::Ptr p);
public:

	int endCountNPC;
	int curCountNPC;
	int period;
	int curPeriod;
	bool isMoved;
	CNpc* INPC;
};

