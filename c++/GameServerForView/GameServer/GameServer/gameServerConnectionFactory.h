#pragma once
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "DBGameService.h"
#include "Poco/OSP/BundleContext.h"
#include "newConnection.h"

class GameServerConnectionFactory : public Poco::Net::TCPServerConnectionFactory
{
public:
	GameServerConnectionFactory(DBGameService::Ptr _pService, BundleContext::Ptr _pContext, CWorld* _World) :
		pService(_pService), pContext(_pContext), World(_World)
	{
	}

	Poco::Net::TCPServerConnection* createConnection(const Poco::Net::StreamSocket& socket)
	{
		return new newConnection(socket, pService, World, pContext);
	}

private:
	std::string _format;
	DBGameService::Ptr pService;
	BundleContext::Ptr pContext;
	CWorld* World;
};