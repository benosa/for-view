#include "UserControllerHandler.h"
#include "PlayerController.h"

UserControllerHandler::UserControllerHandler(Poco::Net::StreamSocket& _socket, CWorld* _World, DBGameService::Ptr _pService, BundleContext::Ptr _pContext, Player* _p) : socket(_socket)
{
	World = _World;
	pService = _pService;
	pContext = _pContext;
	p = _p;
}


UserControllerHandler::~UserControllerHandler()
{
}

bool UserControllerHandler::sendBytes(const char bytes[], int bytesToWrite) {
	int tmp = 0;
	try {
		while (bytesToWrite > tmp) {
			//if (!socket->isNull())
			tmp += socket.sendBytes(bytes + tmp, bytesToWrite - tmp);
		}
	}
	catch (Poco::Exception& error) {
		std::cout << "recv failed (Error: " << error.displayText() << ')' << std::endl;
		socket.shutdownSend();
		//delete this;
		return false;
	}
	return true;
}

void UserControllerHandler::ParseCommand(ReceiveCommand& rc){
	if (rc.command == Command::West){
		MoveArray.push(&UserControllerHandler::West);
	}
	else if (rc.command == Command::East){
		MoveArray.push(&UserControllerHandler::East);
	}
	else if (rc.command == Command::North){
		MoveArray.push(&UserControllerHandler::North);
	}
	else if (rc.command == Command::South){
		MoveArray.push(&UserControllerHandler::South);
	}
	else if (rc.command == Command::Up){
		MoveArray.push(&UserControllerHandler::Up);
	}
	else if (rc.command == Command::Down){
		MoveArray.push(&UserControllerHandler::Down);
	}
	else if (rc.command == Command::Look){
		this->Look();
	}
	else if (rc.command == Command::AboutMe){
		this->AboutMe();
	}
	/*����� ����� �������� �������� �� ������������� ��������*/
	else if (rc.command == Command::Killnpc){
		CombatArray.push_back(p->CurrentCell->NPCMap[rc.id]);
	}
	/*����� ����� �������� �������� �� ������������� ��������*/
	else if (rc.command == Command::Killplayer){
		CombatArray.push_back(p->CurrentCell->PlayersMap[rc.id]);
		CPlayerController* pcdt = p->CurrentCell->PlayersMap[rc.id]->pController;
		pcdt->IPlayerControllerHandler->SetCombatPlayer(p);
		//p->CurrentCell->PlayersMap[rc.id]->pController->CombatArray.push_back(p);
	}
}

void UserControllerHandler::AboutMe(){
	pContext->logger().information("i am looking around\n");
	MessageCommand cmd;
	cmd.AboutMe(p);
	sendBytes(cmd.Message.data(), cmd.Message.size());
}

void UserControllerHandler::Look(){
	/*
	1) ���������� ������� � ������
	2) ���������� ��� � ������
	3) ���������� ��� � ������
	*/
	pContext->logger().information("i am looking around\n");
	MessageCommand cmd;
	cmd.Look(p);
	//cmd.Message;
	int tt = cmd.Message.size();
	sendBytes(cmd.Message.data(), cmd.Message.size());
}
void UserControllerHandler::Up(){
	if (p->CurrentCell->Up != "000000000000000000000000"){
		pContext->logger().information("i go to Up\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->Up];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void UserControllerHandler::Down(){
	if (p->CurrentCell->Down != "000000000000000000000000"){
		pContext->logger().information("i go to Down\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->Down];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void UserControllerHandler::North(){
	if (p->CurrentCell->North != "000000000000000000000000"){
		pContext->logger().information("i go to North\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->North];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void UserControllerHandler::South(){
	if (p->CurrentCell->South != "000000000000000000000000"){
		pContext->logger().information("i go to South\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->South];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void UserControllerHandler::West(){
	if (p->CurrentCell->West != "000000000000000000000000"){
		pContext->logger().information("i go to west\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->West];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void UserControllerHandler::East(){
	if (p->CurrentCell->East != "000000000000000000000000"){
		pContext->logger().information("i go to East\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->East];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}

void UserControllerHandler::Kill(){

}
void UserControllerHandler::Open(){}

void UserControllerHandler::UpdateEvent(){
	if (CombatArray.size() == 0){
		this->MoveUpdate();
	}
	else this->CombatUpdate();
}

void UserControllerHandler::SetCombatPlayer(Player* RemotePlayer){
	CombatArray.push_back(RemotePlayer);
}

void UserControllerHandler::MoveUpdate(){
	if (MoveArray.size() > 0){
		pfn_MyFuncType p = MoveArray.front();
		((*this).*p)();
		MoveArray.pop();
	}
}

void UserControllerHandler::CombatUpdate(){
	if (CombatArray.size() > 0){
		for (int i = 0; i < CombatArray.size(); i++){
			//CombatArray[i]->
			//pfn_MyFuncType p = CombatArray.front();
			//((*this).*CombatArray)();
			//MoveArray.pop();
			class Params;
			Params* p1;
			Params* p2;
		}

	}
}