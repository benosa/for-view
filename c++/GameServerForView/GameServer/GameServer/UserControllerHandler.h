#pragma once
#include "IPlayerController.h"
#include "Player.h"
//#include "MessageCommand.h"
//#include "ReceiveCommand.h"
class ReceiveCommand;
class UserControllerHandler : public IPlayerController
{
public:
	typedef void (UserControllerHandler::*pfn_MyFuncType)();
	std::queue<pfn_MyFuncType> MoveArray;
	std::vector<CPlayer*> CombatArray;
	std::vector<std::string> MagicArray;
private:
	CWorld* World;
	DBGameService::Ptr pService;
	BundleContext::Ptr pContext;
	Player* p;
	Poco::Net::StreamSocket& socket;
public:
	UserControllerHandler(Poco::Net::StreamSocket& _socket, CWorld* _World, DBGameService::Ptr _pService, BundleContext::Ptr _pContext, Player* _p);
	~UserControllerHandler();
	bool sendBytes(const char bytes[], int bytesToWrite);
public:
	void Worker(ReceiveCommand&){};
	void UpdateEvent();
	void SetCombatPlayer(Player*);
private:
	void ParseCommand(ReceiveCommand&);
private:
	void Up();
	void Down();
	void North();
	void South();
	void West();
	void East();

	void Kill();

	void Open();
	void Look();
	void LookAt();

	void InjectPlayer(Player*);


	void Send();
	void SendTo();
	void ISave();
	void Prompt();
	void Exit();
	void AboutMe();
	void MoveUpdate();
	void CombatUpdate();
};

