#include "newConnection.h"
newConnection::newConnection(const Poco::Net::StreamSocket& s, DBGameService::Ptr _pService, CWorld* _World, BundleContext::Ptr _pContext) :
Poco::Net::TCPServerConnection(s) {
	pContext = _pContext;
	pService = _pService;
	World = _World;
}


void newConnection::run() {
	std::string str = "New connection from: " + socket().peerAddress().host().toString();
	pContext->logger().information(str);
	//pContext->logger().error("New connection from.");
	pCtrl = new CPlayerController(socket(), World, pService,pContext);
	//pCtrl->pController = pCtrl;
	//std::cout << "Connection finished!" << endl << flush;
}

newConnection::~newConnection(){
	//pCtrl->
	delete pCtrl;
	pContext->logger().notice("Connection for Game Server ------------------------  finished!");
}
