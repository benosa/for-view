#pragma once
#include <vector>
//#include "CPlayer.h"
#include "Lut.h"
//#include "Worlds.h"
#include "Npc.h"
#include "Poco/SharedPtr.h"
#include "NullReleasePolicy.h"
#include "Poco/MongoDB/Document.h"
#include "Poco/MongoDB/Array.h"
class Worlds;
class Player;
class CCell
{
private:
	std::vector<Player*> Players;
	std::vector<CLut*> Lut;
	std::vector<CNpc*> NPC;
public:
	CCell* ICell;
	Worlds* ICurrentWorld;
	std::map<std::string, Player*> PlayersMap;
	std::map<std::string, CNpc*> NPCMap;
public:
	Poco::MongoDB::Array::Ptr info;
	unsigned int type;
	unsigned char napravlenie;
	int ID;
	std::string North;
	std::string South;
	std::string East;
	std::string West;
	std::string Up;
	std::string Down;
	std::string id;


	CCell(void){};
	~CCell(void);
	std::vector<CNpc*>* getArrayNPC();
	void InjectNPC(CNpc*);
	void InjectPlayer(Player*);
	void InjectLut(CLut*);
	void RemoveNPC(CNpc*);
	void RemovePlayer(Player*);
	void RemoveLut(CLut*);
};
