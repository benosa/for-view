#include "CommandsProcesor.h"
#include "Poco/String.h"
using Poco::toUpper;
using Poco::toLower;
using Poco::toLowerInPlace;
using Poco::icompare;


CCommandsProcesor::CCommandsProcesor()
{
	
}


CCommandsProcesor::~CCommandsProcesor()
{
}

void CCommandsProcesor::ParseCommand(ReceiveCommand& rc){
	if (rc.command == Command::West){
		MoveArray.push(&CCommandsProcesor::West);
	}
	else if (rc.command == Command::East){
		MoveArray.push(&CCommandsProcesor::East);
	}
	else if (rc.command == Command::North){
		MoveArray.push(&CCommandsProcesor::North);
	}
	else if (rc.command == Command::South){
		MoveArray.push(&CCommandsProcesor::South);
	}
	else if (rc.command == Command::Up){
		MoveArray.push(&CCommandsProcesor::Up);
	}
	else if (rc.command == Command::Down){
		MoveArray.push(&CCommandsProcesor::Down);
	}
}

void CCommandsProcesor::MoveUpdate(){
	if (MoveArray.size() > 0){
		pfn_MyFuncType p = MoveArray.front();
		((*this).*p)();
		MoveArray.pop();
	}
}
void CCommandsProcesor::Up(){
	p->pContext->logger().information("i go to Up\n");
}
void CCommandsProcesor::Down(){
	p->pContext->logger().information("i go to Down\n");
}
void CCommandsProcesor::North(){
	p->pContext->logger().information("i go to North\n");
}
void CCommandsProcesor::South(){
	p->pContext->logger().information("i go to South\n");
}
void CCommandsProcesor::West(){
	p->pContext->logger().information("i go to west\n");
}
void CCommandsProcesor::East(){
	p->pContext->logger().information("i go to East\n");
}
void CCommandsProcesor::Kill(){}
void CCommandsProcesor::Open(){}