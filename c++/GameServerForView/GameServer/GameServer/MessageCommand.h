#pragma once
#include <stdint.h>
#include "Poco/DOM/Document.h"
#include "Poco/DOM/Element.h"
#include "Poco/DOM/Text.h"
#include "Poco/DOM/AutoPtr.h"
#include "Poco/DOM/DOMWriter.h"
#include "Poco/XML/XMLWriter.h"
#include "Poco/UTF8Encoding.h"
#include "Poco/BinaryReader.h"
#include "Poco/BinaryWriter.h"
#include "Poco/NumberFormatter.h"
#include "Player.h"
#include "Cell.h"
#include "Npc.h"
#include "Poco/DeflatingStream.h"
#include "Poco/StreamCopier.h"
#include <string>
#include "Poco/MongoDB/Document.h"
#include "Poco/MongoDB/Array.h"
#include "DBGameService.h"
using namespace Poco::MongoDB;

using namespace Poco;
using Poco::DeflatingOutputStream;
using Poco::DeflatingStreamBuf;
using Poco::StreamCopier;

enum TCommand{
	Message = 0x0101,
	Prompt = 0x0103,
	_AboutMe = 0x0108,
	_AddNPC = 0x0109
};
enum Command {
	East = 0x0101,
	West = 0x0102,
	North = 0x0103,
	South = 0x0104,
	Up = 0x0105,
	Down = 0x0106,
	Look = 0x0107,
	AboutMe = 0x0108,
	AddNPC = 0x0109,
	RemoveNPC = 0x010A,
	Killnpc = 0x010B,
	Killplayer = 0x010C,
	reinitializate = 0x010D
};
extern DBGameService::Ptr pService;
inline std::string getLangString(std::string Lang, Array::Ptr p, std::string name){
	std::string rval = "";
	std::string id;
	std::string ref;
	try{
		Document::Ptr ptr;
		if (Lang == "rus") {
			ptr = p->get<Document::Ptr>(1);
			ptr = ptr->get<Document::Ptr>("rus");
		}
		else if (Lang == "eng") {
			ptr = p->get<Document::Ptr>(0);
			ptr = ptr->get<Document::Ptr>("eng");
		}
		id = ptr->get("$id")->toString();
		ref = ptr->get<std::string>("$ref");
		rval = *pService->getTranslate(ref, id, name);
	}
	catch (...){
		rval = "Translate Lang not found!";
	}
	return rval;
}

class MessageCommand
{
public:
	typedef std::basic_string<wchar_t> wstring;

	/*wchar_t command;
	wstring message;
	wstring id;*/
	std::string Message;
public:
	MessageCommand(){};
	void Look(Player* p){
		AutoPtr<XML::Document> pDoc = new XML::Document;
		AutoPtr<XML::Element> pRoot = pDoc->createElement("root");
		pDoc->appendChild(pRoot);
		AutoPtr<XML::Element> command = pDoc->createElement("command");
		AutoPtr<XML::Text> pcommandt = pDoc->createTextNode(Poco::NumberFormatter::format(Command::Look));
		command->appendChild(pcommandt);
		pRoot->appendChild(command);
		AutoPtr<XML::Element> info = pDoc->createElement("info");
		AutoPtr<XML::Text> pText1 = pDoc->createTextNode(getLangString(p->Lang, p->CurrentCell->info,"Info") + " ; myID: " + p->CurrentCell->id);
		info->appendChild(pText1);
		pRoot->appendChild(info);
		AutoPtr<XML::Element> pNPCArray = pDoc->createElement("NPCArray");
		pRoot->appendChild(pNPCArray);
		std::vector<CNpc*>* ArrayNPC = p->CurrentCell->getArrayNPC();
		for (int i = 0; i < ArrayNPC->size(); i++){
			//id
			AutoPtr<XML::Element> pNPC = pDoc->createElement("NPC");
			pNPCArray->appendChild(pNPC);
			AutoPtr<XML::Element> id = pDoc->createElement("id");
			AutoPtr<XML::Text> idt = pDoc->createTextNode((*ArrayNPC)[i]->id);
			id->appendChild(idt);
			pNPC->appendChild(id);
			AutoPtr<XML::Element> pCurCell = pDoc->createElement("CurCell");
			AutoPtr<XML::Text> pCurCellt = pDoc->createTextNode((*ArrayNPC)[i]->CurCell);
			pCurCell->appendChild(pCurCellt);
			pNPC->appendChild(pCurCell);
			AutoPtr<XML::Element> pName = pDoc->createElement("Name");
			AutoPtr<XML::Text> pNamet = pDoc->createTextNode(getLangString(p->Lang, (*ArrayNPC)[i]->Name, "Name"));
			pName->appendChild(pNamet);
			pNPC->appendChild(pName);
			AutoPtr<XML::Element> pAge = pDoc->createElement("Age");
			AutoPtr<XML::Text> pAget = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Age));
			pAge->appendChild(pAget);
			pNPC->appendChild(pAge);
			AutoPtr<XML::Element> pDescription = pDoc->createElement("Description");
			AutoPtr<XML::Text> pDescriptiont = pDoc->createTextNode(getLangString(p->Lang, (*ArrayNPC)[i]->Description,"Description"));
			pDescription->appendChild(pDescriptiont);
			pNPC->appendChild(pDescription);
			AutoPtr<XML::Element> pImgId = pDoc->createElement("ImgId");
			AutoPtr<XML::Text> pImgIdt = pDoc->createTextNode((*ArrayNPC)[i]->ImgId);
			pImgId->appendChild(pImgIdt);
			pNPC->appendChild(pImgId);
			AutoPtr<XML::Element> pLevel = pDoc->createElement("Level");
			AutoPtr<XML::Text> pLevelt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Level));
			pLevel->appendChild(pLevelt);
			pNPC->appendChild(pLevel);
			AutoPtr<XML::Element> pstrenght = pDoc->createElement("strenght");
			AutoPtr<XML::Text> pstrenghtt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->strenght));
			pstrenght->appendChild(pstrenghtt);
			pNPC->appendChild(pstrenght);
			AutoPtr<XML::Element> pbody = pDoc->createElement("body");
			AutoPtr<XML::Text> pbodyt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->body));
			pbody->appendChild(pbodyt);
			pNPC->appendChild(pbody);
			AutoPtr<XML::Element> pintillect = pDoc->createElement("intillect");
			AutoPtr<XML::Text> pintillectt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->intillect));
			pintillect->appendChild(pintillectt);
			pNPC->appendChild(pintillect);
			AutoPtr<XML::Element> pdexterity = pDoc->createElement("dexterity");
			AutoPtr<XML::Text> pdexterityt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->dexterity));
			pdexterity->appendChild(pdexterityt);
			pNPC->appendChild(pdexterity);
			AutoPtr<XML::Element> pphisical_Damage = pDoc->createElement("phisical_Damage");
			AutoPtr<XML::Text> pphisical_Damaget = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->phisical_Damage));
			pphisical_Damage->appendChild(pphisical_Damaget);
			pNPC->appendChild(pphisical_Damage);
			AutoPtr<XML::Element> pmagical_Damage = pDoc->createElement("magical_Damage");
			AutoPtr<XML::Text> pmagical_Damaget = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->magical_Damage));
			pmagical_Damage->appendChild(pmagical_Damaget);
			pNPC->appendChild(pmagical_Damage);
			AutoPtr<XML::Element> pchance_to_Hit = pDoc->createElement("chance_to_Hit");
			AutoPtr<XML::Text> pchance_to_Hitt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->chance_to_Hit));
			pchance_to_Hit->appendChild(pchance_to_Hitt);
			pNPC->appendChild(pchance_to_Hit);
			AutoPtr<XML::Element> pchance_to_Cast = pDoc->createElement("chance_to_Cast");
			AutoPtr<XML::Text> pchance_to_Castt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->chance_to_Cast));
			pchance_to_Cast->appendChild(pchance_to_Castt);
			pNPC->appendChild(pchance_to_Cast);
			AutoPtr<XML::Element> psuppression = pDoc->createElement("suppression");
			AutoPtr<XML::Text> psuppressiont = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->suppression));
			psuppression->appendChild(psuppressiont);
			pNPC->appendChild(psuppression);
			AutoPtr<XML::Element> pabsorption = pDoc->createElement("absorption");
			AutoPtr<XML::Text> pabsorptiont = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->absorption));
			pabsorption->appendChild(pabsorptiont);
			pNPC->appendChild(pabsorption);
			AutoPtr<XML::Element> pHP = pDoc->createElement("HP");
			AutoPtr<XML::Text> pHPt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->HP));
			pHP->appendChild(pHPt);
			pNPC->appendChild(pHP);
			AutoPtr<XML::Element> pMP = pDoc->createElement("MP");
			AutoPtr<XML::Text> pMPt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->MP));
			pMP->appendChild(pMPt);
			pNPC->appendChild(pMP);
			AutoPtr<XML::Element> pGold = pDoc->createElement("Gold");
			AutoPtr<XML::Text> pGoldt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Gold));
			pGold->appendChild(pGoldt);
			pNPC->appendChild(pGold);
			AutoPtr<XML::Element> pPlatina = pDoc->createElement("Platina");
			AutoPtr<XML::Text> pPlatinat = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Platina));
			pPlatina->appendChild(pPlatinat);
			pNPC->appendChild(pPlatina);
			AutoPtr<XML::Element> pSilver = pDoc->createElement("Silver");
			AutoPtr<XML::Text> pSilvert = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Silver));
			pSilver->appendChild(pSilvert);
			pNPC->appendChild(pSilver);
			AutoPtr<XML::Element> pExpirence = pDoc->createElement("Expirence");
			AutoPtr<XML::Text> pExpirencet = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Expirence));
			pExpirence->appendChild(pExpirencet);
			pNPC->appendChild(pExpirence);
			AutoPtr<XML::Element> prelaxHP = pDoc->createElement("relaxHP");
			AutoPtr<XML::Text> prelaxHPt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->relaxHP));
			prelaxHP->appendChild(prelaxHPt);
			pNPC->appendChild(prelaxHP);
			AutoPtr<XML::Element> prelaxMP = pDoc->createElement("relaxMP");
			AutoPtr<XML::Text> prelaxMPt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->relaxMP));
			prelaxMP->appendChild(prelaxMPt);
			pNPC->appendChild(prelaxMP);
			AutoPtr<XML::Element> preflection = pDoc->createElement("reflection");
			AutoPtr<XML::Text> preflectiont = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->reflection));
			preflection->appendChild(preflectiont);
			pNPC->appendChild(preflection);
			AutoPtr<XML::Element> parmor = pDoc->createElement("armor");
			AutoPtr<XML::Text> parmort = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->armor));
			parmor->appendChild(parmort);
			pNPC->appendChild(parmor);
			AutoPtr<XML::Element> pprotection = pDoc->createElement("protection");
			AutoPtr<XML::Text> pprotectiont = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->protection));
			pprotection->appendChild(pprotectiont);
			pNPC->appendChild(pprotection);
			AutoPtr<XML::Element> plightingResistance = pDoc->createElement("lightingResistance");
			AutoPtr<XML::Text> plightingResistancet = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->lightingResistance));
			plightingResistance->appendChild(plightingResistancet);
			pNPC->appendChild(plightingResistance);
			AutoPtr<XML::Element> pfireResistance = pDoc->createElement("fireResistance");
			AutoPtr<XML::Text> pfireResistancet = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->fireResistance));
			pfireResistance->appendChild(pfireResistancet);
			pNPC->appendChild(pfireResistance);
			AutoPtr<XML::Element> pcoldResistance = pDoc->createElement("coldResistance");
			AutoPtr<XML::Text> pcoldResistancet = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->coldResistance));
			pcoldResistance->appendChild(pcoldResistancet);
			pNPC->appendChild(pcoldResistance);
			AutoPtr<XML::Element> pmaxHeight = pDoc->createElement("maxHeight");
			AutoPtr<XML::Text> pmaxHeightt = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->maxHeight));
			pmaxHeight->appendChild(pmaxHeightt);
			pNPC->appendChild(pmaxHeight);
			AutoPtr<XML::Element> pKarma = pDoc->createElement("Karma");
			AutoPtr<XML::Text> pKarmat = pDoc->createTextNode(Poco::NumberFormatter::format((*ArrayNPC)[i]->Karma));
			pKarma->appendChild(pKarmat);
			pNPC->appendChild(pKarma);
			AutoPtr<XML::Element> piAggressor = pDoc->createElement("iAggressor");
			AutoPtr<XML::Text> piAggressort = pDoc->createTextNode(((*ArrayNPC)[i]->iAggressor == true)?"true":"false");
			piAggressor->appendChild(piAggressort);
			pNPC->appendChild(piAggressor);
			AutoPtr<XML::Element> pWorldID = pDoc->createElement("WorldID");
			AutoPtr<XML::Text> pWorldIDt = pDoc->createTextNode((*ArrayNPC)[i]->WorldID);
			pWorldID->appendChild(pWorldIDt);
			pNPC->appendChild(pWorldID);
			
		}
		//AutoPtr<XML::Element> close = pDoc->createElement("close");
		//pRoot->appendChild(close);
		toString(pDoc);	
	}
	void AddNPC(Player* pl, CNpc* p){
		AutoPtr<XML::Document> pDoc = new XML::Document;
		AutoPtr<XML::Element> pRoot = pDoc->createElement("root");
		pDoc->appendChild(pRoot);
		AutoPtr<XML::Element> command = pDoc->createElement("command");
		AutoPtr<XML::Text> pcommandt = pDoc->createTextNode(Poco::NumberFormatter::format(Command::AddNPC));
		command->appendChild(pcommandt);
		pRoot->appendChild(command);
			AutoPtr<XML::Element> id = pDoc->createElement("id");
			AutoPtr<XML::Text> idt = pDoc->createTextNode(p->id);
			id->appendChild(idt);
			pRoot->appendChild(id);
			AutoPtr<XML::Element> pCurCell = pDoc->createElement("CurCell");
			AutoPtr<XML::Text> pCurCellt = pDoc->createTextNode(p->CurCell);
			pCurCell->appendChild(pCurCellt);
			pRoot->appendChild(pCurCell);
			AutoPtr<XML::Element> pName = pDoc->createElement("Name");
			AutoPtr<XML::Text> pNamet = pDoc->createTextNode(getLangString(pl->Lang, (p->Name),"Name"));
			pName->appendChild(pNamet);
			pRoot->appendChild(pName);
			AutoPtr<XML::Element> pAge = pDoc->createElement("Age");
			AutoPtr<XML::Text> pAget = pDoc->createTextNode(Poco::NumberFormatter::format(p->Age));
			pAge->appendChild(pAget);
			pRoot->appendChild(pAge);
			AutoPtr<XML::Element> pDescription = pDoc->createElement("Description");
			AutoPtr<XML::Text> pDescriptiont = pDoc->createTextNode(getLangString(pl->Lang, p->Description,"Description"));
			pDescription->appendChild(pDescriptiont);
			pRoot->appendChild(pDescription);
			AutoPtr<XML::Element> pImgId = pDoc->createElement("ImgId");
			AutoPtr<XML::Text> pImgIdt = pDoc->createTextNode(p->ImgId);
			pImgId->appendChild(pImgIdt);
			pRoot->appendChild(pImgId);
			AutoPtr<XML::Element> pLevel = pDoc->createElement("Level");
			AutoPtr<XML::Text> pLevelt = pDoc->createTextNode(Poco::NumberFormatter::format(p->Level));
			pLevel->appendChild(pLevelt);
			pRoot->appendChild(pLevel);
			AutoPtr<XML::Element> pstrenght = pDoc->createElement("strenght");
			AutoPtr<XML::Text> pstrenghtt = pDoc->createTextNode(Poco::NumberFormatter::format(p->strenght));
			pstrenght->appendChild(pstrenghtt);
			pRoot->appendChild(pstrenght);
			AutoPtr<XML::Element> pbody = pDoc->createElement("body");
			AutoPtr<XML::Text> pbodyt = pDoc->createTextNode(Poco::NumberFormatter::format(p->body));
			pbody->appendChild(pbodyt);
			pRoot->appendChild(pbody);
			AutoPtr<XML::Element> pintillect = pDoc->createElement("intillect");
			AutoPtr<XML::Text> pintillectt = pDoc->createTextNode(Poco::NumberFormatter::format(p->intillect));
			pintillect->appendChild(pintillectt);
			pRoot->appendChild(pintillect);
			AutoPtr<XML::Element> pdexterity = pDoc->createElement("dexterity");
			AutoPtr<XML::Text> pdexterityt = pDoc->createTextNode(Poco::NumberFormatter::format(p->dexterity));
			pdexterity->appendChild(pdexterityt);
			pRoot->appendChild(pdexterity);
			AutoPtr<XML::Element> pphisical_Damage = pDoc->createElement("phisical_Damage");
			AutoPtr<XML::Text> pphisical_Damaget = pDoc->createTextNode(Poco::NumberFormatter::format(p->phisical_Damage));
			pphisical_Damage->appendChild(pphisical_Damaget);
			pRoot->appendChild(pphisical_Damage);
			AutoPtr<XML::Element> pmagical_Damage = pDoc->createElement("magical_Damage");
			AutoPtr<XML::Text> pmagical_Damaget = pDoc->createTextNode(Poco::NumberFormatter::format(p->magical_Damage));
			pmagical_Damage->appendChild(pmagical_Damaget);
			pRoot->appendChild(pmagical_Damage);
			AutoPtr<XML::Element> pchance_to_Hit = pDoc->createElement("chance_to_Hit");
			AutoPtr<XML::Text> pchance_to_Hitt = pDoc->createTextNode(Poco::NumberFormatter::format(p->chance_to_Hit));
			pchance_to_Hit->appendChild(pchance_to_Hitt);
			pRoot->appendChild(pchance_to_Hit);
			AutoPtr<XML::Element> pchance_to_Cast = pDoc->createElement("chance_to_Cast");
			AutoPtr<XML::Text> pchance_to_Castt = pDoc->createTextNode(Poco::NumberFormatter::format(p->chance_to_Cast));
			pchance_to_Cast->appendChild(pchance_to_Castt);
			pRoot->appendChild(pchance_to_Cast);
			AutoPtr<XML::Element> psuppression = pDoc->createElement("suppression");
			AutoPtr<XML::Text> psuppressiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->suppression));
			psuppression->appendChild(psuppressiont);
			pRoot->appendChild(psuppression);
			AutoPtr<XML::Element> pabsorption = pDoc->createElement("absorption");
			AutoPtr<XML::Text> pabsorptiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->absorption));
			pabsorption->appendChild(pabsorptiont);
			pRoot->appendChild(pabsorption);
			AutoPtr<XML::Element> pHP = pDoc->createElement("HP");
			AutoPtr<XML::Text> pHPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->HP));
			pHP->appendChild(pHPt);
			pRoot->appendChild(pHP);
			AutoPtr<XML::Element> pMP = pDoc->createElement("MP");
			AutoPtr<XML::Text> pMPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->MP));
			pMP->appendChild(pMPt);
			pRoot->appendChild(pMP);
			AutoPtr<XML::Element> pGold = pDoc->createElement("Gold");
			AutoPtr<XML::Text> pGoldt = pDoc->createTextNode(Poco::NumberFormatter::format(p->Gold));
			pGold->appendChild(pGoldt);
			pRoot->appendChild(pGold);
			AutoPtr<XML::Element> pPlatina = pDoc->createElement("Platina");
			AutoPtr<XML::Text> pPlatinat = pDoc->createTextNode(Poco::NumberFormatter::format(p->Platina));
			pPlatina->appendChild(pPlatinat);
			pRoot->appendChild(pPlatina);
			AutoPtr<XML::Element> pSilver = pDoc->createElement("Silver");
			AutoPtr<XML::Text> pSilvert = pDoc->createTextNode(Poco::NumberFormatter::format(p->Silver));
			pSilver->appendChild(pSilvert);
			pRoot->appendChild(pSilver);
			AutoPtr<XML::Element> pExpirence = pDoc->createElement("Expirence");
			AutoPtr<XML::Text> pExpirencet = pDoc->createTextNode(Poco::NumberFormatter::format(p->Expirence));
			pExpirence->appendChild(pExpirencet);
			pRoot->appendChild(pExpirence);
			AutoPtr<XML::Element> prelaxHP = pDoc->createElement("relaxHP");
			AutoPtr<XML::Text> prelaxHPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->relaxHP));
			prelaxHP->appendChild(prelaxHPt);
			pRoot->appendChild(prelaxHP);
			AutoPtr<XML::Element> prelaxMP = pDoc->createElement("relaxMP");
			AutoPtr<XML::Text> prelaxMPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->relaxMP));
			prelaxMP->appendChild(prelaxMPt);
			pRoot->appendChild(prelaxMP);
			AutoPtr<XML::Element> preflection = pDoc->createElement("reflection");
			AutoPtr<XML::Text> preflectiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->reflection));
			preflection->appendChild(preflectiont);
			pRoot->appendChild(preflection);
			AutoPtr<XML::Element> parmor = pDoc->createElement("armor");
			AutoPtr<XML::Text> parmort = pDoc->createTextNode(Poco::NumberFormatter::format(p->armor));
			parmor->appendChild(parmort);
			pRoot->appendChild(parmor);
			AutoPtr<XML::Element> pprotection = pDoc->createElement("protection");
			AutoPtr<XML::Text> pprotectiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->protection));
			pprotection->appendChild(pprotectiont);
			pRoot->appendChild(pprotection);
			AutoPtr<XML::Element> plightingResistance = pDoc->createElement("lightingResistance");
			AutoPtr<XML::Text> plightingResistancet = pDoc->createTextNode(Poco::NumberFormatter::format(p->lightingResistance));
			plightingResistance->appendChild(plightingResistancet);
			pRoot->appendChild(plightingResistance);
			AutoPtr<XML::Element> pfireResistance = pDoc->createElement("fireResistance");
			AutoPtr<XML::Text> pfireResistancet = pDoc->createTextNode(Poco::NumberFormatter::format(p->fireResistance));
			pfireResistance->appendChild(pfireResistancet);
			pRoot->appendChild(pfireResistance);
			AutoPtr<XML::Element> pcoldResistance = pDoc->createElement("coldResistance");
			AutoPtr<XML::Text> pcoldResistancet = pDoc->createTextNode(Poco::NumberFormatter::format(p->coldResistance));
			pcoldResistance->appendChild(pcoldResistancet);
			pRoot->appendChild(pcoldResistance);
			AutoPtr<XML::Element> pmaxHeight = pDoc->createElement("maxHeight");
			AutoPtr<XML::Text> pmaxHeightt = pDoc->createTextNode(Poco::NumberFormatter::format(p->maxHeight));
			pmaxHeight->appendChild(pmaxHeightt);
			pRoot->appendChild(pmaxHeight);
			AutoPtr<XML::Element> pKarma = pDoc->createElement("Karma");
			AutoPtr<XML::Text> pKarmat = pDoc->createTextNode(Poco::NumberFormatter::format(p->Karma));
			pKarma->appendChild(pKarmat);
			pRoot->appendChild(pKarma);
			AutoPtr<XML::Element> piAggressor = pDoc->createElement("iAggressor");
			AutoPtr<XML::Text> piAggressort = pDoc->createTextNode((p->iAggressor == true) ? "true" : "false");
			piAggressor->appendChild(piAggressort);
			pRoot->appendChild(piAggressor);
			AutoPtr<XML::Element> pWorldID = pDoc->createElement("WorldID");
			AutoPtr<XML::Text> pWorldIDt = pDoc->createTextNode(p->WorldID);
			pWorldID->appendChild(pWorldIDt);
			pRoot->appendChild(pWorldID);

		AutoPtr<XML::Element> close = pDoc->createElement("close");
		pRoot->appendChild(close);
		toString(pDoc);
	}
	void RemoveNPC(CNpc* p){
		AutoPtr<XML::Document> pDoc = new XML::Document;
		AutoPtr<XML::Element> pRoot = pDoc->createElement("root");
		pDoc->appendChild(pRoot);
		AutoPtr<XML::Element> command = pDoc->createElement("command");
		AutoPtr<XML::Text> pcommandt = pDoc->createTextNode(Poco::NumberFormatter::format(Command::AddNPC));
		command->appendChild(pcommandt);
		pRoot->appendChild(command);
		AutoPtr<XML::Element> id = pDoc->createElement("id");
		AutoPtr<XML::Text> idt = pDoc->createTextNode(p->id);
		id->appendChild(idt);
		pRoot->appendChild(id);
		toString(pDoc);
	}
	void Prompt(Player* p){
		
	}
	void AboutMe(Player* p){
		Poco::AutoPtr<XML::Document> pDoc = new Poco::XML::Document;
		Poco::AutoPtr<Poco::XML::Element> pRoot = pDoc->createElement("root");
		pDoc->appendChild(pRoot);
		AutoPtr<XML::Element> command = pDoc->createElement("command");
		AutoPtr<XML::Text> pcommandt = pDoc->createTextNode(Poco::NumberFormatter::format(TCommand::_AboutMe));
		command->appendChild(pcommandt);
		pRoot->appendChild(command);
		AutoPtr<XML::Element> id = pDoc->createElement("id");
		AutoPtr<XML::Text> idt = pDoc->createTextNode(p->id);
		id->appendChild(idt);
		pRoot->appendChild(id);
		AutoPtr<XML::Element> pCurCell = pDoc->createElement("CurCell");
		AutoPtr<XML::Text> pCurCellt = pDoc->createTextNode(p->CurCell);
		pCurCell->appendChild(pCurCellt);
		pRoot->appendChild(pCurCell);
		AutoPtr<XML::Element> pName = pDoc->createElement("Name");
		AutoPtr<XML::Text> pNamet = pDoc->createTextNode(getLangString(p->Lang, p->Name, "Name"));
		pName->appendChild(pNamet);
		pRoot->appendChild(pName);
		AutoPtr<XML::Element> pusername = pDoc->createElement("username");
		AutoPtr<XML::Text> pusernamet = pDoc->createTextNode(p->username);
		pusername->appendChild(pusernamet);
		pRoot->appendChild(pusername);
		AutoPtr<XML::Element> ppassword = pDoc->createElement("password");
		AutoPtr<XML::Text> ppasswordt = pDoc->createTextNode(p->password);
		ppassword->appendChild(ppasswordt);
		pRoot->appendChild(ppassword);
		AutoPtr<XML::Element> pemail = pDoc->createElement("email");
		AutoPtr<XML::Text> pemailt = pDoc->createTextNode(p->email);
		pemail->appendChild(pemailt);
		pRoot->appendChild(pemail);
		AutoPtr<XML::Element> plogged = pDoc->createElement("logged");
		AutoPtr<XML::Text> ploggedt = pDoc->createTextNode(Poco::NumberFormatter::format(p->logged));
		plogged->appendChild(ploggedt);
		pRoot->appendChild(plogged);
		AutoPtr<XML::Element> pbanned = pDoc->createElement("banned");
		AutoPtr<XML::Text> pbannedt = pDoc->createTextNode(Poco::NumberFormatter::format(p->banned));
		pbanned->appendChild(pbannedt);
		pRoot->appendChild(pbanned);
		AutoPtr<XML::Element> pAge = pDoc->createElement("Age");
		AutoPtr<XML::Text> pAget = pDoc->createTextNode(Poco::NumberFormatter::format(p->Age));
		pAge->appendChild(pAget);
		pRoot->appendChild(pAge);
		AutoPtr<XML::Element> pDescription = pDoc->createElement("Description");
		AutoPtr<XML::Text> pDescriptiont = pDoc->createTextNode(getLangString(p->Lang, p->Description,"Description"));
		pDescription->appendChild(pDescriptiont);
		pRoot->appendChild(pDescription);
		AutoPtr<XML::Element> pDateBorn = pDoc->createElement("DateBorn");
		AutoPtr<XML::Text> pDateBornt = pDoc->createTextNode(DateTimeFormatter::format(p->DateBorn, "%e %b %Y %H:%M"));
		pDateBorn->appendChild(pDateBornt);
		pRoot->appendChild(pDateBorn);
		AutoPtr<XML::Element> pDateDied = pDoc->createElement("DateDied");
		AutoPtr<XML::Text> pDateDiedt = pDoc->createTextNode(DateTimeFormatter::format(p->DateDied, "%e %b %Y %H:%M"));
		pDateDied->appendChild(pDateDiedt);
		pRoot->appendChild(pDateDied);
		AutoPtr<XML::Element> pImgId = pDoc->createElement("ImgId");
		AutoPtr<XML::Text> pImgIdt = pDoc->createTextNode(p->ImgId);
		pImgId->appendChild(pImgIdt);
		pRoot->appendChild(pImgId);
		AutoPtr<XML::Element> pLevel = pDoc->createElement("Level");
		AutoPtr<XML::Text> pLevelt = pDoc->createTextNode(Poco::NumberFormatter::format(p->Level));
		pLevel->appendChild(pLevelt);
		pRoot->appendChild(pLevel);
		AutoPtr<XML::Element> pstrenght = pDoc->createElement("strenght");
		AutoPtr<XML::Text> pstrenghtt = pDoc->createTextNode(Poco::NumberFormatter::format(p->strenght));
		pstrenght->appendChild(pstrenghtt);
		pRoot->appendChild(pstrenght);
		AutoPtr<XML::Element> pbody = pDoc->createElement("body");
		AutoPtr<XML::Text> pbodyt = pDoc->createTextNode(Poco::NumberFormatter::format(p->body));
		pbody->appendChild(pbodyt);
		pRoot->appendChild(pbody);
		AutoPtr<XML::Element> pintillect = pDoc->createElement("intillect");
		AutoPtr<XML::Text> pintillectt = pDoc->createTextNode(Poco::NumberFormatter::format(p->intillect));
		pintillect->appendChild(pintillectt);
		pRoot->appendChild(pintillect);
		AutoPtr<XML::Element> pdexterity = pDoc->createElement("dexterity");
		AutoPtr<XML::Text> pdexterityt = pDoc->createTextNode(Poco::NumberFormatter::format(p->dexterity));
		pdexterity->appendChild(pdexterityt);
		pRoot->appendChild(pdexterity);
		AutoPtr<XML::Element> pphisical_Damage = pDoc->createElement("phisical_Damage");
		AutoPtr<XML::Text> pphisical_Damaget = pDoc->createTextNode(Poco::NumberFormatter::format(p->phisical_Damage));
		pdexterity->appendChild(pphisical_Damaget);
		pRoot->appendChild(pphisical_Damage);
		AutoPtr<XML::Element> pmagical_Damage = pDoc->createElement("magical_Damage");
		AutoPtr<XML::Text> pmagical_Damaget = pDoc->createTextNode(Poco::NumberFormatter::format(p->magical_Damage));
		pmagical_Damage->appendChild(pmagical_Damaget);
		pRoot->appendChild(pmagical_Damage);
		AutoPtr<XML::Element> pchance_to_Hit = pDoc->createElement("chance_to_Hit");
		AutoPtr<XML::Text> pchance_to_Hitt = pDoc->createTextNode(Poco::NumberFormatter::format(p->chance_to_Hit));
		pchance_to_Hit->appendChild(pchance_to_Hitt);
		pRoot->appendChild(pchance_to_Hit);
		AutoPtr<XML::Element> pchance_to_Cast = pDoc->createElement("chance_to_Cast");
		AutoPtr<XML::Text> pchance_to_Castt = pDoc->createTextNode(Poco::NumberFormatter::format(p->chance_to_Cast));
		pchance_to_Cast->appendChild(pchance_to_Castt);
		pRoot->appendChild(pchance_to_Cast);
		AutoPtr<XML::Element> psuppression = pDoc->createElement("suppression");
		AutoPtr<XML::Text> psuppressiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->suppression));
		psuppression->appendChild(psuppressiont);
		pRoot->appendChild(psuppression);
		AutoPtr<XML::Element> pabsorption = pDoc->createElement("absorption");
		AutoPtr<XML::Text> pabsorptiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->absorption));
		pabsorption->appendChild(pabsorptiont);
		pRoot->appendChild(pabsorption);
		AutoPtr<XML::Element> pHP = pDoc->createElement("HP");
		AutoPtr<XML::Text> pHPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->HP));
		pHP->appendChild(pHPt);
		pRoot->appendChild(pHP);
		AutoPtr<XML::Element> pMP = pDoc->createElement("MP");
		AutoPtr<XML::Text> pMPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->MP));
		pMP->appendChild(pMPt);
		pRoot->appendChild(pMP);
		AutoPtr<XML::Element> pGold = pDoc->createElement("Gold");
		AutoPtr<XML::Text> pGoldt = pDoc->createTextNode(Poco::NumberFormatter::format(p->Gold));
		pGold->appendChild(pGoldt);
		pRoot->appendChild(pGold);
		AutoPtr<XML::Element> pPlatina = pDoc->createElement("Platina");
		AutoPtr<XML::Text> pPlatinat = pDoc->createTextNode(Poco::NumberFormatter::format(p->Platina));
		pPlatina->appendChild(pPlatinat);
		pRoot->appendChild(pPlatina);
		AutoPtr<XML::Element> pSilver = pDoc->createElement("Silver");
		AutoPtr<XML::Text> pSilvert = pDoc->createTextNode(Poco::NumberFormatter::format(p->Silver));
		pSilver->appendChild(pSilvert);
		pRoot->appendChild(pSilver);
		AutoPtr<XML::Element> pExpirence = pDoc->createElement("Expirence");
		AutoPtr<XML::Text> pExpirencet = pDoc->createTextNode(Poco::NumberFormatter::format(p->Expirence));
		pExpirence->appendChild(pExpirencet);
		pRoot->appendChild(pExpirence);
		AutoPtr<XML::Element> prelaxHP = pDoc->createElement("relaxHP");
		AutoPtr<XML::Text> prelaxHPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->relaxHP));
		prelaxHP->appendChild(prelaxHPt);
		pRoot->appendChild(prelaxHP);
		AutoPtr<XML::Element> prelaxMP = pDoc->createElement("relaxMP");
		AutoPtr<XML::Text> prelaxMPt = pDoc->createTextNode(Poco::NumberFormatter::format(p->relaxMP));
		prelaxMP->appendChild(prelaxMPt);
		pRoot->appendChild(prelaxMP);
		AutoPtr<XML::Element> preflection = pDoc->createElement("reflection");
		AutoPtr<XML::Text> preflectiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->reflection));
		preflection->appendChild(preflectiont);
		pRoot->appendChild(preflection);
		AutoPtr<XML::Element> parmor = pDoc->createElement("armor");
		AutoPtr<XML::Text> parmort = pDoc->createTextNode(Poco::NumberFormatter::format(p->armor));
		parmor->appendChild(parmort);
		pRoot->appendChild(parmor);
		AutoPtr<XML::Element> pprotection = pDoc->createElement("protection");
		AutoPtr<XML::Text> pprotectiont = pDoc->createTextNode(Poco::NumberFormatter::format(p->protection));
		pprotection->appendChild(pprotectiont);
		pRoot->appendChild(pprotection);
		AutoPtr<XML::Element> plightingResistance = pDoc->createElement("lightingResistance");
		AutoPtr<XML::Text> plightingResistancet = pDoc->createTextNode(Poco::NumberFormatter::format(p->lightingResistance));
		plightingResistance->appendChild(plightingResistancet);
		pRoot->appendChild(plightingResistance);
		AutoPtr<XML::Element> pfireResistance = pDoc->createElement("fireResistance");
		AutoPtr<XML::Text> pfireResistancet = pDoc->createTextNode(Poco::NumberFormatter::format(p->fireResistance));
		pfireResistance->appendChild(pfireResistancet);
		pRoot->appendChild(pfireResistance);
		AutoPtr<XML::Element> pcoldResistance = pDoc->createElement("coldResistance");
		AutoPtr<XML::Text> pcoldResistancet = pDoc->createTextNode(Poco::NumberFormatter::format(p->coldResistance));
		pcoldResistance->appendChild(pcoldResistancet);
		pRoot->appendChild(pcoldResistance);
		AutoPtr<XML::Element> pmaxHeight = pDoc->createElement("maxHeight");
		AutoPtr<XML::Text> pmaxHeightt = pDoc->createTextNode(Poco::NumberFormatter::format(p->maxHeight));
		pmaxHeight->appendChild(pmaxHeightt);
		pRoot->appendChild(pmaxHeight);
		AutoPtr<XML::Element> pKarma = pDoc->createElement("Karma");
		AutoPtr<XML::Text> pKarmat = pDoc->createTextNode(Poco::NumberFormatter::format(p->Karma));
		pKarma->appendChild(pKarmat);
		pRoot->appendChild(pKarma);
		AutoPtr<XML::Element> piPlayer = pDoc->createElement("iPlayer");
		AutoPtr<XML::Text> piPlayert = pDoc->createTextNode(Poco::NumberFormatter::format(p->iPlayer));
		piPlayer->appendChild(piPlayert);
		pRoot->appendChild(piPlayer);
		AutoPtr<XML::Element> piAggressor = pDoc->createElement("iAggressor");
		AutoPtr<XML::Text> piAggressort = pDoc->createTextNode(Poco::NumberFormatter::format(p->iAggressor));
		piAggressor->appendChild(piAggressort);
		pRoot->appendChild(piAggressor);
		AutoPtr<XML::Element> pkilled = pDoc->createElement("killed");
		AutoPtr<XML::Text> pkilledt = pDoc->createTextNode(Poco::NumberFormatter::format(p->killed));
		pkilled->appendChild(pkilledt);
		pRoot->appendChild(pkilled);
		AutoPtr<XML::Element> pIDCellPropiska = pDoc->createElement("IDCellPropiska");
		AutoPtr<XML::Text> pIDCellPropiskat = pDoc->createTextNode(p->IDCellPropiska);
		pIDCellPropiska->appendChild(pIDCellPropiskat);
		pRoot->appendChild(pIDCellPropiska);
		AutoPtr<XML::Element> pWorldID = pDoc->createElement("WorldID");
		AutoPtr<XML::Text> pWorldIDt = pDoc->createTextNode(p->WorldID);
		pWorldID->appendChild(pWorldIDt);
		pRoot->appendChild(pWorldID);
		AutoPtr<XML::Element> close = pDoc->createElement("close");
		pRoot->appendChild(close);
		toString(pDoc);
	}
	~MessageCommand(){
	}

private:
	void toString(AutoPtr<XML::Document> pDoc){
		std::stringstream ws;
		Poco::UTF8Encoding utf8encoding;
		XML::DOMWriter writer;
		writer.setOptions(XML::XMLWriter::CANONICAL | XML::XMLWriter::PRETTY_PRINT);
		writer.setEncoding("UTF-8", utf8encoding);
		writer.writeNode(ws, pDoc);
		XML::XMLString st = ws.str();
		//Message = XML::fromXMLString(st);
		std::stringstream inputstream;
		std::stringstream outputstream;
		inputstream << XML::fromXMLString(st);
		DeflatingOutputStream deflater(outputstream, DeflatingStreamBuf::STREAM_ZLIB, 9);
		StreamCopier::copyStream(inputstream, deflater);
		deflater.close();
		
		
		Message = outputstream.str();
	}
};