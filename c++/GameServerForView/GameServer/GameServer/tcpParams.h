#pragma once
#include "Poco/Net/TCPServerParams.h"
#include "DBGameService.h"

class tcpParams : public Poco::Net::TCPServerParams {
public:
	DBGameService::Ptr pService;
	tcpParams(DBGameService::Ptr _pService){ this->pService = _pService; }
	~tcpParams(){}
};