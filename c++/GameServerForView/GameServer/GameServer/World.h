#pragma once
#include <map>
#include <string>
#include "Poco/Net/Socket.h"
#include "Poco/SharedPtr.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/OSP/BundleContext.h"
#include "PulsarWorldTimer.h"
#include "DBGameService.h"
#include "Worlds.h"
#include "Poco/MongoDB/ObjectId.h"

//#include "DBGameService.h"


using namespace std;


//std::vector<Worlds*> World;

class CWorld
{
public:
	CWorld();
	CWorld(Poco::OSP::BundleContext::Ptr _pContext);
	~CWorld();
public:
	void Start();
public:
	map<string, Poco::Net::StreamSocket*> PlayerMap;
	Poco::SharedPtr<CPulsarWorldTimer> cpwt;
	DBGameService::Ptr pService;
	Poco::OSP::BundleContext::Ptr pContext;
	Poco::SharedPtr<map<string, Poco::SharedPtr<Worlds>>> WorldsMap;
private:
	void LoadWorlds();
	void LoadMap();
	void LoadNPC();
	void LoadItems();
	void PulsarTimerStart();
	void reinitializate();
	Poco::SharedPtr<map<string, Poco::SharedPtr<Worlds>>> parse();
};