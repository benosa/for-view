#include "AdminControllerHandler.h"
#include "PlayerController.h"



bool AdminControllerHandler::sendBytes(const char bytes[], int bytesToWrite) {
	int tmp = 0;
	try {
		while (bytesToWrite > tmp) {
			//if (!socket->isNull())
			tmp += socket.sendBytes(bytes + tmp, bytesToWrite - tmp);
		}
	}
	catch (Poco::Exception& error) {
		std::cout << "recv failed (Error: " << error.displayText() << ')' << std::endl;
		socket.shutdownSend();
		//delete this;
		return false;
	}
	return true;
}

AdminControllerHandler::AdminControllerHandler(Poco::Net::StreamSocket& _socket, CWorld* _World, DBGameService::Ptr _pService, BundleContext::Ptr _pContext, Player* _p) : socket(_socket)
{
	World = _World;
	pService = _pService;
	pContext = _pContext;
	p = _p;
}


AdminControllerHandler::~AdminControllerHandler()
{
}

void AdminControllerHandler::UpdateEvent(){
	if (CombatArray.size() == 0){
		this->MoveUpdate();
	}
	else this->CombatUpdate();
}

void AdminControllerHandler::SetCombatPlayer(Player* RemotePlayer){
	CombatArray.push_back(RemotePlayer);
}

void AdminControllerHandler::MoveUpdate(){
	if (MoveArray.size() > 0){
		pfn_MyFuncType p = MoveArray.front();
		((*this).*p)();
		MoveArray.pop();
	}
}

void AdminControllerHandler::CombatUpdate(){
	if (CombatArray.size() > 0){
		for (int i = 0; i < CombatArray.size(); i++){
			//CombatArray[i]->
			//pfn_MyFuncType p = CombatArray.front();
			//((*this).*CombatArray)();
			//MoveArray.pop();
			class Params;
			Params* p1;
			Params* p2;
		}

	}
}

void AdminControllerHandler::Worker(ReceiveCommand& rc){
	ParseCommand(rc);
}


/*
����� ������� - �������!
���������, ������� ������� - �����.
*/

/*enum Command{
	East = 0x0101,
	West = 0x0102,
	North = 0x0103,
	South = 0x0104,
	Up = 0x0105,
	Down = 0x0106,
	Look = 0x0107,
	AboutMe = 0x0108,
	AddNPC = 0x0109,
	RemoveNPC = 0x010A,
	Killnpc = 0x010B,
	Killplayer = 0x010C,
	reinitializate = 0x010D
};*/

void AdminControllerHandler::ParseCommand(ReceiveCommand& rc){
	switch (rc.command)
	{
		/*Move*/
		case Command::West:
			MoveArray.push(&AdminControllerHandler::West);
		case Command::East:
			MoveArray.push(&AdminControllerHandler::East);
		case Command::South:
			MoveArray.push(&AdminControllerHandler::South);
		case Command::North:
			MoveArray.push(&AdminControllerHandler::North);
		case Command::Up:
			MoveArray.push(&AdminControllerHandler::Up);
		case Command::Down:
			MoveArray.push(&AdminControllerHandler::Down);
		case Command::Look:
			this->Look();
		case Command::AboutMe:
			this->AboutMe();
		/*Combat*/
		case Command::Killnpc:
			CombatArray.push_back(p->CurrentCell->NPCMap[rc.id]);
		case Command::Killplayer:
			CombatArray.push_back(p->CurrentCell->PlayersMap[rc.id]);
			CPlayerController* pcdt = p->CurrentCell->PlayersMap[rc.id]->pController;
			pcdt->IPlayerControllerHandler->SetCombatPlayer(p);
	//default:
	//	break;
	}
}

void AdminControllerHandler::AboutMe(){
	pContext->logger().information("i am looking around\n");
	MessageCommand cmd;
	cmd.AboutMe(p);
	sendBytes(cmd.Message.data(), cmd.Message.size());
}

void AdminControllerHandler::Look(){
	/*
	1) ���������� ������� � ������
	2) ���������� ��� � ������
	3) ���������� ��� � ������
	*/
	pContext->logger().information("i am looking around\n");
	MessageCommand cmd;
	cmd.Look(p);
	//cmd.Message;
	int tt = cmd.Message.size();
	sendBytes(cmd.Message.data(), cmd.Message.size());
}
void AdminControllerHandler::Up(){
	if (p->CurrentCell->Up != "000000000000000000000000"){
		pContext->logger().information("i go to Up\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->Up];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void AdminControllerHandler::Down(){
	if (p->CurrentCell->Down != "000000000000000000000000"){
		pContext->logger().information("i go to Down\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->Down];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void AdminControllerHandler::North(){
	if (p->CurrentCell->North != "000000000000000000000000"){
		pContext->logger().information("i go to North\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->North];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void AdminControllerHandler::South(){
	if (p->CurrentCell->South != "000000000000000000000000"){
		pContext->logger().information("i go to South\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->South];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void AdminControllerHandler::West(){
	if (p->CurrentCell->West != "000000000000000000000000"){
		pContext->logger().information("i go to west\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->West];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}
void AdminControllerHandler::East(){
	if (p->CurrentCell->East != "000000000000000000000000"){
		pContext->logger().information("i go to East\n");
		CCell* CurCel = p->CurrentCell;
		CCell* NextCell = p->CurrentWorld->Cells[p->CurrentCell->East];
		CurCel->RemovePlayer(p);
		NextCell->InjectPlayer(p);
		p->CurrentCell = NextCell;
		this->Look();
	}
}

void AdminControllerHandler::Kill(){

}
void AdminControllerHandler::Open(){}

