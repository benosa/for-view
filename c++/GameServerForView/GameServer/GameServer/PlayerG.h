#pragma once
#pragma once
#include <vector>
#include "Corps.h"
//#include "Cell.h"

class CCell;
class CPlayer
{
public:
	CPlayer();
	~CPlayer();
	//wxString CPlayrGet();
public:
	//wxSocketBase* sock;
	Poco::SharedPtr<CCell> CurrentCell;
	std::string CurCell;
	std::string id;
	std::string Name;
	int Age;
	std::string Description;
	int DateBorn;
	int DateDied;
	std::string ImgId;
	int Level;
	int strenght;
	int body;
	int intillect;
	int phisical_Damage;
	int magical_Damage;
	int chance_to_Hit;
	int chance_to_Cast;
	int suppression;
	int absorption;
	int HP;
	int MP;
	int Gold;
	int Platina;
	int Silver;
	int Expirence;
	int relaxHP;
	int relaxMP;
	int reflection;
	int armor;
	int protection;
	int lightingResistance;
	int fireResistance;
	int coldResistance;
	int maxHeight;
	int Karma;
	bool iPlayer;
	bool iAggressor;
	bool killed;
	int IDCellPropiska;
	std::string LocationID;
	std::vector<CCorps> CorpsArray;
};
