#pragma once
#include <string>
#include "Poco/Types.h"

class LoginData
{
public:

	std::string Login;
	std::string Password;
	LoginData(unsigned char* data){
		//unpack login, password size string
		Poco::UInt32 size_login;
		memcpy(&size_login, data, sizeof(Poco::UInt32));
		Poco::UInt32 size_password;
		memcpy(&size_password, data + sizeof(Poco::UInt32), sizeof(Poco::UInt32));
		//unpack login string
		login = new char[size_login + 1];
		login[size_login] = '\0';
		memcpy(login, data + sizeof(Poco::UInt32) * 2, size_login);
		Login = login;
		//std::string tmpstrl(login);
		delete[] login;
		//unpack password string
		password = new char[size_password + 1];
		password[size_password] = '\0';
		memcpy(password, data + sizeof(Poco::UInt32) * 2 + size_login, size_password);
		Password = password;
		//std::string tmpstrp(password);
		delete[] password;
	}
	~LoginData(){};

private:
	char* login;
	char* password;
};
