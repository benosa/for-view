#include "newConnectionLoginServer.h"
newConnection::newConnection(const Poco::Net::StreamSocket& s, DBGameService::Ptr _pService, BundleContext::Ptr _pContext) :
Poco::Net::TCPServerConnection(s), ss(s) {
	pContext = _pContext;
	pService = _pService;
}


newConnection::~newConnection(){
	pContext->logger().notice("Connection for Login Server ------------------------  finished!");
}
void newConnection::run() {
/*#if defined(_MSC_VER)
	// Turn on memory leak detection (use _CrtSetBreakAlloc to break at specific allocation)
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);

	//_CrtSetBreakAlloc(6150);
#endif*/
	std::string peerAddr = socket().peerAddress().toString();
	pContext->logger().information("New connection from." + peerAddr);

	bool isOpen = true;
	Poco::Timespan timeOut(10, 0);
	UCHAR size_of_data[4];
	unsigned char* incommingBuffer;
	while (isOpen){
		if (socket().poll(timeOut, Poco::Net::Socket::SELECT_READ) == false){
			cout << "TIMEOUT!" << endl << flush;
		}
		else{
			cout << "RX EVENT!!! ---> " << flush;
			int nBytes = -1;
			try {
				nBytes = socket().receiveBytes(size_of_data, sizeof(size_of_data));
				Poco::UInt32 size = *(Poco::UInt32*)size_of_data;
				//����� ���� ������ - ��-�� �������� �������� ���� �� ������� - ������ ����� �������.
				incommingBuffer = new unsigned char[size];
				nBytes = socket().receiveBytes(incommingBuffer, size);
				LoginData ld(incommingBuffer);
				delete[] incommingBuffer;
				Poco::SharedPtr<CLPlayer> p = Poco::SharedPtr<CLPlayer>(new CLPlayer());
				//Deprecated
				Document::Ptr pDocTmp = pService->getUser(ld.Login, 0, 1);
				if (!pDocTmp.isNull()){
					p->parse(pDocTmp);
				}
				else p = NULL;
				/*if login not found*/
				if (p.isNull()){
					isOpen = false;
					pContext->logger().information("Connection from: " + peerAddr + " error - login not found");
					RequestCommand r('\x02', "", "", '\x01');
					socket().sendBytes(&r, r.getSize());
					socket().close();
					/* if login found */
				}
				else{
					/*if password not match*/;
					if (p->password != ld.Password){
						isOpen = false;
						pContext->logger().information("Connection from: " + peerAddr + " error -  password not correct");
						RequestCommand r('\x02', "", "", '\x02');
						socket().sendBytes(&r, r.getSize());
						socket().close();
						/* if user banned */
					}
					else if (p->banned){
						isOpen = false;
						pContext->logger().information("Connection from: " + peerAddr + " error -  Player is banned");
						RequestCommand r('\x02', "", "", '\x03');
						socket().sendBytes(&r, r.getSize());
						socket().close();
						/* All ok - welcome user */
					}
					else{
						//std::string _strm = string(socket().peerAddress().toString().c_str());
						std::string tmpstr = "Connection from: OK " + peerAddr + "-  Player is alive";
						pContext->logger().information(tmpstr);
						pService->setUserLoggedIn(ld.Login);
						Poco::MongoDB::Document::Ptr bs = pService->getBalanceServer();
						RequestCommand r(bs, '\x01', '\x00', p->id);
						//delete bs;
						Poco::UInt32 tmp = r.getSize();
						Poco::SharedPtr<unsigned char> ch = (r.getData());
						socket().sendBytes(&tmp, sizeof(Poco::UInt32));
						socket().sendBytes(ch, tmp);
						socket().close();
						isOpen = false;
					}
				}
			}
			catch (Poco::Net::NetException& exc) {
				//Handle your network errors.
				cerr << "Network error: " << exc.message() << endl;
				isOpen = false;
			}
			if (nBytes == 0){
				std::cout << "Client closes connection!" << endl << flush;
				isOpen = false;
			}
			else{
				//cout << "Receiving nBytes: " << nBytes+sizeof(int) << endl << flush;
			}
		}
	}
	//std::cout << "Connection finished!" << endl << flush;
}
