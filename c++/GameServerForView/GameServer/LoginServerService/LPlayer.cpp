#include "LPlayer.h"


CLPlayer::CLPlayer()
{
}


CLPlayer::~CLPlayer()
{
}

void CLPlayer::parse(Document::Ptr pdoc){
	this->id = pdoc->get("_id")->toString();
	this->username = pdoc->get<std::string>("username");
	this->password = pdoc->get<std::string>("password");
	this->email = pdoc->get<std::string>("email");
	this->logged = pdoc->get<bool>("logged");
	this->banned = pdoc->get<bool>("banned");
}