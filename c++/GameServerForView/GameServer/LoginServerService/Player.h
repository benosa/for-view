#pragma once
#include <string>

using namespace std;

struct Player
{
	string username;
	string password;
	string email;
	bool banned;
	bool logged;
};