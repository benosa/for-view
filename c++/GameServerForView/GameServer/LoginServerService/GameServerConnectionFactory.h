#pragma once
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/Net/StreamSocket.h"
#include "DBGameService.h"
#include "newConnectionLoginServer.h"

using Poco::OSP::BundleContext;

class GameServerConnectionFactory : public Poco::Net::TCPServerConnectionFactory
	/// A factory for TimeServerConnection.
{
public:
	GameServerConnectionFactory(DBGameService::Ptr _pService, BundleContext::Ptr _pContext) :
		pService(_pService), pContext(_pContext)
	{
	}

	Poco::Net::TCPServerConnection* createConnection(const Poco::Net::StreamSocket& socket)
	{
		return new newConnection(socket, pService, pContext);
	}

private:
	std::string _format;
	DBGameService::Ptr pService;
	BundleContext::Ptr pContext;
};