#pragma once
#include "Poco/Activity.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServer.h"
#include "DBGameService.h"
#include "GameServerConnectionFactory.h"
using namespace Poco::OSP;

class LoginServer
{
	public:
		LoginServer();
		void start();
		void stop();
		DBGameService::Ptr pService;
		BundleContext::Ptr pContext;
	protected:
		void runActivity();
	private:
		Poco::Activity<LoginServer> _activity;
};
