#pragma once
#include <stdint.h>
#include "Poco/NumberParser.h"
#include "Poco/NumberFormatter.h"
#include "Poco/StringTokenizer.h"
#include "Poco/String.h" // for cat
#include "Poco/SharedPtr.h"
#include "Poco/MongoDB/Cursor.h"
#include "BalanceServer.h"

using Poco::StringTokenizer;
using Poco::cat;

using Poco::NumberFormatter;

using Poco::NumberParser;
using Poco::StringTokenizer;

/*
command:
0x01 - ok, reconnect to game;
0x02 - error.
error:
0x00 - not error;
0x01 - login not found;
0x02 - login or password incorrect;
0x03 - user is banned.
*/


struct ip_addr{
	char A;
	char B;
	char C;
	char D;
	void setIP(std::string str){
		StringTokenizer t4(str, ".",
			StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
		A = NumberParser::parse(t4[0]);
		B = NumberParser::parse(t4[1]);
		C = NumberParser::parse(t4[2]);
		D = NumberParser::parse(t4[3]);
	}
	Poco::UInt32 getIPUInt32(){
		return *(Poco::UInt32*)this;
	}
	std::string getStringFromIP(Poco::UInt32 ip){
		D = (ip >> 24) & 0xFF;
		C = (ip >> 16) & 0xFF;
		B = (ip >> 8) & 0xFF;
		A = ip & 0xFF;
		std::string s, s1, s2, s3, s4;
		s1.append((A != 0) ? std::to_string(A) : "0");
		s2.append((B != '\0') ? std::to_string(B) : "0");
		s3.append((C != '\0') ? std::to_string(C) : "0");
		s4.append((D != '\0') ? std::to_string(D) : "0");
		s += s1 + "." + s2 + "." + s3 + "." + s4;

		return s;
	}
};
class RequestCommand
{
public:
	char command;
	Poco::UInt32 ip;
	Poco::UInt32 port;
	char error;
	char id[25];
public:
	RequestCommand(){};
	RequestCommand(unsigned char* buffer, int size){
		if (size == this->getSize()){
			memcpy(&command, buffer, sizeof(char));
			memcpy(&ip, buffer + sizeof(char), sizeof(Poco::UInt32));
			memcpy(&port, buffer + sizeof(char) + sizeof(Poco::UInt32), sizeof(Poco::UInt32));
			memcpy(&error, buffer + sizeof(char) + sizeof(Poco::UInt32) + sizeof(Poco::UInt32), sizeof(char));
			memcpy(&id, buffer + sizeof(char) + sizeof(Poco::UInt32) + sizeof(Poco::UInt32) + sizeof(char), sizeof(char[25]));
		}
	}
	RequestCommand(char _command, std::string _ip, std::string _port, char _error, std::string _id = "000000000000000000000000"){
		ip_addr _ip_;
		command = _command;
		if (_ip.size() > 0 && _port.size() > 0){
			_ip_.setIP(_ip);
			ip = _ip_.getIPUInt32();
			port = NumberParser::parse(_port);
		}
		
		error = _error;
		memcpy(id, _id.c_str(), sizeof(char[25]));
	}
	RequestCommand(Poco::MongoDB::Document::Ptr ptr, char _command, char _error, std::string _id = "000000000000000000000000"){
		Poco::SharedPtr<BalanceServer> bs = NULL;
		Poco::SharedPtr<BalanceServer> p(new BalanceServer());
		bs.assign(p);
		bs->ip = ptr->get<std::string>("host");
		bs->port = ptr->get<std::string>("port");
		ip_addr _ip_;
		command = _command;
		if (bs->ip.size() > 0 && bs->port.size() > 0){
			_ip_.setIP(bs->ip);
			ip = _ip_.getIPUInt32();
			port = NumberParser::parse(bs->port);
		}

		error = _error;
		memcpy(id, _id.c_str(), sizeof(char[25]));
	}
	~RequestCommand(){
		//delete ch;
	}
	Poco::Int32 getSize(){
		Poco::Int32 i = sizeof(char) + sizeof(Poco::UInt32) + sizeof(Poco::UInt32) + sizeof(char) + sizeof(char[25]);
		return i;
	}
	Poco::SharedPtr<unsigned char> getData(){
		Poco::SharedPtr<unsigned char> p(new unsigned char[35]);
		Poco::SharedPtr<unsigned char> ch;
		ch.assign(p);
		memcpy(ch, &command, sizeof(char));
		memcpy(ch + sizeof(char), &ip, sizeof(Poco::UInt32));
		memcpy(ch + sizeof(char) + sizeof(Poco::UInt32), &port, sizeof(Poco::UInt32));
		memcpy(ch + sizeof(char) + sizeof(Poco::UInt32) + sizeof(Poco::UInt32), &error, sizeof(char));
		memcpy(ch + sizeof(char) + sizeof(Poco::UInt32) + sizeof(Poco::UInt32) + sizeof(char), &id, sizeof(char[25]));
		return ch;
	}
	std::string IpToString(){
		ip_addr _ip_;
		std::string str = _ip_.getStringFromIP(ip);
		return str;
	}
	std::string PortToString(){
		return NumberFormatter::format(port);
	}
};