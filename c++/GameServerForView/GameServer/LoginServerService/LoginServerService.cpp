#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"

#include "Poco/OSP/BundleActivator.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/OSP/ServiceRegistry.h"
#include "Poco/ClassLibrary.h"


#include "DBGameService.h"

#include "LoginServer.h"

using Poco::OSP::BundleActivator;
using Poco::OSP::BundleContext;
using Poco::OSP::ServiceRef;
using Poco::OSP::Service;
using Poco::Thread;
bool serverloop;
class LoginServerBundleActivator: public BundleActivator
{
	public:
		LoginServer example;
		LoginServerBundleActivator()
		{
			
		}
	
		~LoginServerBundleActivator()
		{
			serverloop = false;
			example.stop();
		}
		
		
		void start(BundleContext::Ptr pContext)
		{
			// Obtain the GreetingService object from the Service Registry.
			ServiceRef::Ptr pServiceRef = pContext->registry().findByName("com.InstitutIt.GameServer.DBGameService");
			if (pServiceRef)
			{
				DBGameService::Ptr pService = pServiceRef->castedInstance<DBGameService>();
				example.pService = pService;
				example.pContext = pContext;
				std::string msg("****** ");
				msg += " ******";
				example.start();
			}
			else
			{
				pContext->logger().error("The LoginServerService is not available.");
			}
		}
		
		void stop(BundleContext::Ptr pContext)
		{
			serverloop = false;
			example.stop();
		}
};


POCO_BEGIN_MANIFEST(BundleActivator)
	POCO_EXPORT_CLASS(LoginServerBundleActivator)
POCO_END_MANIFEST
