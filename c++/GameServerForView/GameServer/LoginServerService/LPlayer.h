#pragma once
#include <string>
#include "Poco/SharedPtr.h"
#include "Poco/MongoDB/Document.h"


using namespace std;
using namespace Poco::MongoDB;

class CLPlayer
{
public:
	CLPlayer();
	~CLPlayer();
	void parse(Document::Ptr);
	std::string username;
	std::string password;
	std::string email;
	std::string id;
	bool logged;
	bool banned;
};