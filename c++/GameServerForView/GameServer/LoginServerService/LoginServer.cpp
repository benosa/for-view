#include "LoginServer.h"
extern bool serverloop;
inline unsigned int ip_to_int (const char * ip)
{

    /* The return value. */
    unsigned v = 0;
    /* The count of the number of bytes processed. */
    int i;
    /* A pointer to the next digit to process. */
    const char * start;

    start = ip;
    for (i = 0; i < 4; i++) {
        /* The digit being processed. */
        char c;
        /* The value of this byte. */
        int n = 0;
        while (1) {
            c = * start;
            start++;
            if (c >= '0' && c <= '9') {
                n *= 10;
                n += c - '0';
            }
            /* We insist on stopping at "." if we are still parsing
               the first, second, or third numbers. If we have reached
               the end of the numbers, we will allow any character. */
            else if ((i < 3 && c == '.') || i == 3) {
                break;
            }
            else {
                return 0;//INVALID;
            }
        }
        if (n >= 256) {
            return 0;//INVALID;
        }
        v *= 256;
        v += n;
    }
    return v;
}


/*
*	����� ����� �������� ������ ������� �� �������!!!!!!!!!!!!!!!!!
*/
LoginServer::LoginServer(): _activity(this, &LoginServer::runActivity)
{
}
void LoginServer::start()
{
	_activity.start();
}
void LoginServer::stop()
{
	_activity.stop(); // request stop
	_activity.wait(); // wait until activity actually stops
}
void LoginServer::runActivity()
{
	while (!_activity.isStopped())
	{
		Poco::Net::ServerSocket svs(1234);
		svs.setReuseAddress(true);
		svs.setReusePort(true);
		Poco::Net::TCPServerParams::Ptr pParams = new Poco::Net::TCPServerParams();
		pParams->setMaxThreads(4);
		pParams->setMaxQueued(4);
		pParams->setThreadIdleTime(100);

		Poco::Net::TCPServer myServer(new GameServerConnectionFactory(pService,pContext), svs, pParams);
		myServer.start();
		serverloop = true;
		while(serverloop){
			//std::cout << "Activity TCP Server running." << std::endl;
			Sleep(1000);
		}
	}
}