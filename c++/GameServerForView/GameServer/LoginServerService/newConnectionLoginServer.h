#pragma once
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/Net/NetException.h"
#include "Poco/MongoDB/Document.h"
#include "DBGameService.h"
#include "LoginData.h"
//#include "LoginData.h"
#include "RequestCommand.h"
#include "LPlayer.h"
using Poco::OSP::BundleContext;
using Poco::MongoDB::Document;
class newConnection : public Poco::Net::TCPServerConnection {
public:
	BundleContext::Ptr pContext;
	newConnection(const Poco::Net::StreamSocket& s, DBGameService::Ptr _pService, BundleContext::Ptr _pContext);
	~newConnection();
	void run();
public:
	DBGameService::Ptr pService;
	const Poco::Net::StreamSocket& ss;
};
