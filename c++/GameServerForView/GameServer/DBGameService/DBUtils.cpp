#include "DBUtils.h"
#include "Poco/BinaryWriter.h"
#include "Poco/BinaryReader.h"
#include "Poco/Format.h"
#include "Poco/String.h"
#define GAMEDBNAME "dragons"
using Poco::BinaryWriter;
using Poco::BinaryReader;


DBUtils::DBUtils(void)
{
/*#if defined(_MSC_VER)
	// Turn on memory leak detection (use _CrtSetBreakAlloc to break at specific allocation)
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);

	//_CrtSetBreakAlloc(6150);
#endif*/
	//connection = new Poco::MongoDB::Connection("localhost", 27017);
	connection.connect("localhost", 27017);
	//_Player = NULL;
}

DBUtils::DBUtils(Poco::OSP::BundleContext::Ptr pContext)
{
/*#if defined(_MSC_VER)
	// Turn on memory leak detection (use _CrtSetBreakAlloc to break at specific allocation)
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);

	//_CrtSetBreakAlloc(6150);
#endif*/
	//connection = new Poco::MongoDB::Connection("localhost", 27017);
	_pContext = pContext;
	connection.connect("localhost", 27017);
	//_Player = NULL;
}


inline std::string HexStringObjectIdtoBinaryStringObjectId(string HexStringObjectId){
	std::string res;
	res.reserve(HexStringObjectId.size() / 2);
	for (int i = 0; i < HexStringObjectId.size(); i += 2)
	{
		std::istringstream iss(HexStringObjectId.substr(i, 2));
		int temp;
		iss >> std::hex >> temp;
		res += static_cast<char>(temp);
	}
	return res;
}

DBUtils::~DBUtils(void)
{
	connection.disconnect();
}

Poco::SharedPtr<vector<Document::Ptr>>  DBUtils::LoadWorlds(){
	Poco::SharedPtr<vector<Document::Ptr>> vworlds = Poco::SharedPtr<vector<Document::Ptr>>(new vector<Document::Ptr>());
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, "Worlds");
	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);
	while (1)
	{
		for (Poco::MongoDB::Document::Vector::const_iterator wit = response.documents().begin(); wit != response.documents().end(); ++wit)
		{
			vworlds->push_back(*wit);
		}
		if (response.cursorID() == 0)
		{
			break;
		}
		response = cursor.next(connection);
	}
	return vworlds;
}

Document::Ptr DBUtils::LoadSpells(string $ref, string $id){
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, $ref);

	string res_spells = HexStringObjectIdtoBinaryStringObjectId($id);
	Poco::SharedPtr<ObjectId> obj_spells(new ObjectId(res_spells));
	std::string st = obj_spells->toString();
	cursor.query().selector().add<ObjectId::Ptr>("_id", obj_spells);
	cursor.query().setNumberToReturn(1);
	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);
	if (!response.empty() && response.hasDocuments()){
		Document::Ptr p = response.documents()[0];
		return p;
	}
	return Document::Ptr(NULL);
}

Poco::SharedPtr<vector<Document::Ptr>>  DBUtils::LoadContinents(string $dbref, string $id){
	Poco::SharedPtr<vector<Document::Ptr>> vcontinents = Poco::SharedPtr<vector<Document::Ptr>>(new vector<Document::Ptr>());
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, $dbref);
	string res_continent = HexStringObjectIdtoBinaryStringObjectId($id);
	Poco::SharedPtr<ObjectId> obj_continent(new ObjectId(res_continent));
	std::string st = obj_continent->toString();
	cursor.query().selector().add<ObjectId::Ptr>("_id", obj_continent);

	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);

	while (1){
		for (Poco::MongoDB::Document::Vector::const_iterator cit = response.documents().begin(); cit != response.documents().end(); ++cit)
		{
			vcontinents->push_back(*cit);
		}
		if (response.cursorID() == 0)
		{
			break;
		}
		response = cursor.next(connection);
	}
	return vcontinents;
}

Poco::SharedPtr<std::vector<Document::Ptr>> DBUtils::LoadLocations(string $ref, string $id){
	Poco::SharedPtr<vector<Document::Ptr>> vlocations = Poco::SharedPtr<vector<Document::Ptr>>(new vector<Document::Ptr>());
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, $ref);

	string res_location = HexStringObjectIdtoBinaryStringObjectId($id);
	Poco::SharedPtr<ObjectId> obj_location(new ObjectId(res_location));
	std::string st = obj_location->toString();
	cursor.query().selector().add<ObjectId::Ptr>("_id", obj_location);

	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);

	while (1){
		for (Poco::MongoDB::Document::Vector::const_iterator lit = response.documents().begin(); lit != response.documents().end(); ++lit)
		{
			vlocations->push_back(*lit);
		}
		if (response.cursorID() == 0)
		{
			break;
		}
		response = cursor.next(connection);
	}
	return vlocations;
}

Poco::SharedPtr<std::vector<Document::Ptr>> DBUtils::LoadCells(string $ref, string $id){
	Poco::SharedPtr<vector<Document::Ptr>> vcells = Poco::SharedPtr<vector<Document::Ptr>>(new vector<Document::Ptr>());
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, $ref);

	string res_cell = HexStringObjectIdtoBinaryStringObjectId($id);
	Poco::SharedPtr<ObjectId> obj_cell(new ObjectId(res_cell));
	std::string st = obj_cell->toString();
	cursor.query().selector().add<ObjectId::Ptr>("_id", obj_cell);

	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);
	while (1){
		for (Poco::MongoDB::Document::Vector::const_iterator cit = response.documents().begin(); cit != response.documents().end(); ++cit)
		{
			vcells->push_back(*cit);
		}
		if (response.cursorID() == 0)
		{
			break;
		}
		response = cursor.next(connection);
	}
	return vcells;
}

Poco::SharedPtr<std::vector<Document::Ptr>> DBUtils::LoadNpcs(string $ref, string $id){
	Poco::SharedPtr<vector<Document::Ptr>> vnpcs = Poco::SharedPtr<vector<Document::Ptr>>(new vector<Document::Ptr>());
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, $ref);
	string res_npcptr = HexStringObjectIdtoBinaryStringObjectId($id);
	Poco::SharedPtr<ObjectId> obj_npcptr(new ObjectId(res_npcptr));
	std::string st = obj_npcptr->toString();
	cursor.query().selector().add<ObjectId::Ptr>("_id", obj_npcptr);

	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);

	while (1){
		for (Poco::MongoDB::Document::Vector::const_iterator npcptrit = response.documents().begin(); npcptrit != response.documents().end(); ++npcptrit)
		{
			vnpcs->push_back(*npcptrit);
		}
		if (response.cursorID() == 0)
		{
			break;
		}
		response = cursor.next(connection);
	}
	return vnpcs;
}

Document::Ptr DBUtils::getUser(std::string username, bool b, bool t){
	Poco::MongoDB::Cursor cursor("GameServer", "Users");
	if (!b){
		cursor.query().selector().add("username", username);
		if (t){
			cursor.query().returnFieldSelector().add("username", 1);
			cursor.query().returnFieldSelector().add("password", 1);
			cursor.query().returnFieldSelector().add("email", 1);
			cursor.query().returnFieldSelector().add("_id", 1);
			cursor.query().returnFieldSelector().add("logged", 1);
			cursor.query().returnFieldSelector().add("banned", 1);
		}
	}
	else{
		string res = HexStringObjectIdtoBinaryStringObjectId(username);
		Poco::SharedPtr<ObjectId> obj(new ObjectId(res));
		std::string st = obj->toString();
		cursor.query().selector().add<ObjectId::Ptr>("_id", obj);
	}
	cursor.query().setNumberToReturn(1);
	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);
	if (!response.empty() && response.hasDocuments()){
		Document::Ptr p = response.documents()[0];
		return p;
	}
	return Document::Ptr(NULL);
}

Poco::SharedPtr<std::string> DBUtils::getTranslate(string $ref, string $id, string Name){
	Poco::MongoDB::Cursor cursor(GAMEDBNAME, $ref);
	string res_cell = HexStringObjectIdtoBinaryStringObjectId($id);
	Poco::SharedPtr<ObjectId> obj_cell(new ObjectId(res_cell));
	cursor.query().selector().add<ObjectId::Ptr>("_id", obj_cell);
	cursor.query().returnFieldSelector().add(Name, 1);
	cursor.query().setNumberToReturn(1);
	Poco::SharedPtr<std::string> str = new std::string();
	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);
	if (!response.empty() && response.hasDocuments()){
		Document::Ptr p = response.documents()[0];
		*str = p->get<std::string>(Name);
	}
	else *str = "Translate lang not found!";	
	return str;
}

Poco::MongoDB::Document::Ptr DBUtils::getBalanceServer(){
	Poco::MongoDB::Cursor cursor("GameServer", "GameServers");
	cursor.query().setNumberToReturn(1);
	cursor.query().selector().addNewDocument("$query");
	cursor.query().selector().addNewDocument("$orderby").add("online", -1);
	Poco::MongoDB::ResponseMessage& response = cursor.next(connection);
	return response.documents()[0];
	/*Poco::SharedPtr<BalanceServer> bs = NULL; 
	while (1)
	{
		for (Poco::MongoDB::Document::Vector::const_iterator it = response.documents().begin(); it != response.documents().end(); ++it)
		{
			Poco::SharedPtr<BalanceServer> p(new BalanceServer());
			bs.assign(p);
			bs->ip = (*it)->get<std::string>("host");
			bs->port = (*it)->get<std::string>("port");
		}
		if (response.cursorID() == 0)
		{
			break;
		}
		response = cursor.next(connection);
	};
	return bs;*/
}
void DBUtils::setUserLoggedIn(std::string& username){
	Poco::MongoDB::Database db("GameServer");
	Poco::SharedPtr<Poco::MongoDB::UpdateRequest> request = db.createUpdateRequest("Users");
	request->selector().add("username", "benosa");
	request->update().addNewDocument("$set").add("logged", true);
	connection.sendRequest(*request);
	Poco::MongoDB::Document::Ptr lastError = db.getLastErrorDoc(connection);
	Sleep(100);
}

void DBUtils::setUserBanned(std::string& username){
	Poco::MongoDB::Database db("GameServer");
	Poco::SharedPtr<Poco::MongoDB::UpdateRequest> request = db.createUpdateRequest("Users");
	request->selector().add("username", username);
	request->update().addNewDocument("$set").add("banned", true);
	connection.sendRequest(*request);
	Poco::MongoDB::Document::Ptr lastError = db.getLastErrorDoc(connection);
	Sleep(100);
}