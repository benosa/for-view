#pragma once
//#include "Player.h"
#include "BalanceServer.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/MongoDB/MongoDB.h"
#include "Poco/MongoDB/Connection.h"
#include "Poco/MongoDB/Database.h"
#include "Poco/MongoDB/Cursor.h"
#include "Poco/MongoDB/Array.h"
#include "Poco/MongoDB/ObjectId.h"
#include "Poco/MongoDB/Binary.h"
#include "Poco/MongoDB/Document.h"
#include "Poco/MongoDB/Element.h"
#include "Poco/NumberParser.h"
#include "Poco/SharedPtr.h"
#include "Poco/MongoDB/BSONReader.h"

//#include "Worlds.h"
//#include "Cell.h"
// #include "Location.h"
#include <string>

//using namespace std;
using namespace Poco::MongoDB;


class Player;
class DBUtils
{
public:
	DBUtils(void);
	DBUtils(Poco::OSP::BundleContext::Ptr);
	~DBUtils(void);
public:
	Poco::OSP::BundleContext::Ptr _pContext;
	Poco::MongoDB::Connection connection;
	//Poco::SharedPtr<Player> getUser(std::string,int);
	Document::Ptr getUser(std::string, bool b = false, bool t = false);
	void setUserLoggedIn(std::string&);
	void setUserBanned(std::string&);
	Poco::MongoDB::Document::Ptr getBalanceServer();
	//Poco::SharedPtr<Player> _Player;
	Poco::SharedPtr<vector<Document::Ptr>> LoadWorlds();
	Poco::SharedPtr<std::vector<Document::Ptr>> LoadContinents(string $ref, string $id);
	Poco::SharedPtr<std::vector<Document::Ptr>> LoadLocations(string $ref, string $id);
	Poco::SharedPtr<std::vector<Document::Ptr>> LoadCells(string $ref, string $id);
	Poco::SharedPtr<std::vector<Document::Ptr>> LoadNpcs(string $ref, string $id);
	Document::Ptr LoadSpells(string $ref, string $id);
	Poco::SharedPtr<std::string> getTranslate(string $ref, string $id, string Name);
};

