#pragma once
#include "Poco/OSP/Service.h"
#include "Poco/SharedPtr.h"
#include "Poco/AutoPtr.h"
#include "Poco/MongoDB/Document.h"
#include <map>
#include <vector>
//#include "Player.h"
using namespace std;
using namespace Poco::MongoDB;
class DBGameService : public Poco::OSP::Service
{
public:
	typedef Poco::AutoPtr<DBGameService> Ptr;

	virtual std::string greeting() = 0;
	virtual Poco::MongoDB::Document::Ptr getUser(std::string, bool b = false, bool t = false) = 0;
	virtual void setUserLoggedIn(std::string&) = 0;
	virtual void setUserBanned(std::string&) = 0;
	virtual Poco::MongoDB::Document::Ptr getBalanceServer() = 0;
	/*
	FOR WORLDS
	*/
	virtual Poco::SharedPtr<std::vector<Document::Ptr>> LoadWorlds() = 0;
	virtual Poco::SharedPtr<std::vector<Document::Ptr>> LoadContinents(string $ref,string $id) = 0;
	virtual Poco::SharedPtr<std::vector<Document::Ptr>> LoadLocations(string $ref, string $id) = 0;
	virtual Poco::SharedPtr<std::vector<Document::Ptr>> LoadCells(string $ref, string $id) = 0;
	virtual Poco::SharedPtr<std::vector<Document::Ptr>> LoadNpcs(string $ref, string $id) = 0;
	virtual Poco::SharedPtr<std::string> getTranslate(string $ref, string $id, string Name) = 0;
	//virtual Document::Ptr LoadSpells(string $ref, string $id) = 0;
};