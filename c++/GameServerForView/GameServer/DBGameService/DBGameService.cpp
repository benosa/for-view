#include "DBGameService.h"
#include "Poco/OSP/BundleActivator.h"
#include "Poco/OSP/BundleContext.h"
#include "Poco/OSP/Bundle.h"
#include "Poco/OSP/ServiceRegistry.h"
#include "Poco/ClassLibrary.h"
#include "DBUtils.h"
//#include "Player.h"

using Poco::OSP::BundleActivator;
using Poco::OSP::BundleContext;
using Poco::OSP::Bundle;
using Poco::OSP::Properties;
using Poco::OSP::ServiceRef;

class DBGameServiceImpl: public DBGameService
{
	public:
		DBUtils DB;
		DBGameServiceImpl(BundleContext::Ptr pContext):
		_pContext(pContext)
		{
			DB._pContext = pContext;
		}
		
		~DBGameServiceImpl()
		{
		}
		std::string greeting()
		{
			return "benosa";
		}

		Poco::MongoDB::Document::Ptr getUser(string username, bool b, bool t){
			return DB.getUser(username, b, t);
		}

		Poco::MongoDB::Document::Ptr getBalanceServer(){
			return DB.getBalanceServer();
		}
		/*Poco::SharedPtr<map<string, Poco::SharedPtr<Worlds>>> LoadWorlds(){
			return DB.LoadWorlds();
		}*/
		Poco::SharedPtr<std::vector<Document::Ptr>> LoadWorlds(){
			return DB.LoadWorlds();
		}
		Poco::SharedPtr<std::vector<Document::Ptr>> LoadContinents(string $ref, string $id){
			return DB.LoadContinents($ref, $id);
		}
		Poco::SharedPtr<std::vector<Document::Ptr>> LoadLocations(string $ref, string $id){
			return DB.LoadLocations($ref, $id);
		}
		Poco::SharedPtr<std::vector<Document::Ptr>> LoadCells(string $ref, string $id){
			return DB.LoadCells($ref, $id);
		}
		Poco::SharedPtr<std::vector<Document::Ptr>> LoadNpcs(string $ref, string $id){
			return DB.LoadNpcs($ref, $id);
		}
		Poco::SharedPtr<std::string> getTranslate(string $ref, string $id, string Name){
			return DB.getTranslate($ref, $id, Name);
		}
		void setUserLoggedIn(std::string& username){
			DB.setUserLoggedIn(username);
		}

		void setUserBanned(std::string& username){

		}
	
		const std::type_info& type() const
		{
			return typeid(DBGameService);
		}
	
		bool isA(const std::type_info& otherType) const
		{
			std::string name(typeid(DBGameService).name());
			return name == otherType.name() || Service::isA(otherType);
		}
	private:
		BundleContext::Ptr _pContext;
};


class DBGameServiceBundleActivator: public BundleActivator
{
	public:
		DBGameServiceBundleActivator()
		{
		}
	
		~DBGameServiceBundleActivator()
		{
		}
	
		void start(BundleContext::Ptr pContext)
		{
			DBGameService::Ptr pService = new DBGameServiceImpl(pContext);
			_pService = pContext->registry().registerService("com.InstitutIt.GameServer.DBGameService", pService, Properties());
		}
		
		void stop(BundleContext::Ptr pContext)
		{
			pContext->registry().unregisterService(_pService);
		}

	private:
		ServiceRef::Ptr _pService;
};


POCO_BEGIN_MANIFEST(BundleActivator)
	POCO_EXPORT_CLASS(DBGameServiceBundleActivator)
POCO_END_MANIFEST
