#pragma once
#include <string>
#include "Poco/SharedPtr.h"

using namespace std;

class Player
{
public:
	Player(){};
	~Player(){};
	std::string username;
	std::string password;
	std::string email;
	std::string id;
	bool logged;
	bool banned;
};